//
//  imageViewDesignable.swift
//  designables
//
//  Created by Kevin Sabajanes on 01/05/2019.
//  Copyright © 2019 Kevinsan. All rights reserved.
//

import UIKit

@IBDesignable open class DezignableImageView: UIImageView{
    
    //Redondeamos las esquinas superiores de la foto
    @IBInspectable var corner: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
}
