//
//  horizontalSlider.swift
//  arWorld
//
//  Created by Kevinsan on 10/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

@IBDesignable open class HorizontalSlider: UIImageView{
    
    //Redondeamos las esquinas superiores de la foto
    @IBInspectable var corner: CGFloat {
        get {
            return self.layer.cornerRadius
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
}

