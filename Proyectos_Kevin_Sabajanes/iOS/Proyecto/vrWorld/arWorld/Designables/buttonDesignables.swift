//
//  buttonDesignables.swift
//  designables
//
//  Created by Kevin Sabajanes on 02/05/2019.
//  Copyright © 2019 Kevinsan. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable open class buttonDesignables: UIButton {

        //Ancho del borde
        @IBInspectable var borderWidth: CGFloat {
            set {
                layer.borderWidth = newValue
            }
            get {
                return layer.borderWidth
            }
        }
        //Valor de redondeado de los bordes
        @IBInspectable var cornerRadius: CGFloat {
            set {
                layer.cornerRadius = newValue
            }
            get {
                return layer.cornerRadius
            }
        }
        //Color de los bordes
        @IBInspectable var borderColor: UIColor? {
            set {
                guard let uiColor = newValue else { return }
                layer.borderColor = uiColor.cgColor
            }
            get {
                guard let color = layer.borderColor else { return nil }
                return UIColor(cgColor: color)
            }
        }
    
}
