//
//  viewDesignable.swift
//  designables
//
//  Created by Kevin Sabajanes on 01/05/2019.
//  Copyright © 2019 Kevinsan. All rights reserved.
//

import Foundation
import UIKit

var animado = false
@IBDesignable open class viewDesignable: UIView {
    
    //Declaramos la capa que va a tener el gradiente
    private var gradient: CAGradientLayer?

    //Valor de redondeado de los bordes
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }
        set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    //Color de los bordes de la vista
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    //Anchura de los bordes de la vista
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    //Color de la sombra
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    //Opacidad de la sombra
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
    //Longitud de la sombra y su dirección donde s proyectará
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set {
            self.layer.shadowOffset = newValue
        }
    }
    
    //Colores para el gradiente de la vista
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.white
        
    override open class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    //Establece el color del gradiente desde el atributes inspector
    override open func layoutSubviews() {
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
    
    //Establece si la vista se podra animar o no
    @IBInspectable var animar: Bool {
        get {
            return animado
        }
        set {
            animado = newValue
        }
    }
}

extension viewDesignable {
    
    //MARK:- Animacion por defeto al llamar a este metodo de la vista
    public func AnimateView(){
        // Si se ha seleccionado animado ON
        if animado {
            provideAnimation(animationDuration: 2.0, deleyTime: 0, springDamping: 0.8, springVelocity: 1.00)
        }
    }
    private func provideAnimation(animationDuration:TimeInterval, deleyTime:TimeInterval, springDamping:CGFloat, springVelocity:CGFloat){
        self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: animationDuration,
                       delay: deleyTime,
                       usingSpringWithDamping: springDamping,
                       initialSpringVelocity: springVelocity,
                       options: .allowUserInteraction,
                       animations: {
                        self.transform = CGAffineTransform.identity
        })
    }
}


