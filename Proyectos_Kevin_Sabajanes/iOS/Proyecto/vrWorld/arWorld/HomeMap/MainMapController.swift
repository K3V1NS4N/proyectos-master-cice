//
//
//  Created by Kevin Sabajanes García-Fraile on 10/5/19.
//  Copyright © 2019 Kevin Sabajanes García-Fraile. All rights reserved.
//
import UIKit
import CoreLocation
import GoogleMaps
import AudioToolbox
import SwiftyJSON

protocol MapDelegateFirebase {
    func didReceiveData(_ data: [ElementoMapa])
    
}
class MainMapController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate, MapDelegateFirebase {
    //Funcion a la que se llamada cuando el modelo JsonParser recibe la informacion y la formatea
    func didReceiveData(_ data: [ElementoMapa]) {
        print("NOTIFICADO")
        if data.count != 0{
            //for i in 0...data.count-1 {
            for i in 0...10 {
                /*
                    Obtener los elementos almacenados en la base de datos esta desactivado, debido a que esta version
                    no cuenta con los terminos y la politica de privacidad para gestionar las localizaciones que se suben, ya que la localizacion es un dato muy sensible,
                    por lo tanto en su lugar se muestran una serie de elementos de manera aleatoria para emularlo como si fuera real
                */
                //Se añaden los elementos y sus localizaciones de la base de datos
                //createMarker(elemento: data[i])
                
                
                let randomInt = Int.random(in: 0..<3)
                let randomDouble = Double.random(in: 0.0000...0.0003)
                let randomDouble2 = Double.random(in: 0.0000...0.0003)
                let elemento = ElementoMapa(latitud: lastLongitude+randomDouble, longitud: lastLatitude+randomDouble2, icono: randomInt)
                createMarker(elemento: elemento)
                
            }
        }
        stopLoader()
    }
    
    var manager = JsonParser()
    var mapView: GMSMapView? = nil
    var centrado: Bool = true
    var ejemplo: Bool = false
    
    @IBOutlet weak var user: UIButton!
    @IBOutlet weak var botonVr: UIButton!
    var locationManager = CLLocationManager()
    var bearing: Double?
    var elementos: [ElementoMapa]?
    static var elementos2: NSDictionary?
    var creado = false
    private var lastLongitude = 0.0
    private var lastLatitude = 0.0
    let nombreIconos = ["nota","arElement2","art","picture"]
    let networkingFirebase = Download()
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLoader()
        networkingFirebase.getUserInfo()
        manager.delegate = self
        configurarMapa()
        configurarBotones()

    }
 
    //Se encarga de crear los marcadores a si como tambien mostrarlos en la pantalla
    func createMarker(elemento: ElementoMapa){

        let marker = GMSMarker()
        print("SE AÑADE EL MARCADOR")
        marker.position = CLLocationCoordinate2D(latitude: elemento.latitud, longitude: elemento.longitud)
        marker.title = ""
        marker.snippet = ""
        marker.icon = UIImage(named: nombreIconos[elemento.icono])
        
        //Le damos animación a los elementos
        marker.appearAnimation = .pop
        //Le damos cierto delay para que parezca que cada uno se carga de manera independiente y quede guay
        let randomDouble = Double.random(in: 0.15...0.5)
        DispatchQueue.main.asyncAfter(deadline: .now() + randomDouble, execute: {
            marker.map = self.mapView
        })
    }

    
    //Funcion encargada de configurar el mapa de Google
    func configurarMapa(){
        
        // Create a GMSCameraPosition that tells the map to display the
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 100)
        mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        view = mapView
        
        self.mapView?.settings.rotateGestures = false
        
        //Location Manager para obtener la ubicacion actual
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        //Peedimos permiso para acceder a la localizacion
        locationManager.requestWhenInUseAuthorization()
        
        // Activamos la brújula
        mapView?.settings.compassButton = true
        
        // Localizacion de usuario
        mapView?.isMyLocationEnabled = true
        mapView?.delegate = self
        
        //Colocamos mejor la brujula respecto al botón de usuario
        let padding = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 5)
        mapView?.padding = padding
        
        //Configuramos el de legado para obtener los datos del acelerometro de la brújula
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.startUpdatingHeading()
        

        do {
            let hour = Calendar.current.component(.hour, from: Date())
            //Gestiona cuando se tiene que mostrar el mapa de dia o el de noche
            if hour > 7 && hour < 21 {
                
                // Le pasamos el estilo del mapa mediante un JSON.
                if let estiloMapaJson = Bundle.main.url(forResource: "gris", withExtension: "json") {
                    mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: estiloMapaJson)
                } else {
                    NSLog("No se ha podido encontrar el archivo json")
                }
                
            }else{
                
                // Le pasamos el estilo del mapa mediante un JSON.
                if let estiloMapaJson = Bundle.main.url(forResource: "oscuro", withExtension: "json") {
                    mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: estiloMapaJson)
                } else {
                    NSLog("No se ha podido encontrar el archivo json")
                }
            }
            
        } catch {
            NSLog("Ha ocurrido un error al cargar el mapa ->. \(error)")
        }
    }
    
    //Añadimos los contraint programaticamente para poder mostrarlos delante del mapa de Google
    // TODO: Problema con el iphoneX/XS la brujula choca con el boton "user"
    func configurarBotones(){
        
        self.view.insertSubview(botonVr, at: 2)
        self.view.insertSubview(user, at: 2)
        self.view.insertSubview(loader, at: 2)
        botonVr.translatesAutoresizingMaskIntoConstraints = false
        user.translatesAutoresizingMaskIntoConstraints = false
        loader.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: botonVr, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        NSLayoutConstraint(item: botonVr, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: -30.0).isActive = true
    
        NSLayoutConstraint(item: user, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: -15.0).isActive = true
        NSLayoutConstraint(item: user, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 90.0).isActive = true
        NSLayoutConstraint(item: loader, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: -25.0).isActive = true
        NSLayoutConstraint(item: loader, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 140.0).isActive = true
        
        let verticalConstraint = NSLayoutConstraint(item: botonVr, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: botonVr, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100)
        let heightConstraint = NSLayoutConstraint(item: botonVr, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100)
        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
    }
    
    //Se encarga de mostrar la animacion de carga
    func startLoader(){

        loader.hidesWhenStopped = true
        loader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loader.startAnimating()
        
        // UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    //Se encarga de parar la animacion de carga
    func stopLoader(){
        
        loader.stopAnimating()
//        UIApplication.shared.endIgnoringInteractionEvents()
        
    }

    //Funcion que se llama cada vez que hay un cambio en la localización, sirve para obtener siempre el foco de tu localizacion.
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {

        if let location = mapView.myLocation{
            
            // Si se ha centrado el foco de la localización
            if centrado {
                self.centrado = false
                //Ejecutamos con un delay de 3 segundos
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    self.centrado = true
                })
                //Latitud y longitud de nuestra localización
                var latitude = location.coordinate.latitude
                var longitude = location.coordinate.longitude
                
                //Establece el foco
                let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude:(location.coordinate.longitude), zoom:19)
                
                //Calcula si la distancia entre la localizacion y la establecida esta dentro del rango de accion
                //let distancia = esteRango(localizacion: (x: (location.coordinate.latitude), y: (location.coordinate.longitude)))

                //Reducimos los decimales a la localizacion con el objetivo de ahorrar memoria en firebase
                latitude = truncateDigitsAfterDecimal(number: latitude, afterDecimalDigits: 3)
                longitude = truncateDigitsAfterDecimal(number: longitude, afterDecimalDigits: 3)
                
                //Si la localizacion no ha cambiado, no se hace nada (Mejorable)
                print("LATITUDE: \(latitude) | \(lastLatitude) LONGITUDE: \(longitude) | \(lastLatitude)")
                if latitude != lastLatitude && longitude != lastLatitude{
                    lastLatitude = latitude
                    lastLongitude = longitude
                    networkingFirebase.getMap(latitude: latitude,longitude: longitude,json: manager)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    if !self.ejemplo {
                        self.ejemplo = true
                        for i in 0...15 {
                            
                            let randomInt = Int.random(in: 0..<4)
                            let randomInt2 = Int.random(in: 0..<4)
                            let randomDouble = Double.random(in: 0.0000...0.0005)
                            let randomDouble2 = Double.random(in: 0.0000...0.0005)
                            var elemento: ElementoMapa?
                            switch randomInt {
                            case  0:
                                elemento = ElementoMapa(latitud: self.lastLatitude-randomDouble, longitud: self.lastLongitude+randomDouble2, icono: randomInt2)
                            case  1:
                                elemento = ElementoMapa(latitud: self.lastLatitude-randomDouble, longitud: self.lastLongitude-randomDouble2, icono: randomInt2)
                            case  2:
                                elemento = ElementoMapa(latitud: self.lastLatitude+randomDouble, longitud: self.lastLongitude-randomDouble2, icono: randomInt2)
                            case  3:
                                elemento = ElementoMapa(latitud: self.lastLatitude+randomDouble, longitud: self.lastLongitude+randomDouble2, icono: randomInt2)
                            default:
                                elemento = ElementoMapa(latitud: self.lastLatitude+randomDouble, longitud: self.lastLongitude-randomDouble2, icono: randomInt2)
                            }
                            self.createMarker(elemento: elemento!)
                        
                        }
                        self.stopLoader()
                    }
                })
                
                //Vibra si estas cerca del elemento tridimensional
                /*
                if distancia{
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                }
                */
                
                //print("DISTANCIA: \(distancia)")
                
                //Anima el boton
                let pulse = Pulsing(numberOfPulses: 1, radius: 80, position: botonVr.center)
                pulse.animationDuration = 1.5
                pulse.backgroundColor = #colorLiteral(red: 0.560860335, green: 0.51897626, blue: 0.9686274529, alpha: 1)
                self.view.layer.insertSublayer(pulse, below: botonVr.layer)
                
                mapView.animate(to: camera)
                mapView.animate(toBearing: bearing ?? 0)
                //Ángulo de la vista
                mapView.animate(toViewingAngle: 45)
            }
        }
       
    }
    
    //Se encagar de mover el mapa respecto a la direccion que marca la brujula de tu localizacion
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {

        if let mapa = mapView{
            Info.bearing = bearing ?? 0
            bearing = newHeading.trueHeading
            mapa.animate(toBearing: newHeading.trueHeading)
        }
    }
    
    
    
    //CALCULA SI UN PUNTO ESTA DENTRO DEL RANGO DE ACCION DE OTRO PUNTO
    /*
    func distancia(desde origen: (x: Double, y: Double), hasta
        destino: (x: Double, y: Double)) -> Double {
        
        let distanciaX = Double(origen.x - destino.x)
        let distanciaY = Double(origen.y - destino.y)
        
        return ((distanciaX * distanciaX) + (distanciaY * distanciaY)).squareRoot()
    }
    func esteRango(localizacion: (x: Double, y: Double)) -> Bool{
        
        let distanciaEntrega = distancia(desde: localizacion, hasta: localizacionRest)
        
        let distanciaEntrega2 = distancia(desde: localizacion, hasta: localizacionRest2)
        
        print(distanciaEntrega)
        
        return distanciaEntrega < rangoAccion || distanciaEntrega2 < rangoAccion2
        
    }
    */

    //Sirve para añadir un marcador personalizado dentro del mapa con un longPress
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        // Custom logic here
        let marker = GMSMarker()
        marker.position = coordinate
        print(coordinate.latitude)
        print(coordinate.longitude)
        marker.title = "Alguien ha puesto una notaº"
        marker.snippet = ""
        marker.icon = UIImage(named: "nota")
        marker.map = mapView
    }
    
    
    //Funcion encargada de reducir el numero de decimales de un numero
    func truncateDigitsAfterDecimal(number: Double, afterDecimalDigits: Int) -> Double {
        if afterDecimalDigits < 1 || afterDecimalDigits > 512 {return 0.0}
        return Double(String(format: "%.\(afterDecimalDigits)f", number))!
    }
}


