//
//  ajustes.swift
//  arWorld
//
//  Created by Kevinsan on 21/05/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
// MARK: - Configurable constants
private let itemHeight: CGFloat = 75
private let lineSpacing: CGFloat = 0
private let xInset: CGFloat = 0
private let topInset: CGFloat = 0

private var iconos = ["security","notification","map","world","info"]
private var colores = [#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 1, green: 0.3707650798, blue: 0.3130188101, alpha: 1),#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1),#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)]
class Ajustes: UIViewController {
    fileprivate let cellId = "ShareCell"
    @IBOutlet private weak var collectionView: UICollectionView!


    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: cellId, bundle: nil)
        collectionView.register( nib, forCellWithReuseIdentifier: cellId)
        collectionView.contentInset.bottom = itemHeight
        configureCollectionViewLayout()
        setUpNavBar()
        
        
    }
    
    @IBAction func volver(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    private func setUpNavBar() {
        navigationItem.title = "Feed"
        navigationController?.view.backgroundColor = UIColor.white
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    private func configureCollectionViewLayout() {
        guard let layout = collectionView.collectionViewLayout as? VegaScrollFlowLayout else { return }
        layout.minimumLineSpacing = lineSpacing
        layout.sectionInset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        let itemWidth = UIScreen.main.bounds.width - 2 * xInset
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        collectionView.collectionViewLayout.invalidateLayout()
    }
 

}
extension Ajustes: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ShareCell
        
        cell.button.bottomColor = colores[indexPath.row]
        cell.button.topColor = colores[indexPath.row]
        cell.icon.image = UIImage(named: iconos[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconos.count
    }
}
