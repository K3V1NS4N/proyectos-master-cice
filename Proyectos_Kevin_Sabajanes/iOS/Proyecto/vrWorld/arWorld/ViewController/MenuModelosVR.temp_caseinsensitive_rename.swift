//
//  menuModelosVR.swift
//  googleMapsIOS
//
//  Created by Kevinsan on 20/05/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
import QuickLook


class MenuModelosVR: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, QLPreviewControllerDelegate, QLPreviewControllerDataSource, UIViewControllerPreviewingDelegate {
   
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewButton: UICollectionView!
    let collectionViewAIdentifier = "CollectionViewACell"
    let collectionViewBIdentifier = "CollectionViewBCell"
    
    //Caterogrias a elegir
    enum Categoria {
        case nota
        case dibujar
        case ARObjets
        case caso4
        case caso5
        case caso6
        
    }

    var numElementos = 10
    //search results will be limited to 20
    private var models = ["oveja","gato","penguin","mole","santa","oveja","oveja","oveja","oveja","oveja"]
    private var dataImages: [UIImage?] = []
    private var thumbnailIndex = 0
    //Son los nombres de los iconos de las categorias a elegir
    private var IconoGris = ["notaGrisMenu","artGrisMenu","elementoArGrisMenu","notaGrisMenu","notaGrisMenu","notaGrisMenu"]
    private var IconoColor = ["notaColorMenu","artButton","arColorMenu","artButton","artButton","artButton"]

    var indexPathSelected: IndexPath?
    var categoriaSeleccionada = Categoria.ARObjets
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerForPreviewing(with: self, sourceView: collectionView)

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as! UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
        collectionView.allowsMultipleSelection = false
        
    }
    
    
    @IBAction func exit(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //Devolvemos los numeros de elementos
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == self.collectionView {
        
            return numElementos
        }
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! menuVRCollectionViewCell
            switch categoriaSeleccionada {
            case Categoria.nota:
                 cell.imagen.image = UIImage(named: "note")
                 cell.nombreModelo3D.text = "Nota"
            return cell
               
            case Categoria.dibujar:  return cell
             
            case Categoria.ARObjets:
                
                cell.imagen.image = UIImage(named: models[indexPath.row])
                cell.nombreModelo3D.text = models[indexPath.row]
                cell.quickLookButton.tag = indexPath.row
                //Añadimos el selector para que llame al metodo indicado al pulsarse
                cell.quickLookButton.addTarget(self, action: #selector(editGroupAction(sender:)), for: .touchUpInside)
                cell.quickLookButton.contentEdgeInsets = UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
                return cell
            case Categoria.caso4:  return cell
                
            case Categoria.caso5:  return cell
               
            case Categoria.caso6:  return cell
                
            default:
                print("Otherwise, do something else.")
                 return cell
            }
    
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellButton", for: indexPath) as! menuVRCollectionViewCell
           
            switch indexPath.row {
            case 0:
//                var cellImg : UIImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 25, height: 25))
//                cellImg.image = UIImage(named: "notablanco")
//                cell.addSubview(cellImg)
                cell.imagenBoton.image = UIImage(named: "notaGrisMenu")
            case 1:
                cell.imagenBoton.image = UIImage(named: "artGrisMenu")
            case 2:
                cell.imagenBoton.image = UIImage(named: "elementoArGrisMenu")
            case 3:
                cell.imagenBoton.image = UIImage(named: "artButton")
                indexPathSelected = indexPath
            case 4:
                cell.imagenBoton.image = UIImage(named: "notaGrisMenu")
            case 5:
                cell.imagenBoton.image = UIImage(named: "notaGrisMenu")
            default:
                print("Otherwise, do something else.")
            }
            
            return cell
        }
    }
    
    /*
    Se le llama cuando se pulsa el botoncito del ojo de la celda.
    - Parámetro: recibe el identificador del boton pulsado.
    */
    @objc func editGroupAction(sender: UIButton) {
        
        
        thumbnailIndex = sender.tag
        let previewController = QLPreviewController()
        previewController.dataSource = self
        previewController.delegate = self
        present(previewController, animated: true)
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    //ARQUICKLOOK
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
         return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        
        
        let url = Bundle.main.url(forResource: models[thumbnailIndex], withExtension: "usdz")!
        return url as QLPreviewItem

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {
            
      
        }else{
            
            
            if indexPathSelected != indexPath {
                //Añade la Categoria respecto a la posicion del ENUM
//                categoriaSeleccionada = Categoria(rawValue: indexPath.row)
                cambiarCategoria(num: indexPath.row)
                cambiarImagenCategoria(num: indexPath,collection: collectionView)
                
            }
        }
    }
    
    
    func cambiarCategoria(num: Int){
        
        switch num {
        case 0:
            categoriaSeleccionada = Categoria.nota
            numElementos = 1
            DispatchQueue.main.async{
              
                self.collectionView.reloadData()
            }
        case 1:
            print("caso\(num)")
        case 2:
            categoriaSeleccionada = Categoria.ARObjets
            numElementos = 10
            DispatchQueue.main.async{
                
                self.collectionView.reloadData()
            }
        case 3:
           print("caso\(num)")
        case 4:
            print("caso\(num)")
        case 5:
            print("caso\(num)")
        default:
            print("Otherwise, do something else.")
        }
    }
    
    func cambiarImagenCategoria(num: IndexPath, collection: UICollectionView){

        if let indexPathSelected = indexPathSelected {
            
            let cell = collection.cellForItem(at: indexPathSelected) as! menuVRCollectionViewCell
            print(indexPathSelected.row)
            print(cell)
            let imagen = UIImage(named: IconoGris[(indexPathSelected.row)])!
            cell.imagenBoton.image = imagen
            
            
            let cellSelected = collection.cellForItem(at: num) as! menuVRCollectionViewCell
            let imagenSelected = UIImage(named: IconoColor[num.row])!
            cellSelected.imagenBoton.image = imagenSelected
            self.indexPathSelected = num
        } 
    }

    

    // MARK: - View controller previewing delegate
    
    /// - Tag: ViewControllerForLocation
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {

        guard let indexPath = collectionView.indexPathForItem(at: location),
            let cell = collectionView.cellForItem(at: indexPath)
            else { return nil }
        
        // Enable blurring of other UI elements, and a zoom in animation while peeking.
        previewingContext.sourceRect = cell.frame
        // Create and configure an instance of the color item view controller to show for the peek.
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "PreviewControllerCustom") as? PreviewControllerCustom
            else { preconditionFailure("Expected a ColorItemViewController") }
        viewController.nombreTexto = models[indexPath.row]
        
        print("CHIN")
        return viewController
    }
    
    /// - Tag: ViewControllerToCommit
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        // Push the configured view controller onto the navigation stack.
        
        print("PUM")
    }
}

