//
//
//  Created by Kevin Sabajanes García-Fraile on 10/5/19.
//  Copyright © 2019 Kevin Sabajanes García-Fraile. All rights reserved.
//
//

import UIKit
import MapKit
import SceneKit
import ARKit

class VrMode: UIViewController, ARSCNViewDelegate  {



    @IBOutlet weak var mapView: MKMapView!{
        
        didSet{
            
            self.mapView.layer.cornerRadius = 20
        }
        
    }
    @IBOutlet weak var sceneView: ARSCNView!
  let regionRadius: CLLocationDistance = 1000
    
    var locationManager = CLLocationManager()
    private var currentCoordinate: CLLocationCoordinate2D?

    @IBOutlet weak var nolightimage: UIImageView!
    // MARK: - View life cycle

    @IBOutlet weak var badAccuracyImagen: UIImageView!
    @IBOutlet weak var gpsTest: UILabel!
    override func viewDidLoad() {
    super.viewDidLoad()
    
    // Set the view's delegate
    sceneView.delegate = self
    
    // Show statistics such as fps and timing information
    sceneView.showsStatistics = true
    
    // Create a new scene
    let scene = SCNScene(named: "art.scnassets/Collab3Kitchen.dae")!
    
        let image = UIImage(named: "nota")
        let node = SCNNode(geometry: SCNPlane(width: 1, height: 1))
        node.geometry?.firstMaterial?.diffuse.contents = image
    // Set the scene to the view
    sceneView.scene = scene
    mapView.delegate = self
      
    
    configureLocationServices()
    // Do any additional setup after loading the view, typically from a nib.
    
    //Zoom to user location
    if let userLocation = locationManager.location?.coordinate {
        let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 100, longitudinalMeters: 100)
        mapView.setRegion(viewRegion, animated: false)
    }
    
    DispatchQueue.main.async {
        self.locationManager.startUpdatingLocation()
    }

  }
    
    

    @IBAction func volver(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
        
    }
    
    // MARK: - ARSCNViewDelegate
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func configureLocationServices() {
        locationManager.delegate = self
//        let status = CLLocationManager.authorizationStatus()
//
//        if status == .notDetermined {
//            locationManager.requestAlwaysAuthorization()
//        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
//            beginLocationUpdates(locationManager: locationManager)
//        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    private func zoomToLatestLocation(with coordinate: CLLocationCoordinate2D) {
        let zoomRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: 100, longitudinalMeters: 100)
        mapView.setRegion(zoomRegion, animated: true)
    }
    
    private func addAnnotations() {
        
        let appleParkAnnotation = MKPointAnnotation()
        appleParkAnnotation.title = "Apple Park"
        appleParkAnnotation.coordinate = CLLocationCoordinate2D(latitude: 40.00775508470332 , longitude:-3.571092076599598)
        
        let ortegaParkAnnotation = MKPointAnnotation()
        ortegaParkAnnotation.title = "Ortega Park"
        ortegaParkAnnotation.coordinate = CLLocationCoordinate2D(latitude:    40.008174192835185 , longitude: -3.5709351673722267)
        
        mapView.addAnnotation(appleParkAnnotation)
        mapView.addAnnotation(ortegaParkAnnotation)
    }
}

extension VrMode: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did get latest location")

        if let horizontal = locations.first?.horizontalAccuracy{
            if let vertical = locations.first?.verticalAccuracy{
                
                gpsTest.text = "Precision \(horizontal) | \(vertical)"
                
                if horizontal <= 10.0{
                    
                    badAccuracyImagen.isHidden = true
                 
                }else{
                    
                    badAccuracyImagen.isHidden = false
                    let pulse1 = CASpringAnimation(keyPath: "transform.scale")
                    pulse1.duration = 0.6
                    pulse1.fromValue = 1.0
                    pulse1.toValue = 1.12
                    pulse1.autoreverses = true
                    pulse1.repeatCount = 1
                    pulse1.initialVelocity = 0.5
                    pulse1.damping = 0.8
                    
                    let animationGroup = CAAnimationGroup()
                    animationGroup.duration = 2.7
                    animationGroup.repeatCount = 1000
                    animationGroup.animations = [pulse1]
                    
                    badAccuracyImagen.layer.add(animationGroup, forKey: "pulse")
                }
            }
        }
        guard let latestLocation = locations.first else { return }
        
        if currentCoordinate == nil {
            zoomToLatestLocation(with: latestLocation.coordinate)
            addAnnotations()
        }
        
        currentCoordinate = latestLocation.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("The status changed")
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
           
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        guard let lightEstimate = self.sceneView.session.currentFrame?.lightEstimate else { return }
        
        let ambientLightEstimate = lightEstimate.ambientIntensity
        
        let ambientColourTemperature = lightEstimate.ambientColorTemperature
        
        
        print(
            """
            Current Light Estimate = \(ambientLightEstimate)
            Current Ambient Light Colour Temperature Estimate = \(ambientColourTemperature)
            """)
        
        
        if ambientLightEstimate < 150 {
            DispatchQueue.main.async {
                self.nolightimage.isHidden = false
                let pulse1 = CASpringAnimation(keyPath: "transform.scale")
                pulse1.duration = 0.6
                pulse1.fromValue = 1.0
                pulse1.toValue = 1.12
                pulse1.autoreverses = true
                pulse1.repeatCount = 1
                pulse1.initialVelocity = 0.5
                pulse1.damping = 0.8
                
                let animationGroup = CAAnimationGroup()
                animationGroup.duration = 2.7
                animationGroup.repeatCount = 1000
                animationGroup.animations = [pulse1]
                
                self.nolightimage.layer.add(animationGroup, forKey: "pulse")
                print("Lighting Is Too Dark")
            
             }
        }else{
            DispatchQueue.main.async {
                
                self.nolightimage.isHidden = true
            }

        }
        
    }
}

extension VrMode: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if let title = annotation.title, title == "Apple Park" {
            annotationView?.image = UIImage(named: "nota")
        } else if let title = annotation.title, title == "Ortega Park" {
            annotationView?.image = UIImage(named: "elementovr")
        }
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("The annotation was selected: \(String(describing: view.annotation?.title))")
    }
}


