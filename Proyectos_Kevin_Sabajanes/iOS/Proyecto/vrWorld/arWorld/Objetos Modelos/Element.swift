//
//  Element.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 13/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

class Element
{
    enum Categoria
    {
        case nota
        case dibujar
        case objectos3D
        case cuadro
        case caso5
        case caso6
    }
}
