//
//  JsonParser.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 26/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import SwiftyJSON


//Se encarga de parsear la informacion en JSON
class JsonParser {
    
    //Delegado del controlador al que le enviaremos la informacion una vez obtenida y formateada
    var delegate: MapDelegateFirebase?
    
     func parsearJsonMapa(jsonNSData: NSObject){
     
        do {
            //Crea un objeto JSON a partir de un NSObject obtenido de FIREBASE (snapshot.value as? NSDictionary)
            let jsonObj = JSON(jsonNSData)
                //Localizaciones del rango de accion obtenido
                let resultados = jsonObj["l"]
                var elementosMapa: [ElementoMapa] = []
                //Creamos los objetos elemento para almacenarlo en un array
                for resultado in resultados {
                    print("****")
                    let latitude = resultado.1["la"].doubleValue
                    let longitude = resultado.1["lo"].doubleValue
                    let icono = resultado.1["E"].intValue
                    print(icono)
                    //Se añade cada elemento
                    elementosMapa.append((ElementoMapa(latitud: latitude, longitud: longitude, icono: icono)))
                    
                }
            //Le mandamos los elementos obtenidos al MainMapController
            delegate?.didReceiveData(elementosMapa)

        } catch {
            print("Error")
        }
    }
}
