//
//  drawing.swift
//  arWorld
//
//  Created by Kevinsan on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation


public struct paletaDibujar{
    
    var nombre: String
    var dificultad: String
    var objetos = [String]()
    var nombreObjetos = [String]()
    var segundosBusqueda: Int
    var desbloqueado: Bool = false
    
    public init(nombre: String, dificultad: String, objetos: [String], nombreObjetos: [String], segundosBusqueda: Int, desbloqueado: Bool) {
        self.nombre = nombre
        self.dificultad = dificultad
        self.objetos = objetos
        self.nombreObjetos = nombreObjetos
        self.segundosBusqueda = segundosBusqueda
        self.desbloqueado = desbloqueado
    }
}
