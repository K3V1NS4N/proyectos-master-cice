//
//  paletaDibujarDatos.swift
//  arWorld
//
//  Created by Kevinsan on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct paletaDatos {
    public static var paletas: [lugar] = [lugar(nombre: "Casa", dificultad: "Facil", objetos: ["Almohada","Armario","Interruptor","Lampara","Lavabo","Mando","Mesa","Plancha","Television","Ventana"], nombreObjetos: ["pillow","wardrobe","switch","lamp","washbasin","remote control","desk","iron","television","window"], segundosBusqueda: 15, desbloqueado: true)]
}
