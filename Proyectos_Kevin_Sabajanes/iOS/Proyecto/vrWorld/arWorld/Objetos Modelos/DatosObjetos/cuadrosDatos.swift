//
//  cuadrosDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 05/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct CuadrosDatos {
    
    public static var cuadros: [Cuadro] = [Cuadro(nombre: "Normal", nombreIcono: "cuadroMiniatura", nombre3DModel: "untitled.dae",herramientas: Herramientas(iconosInferiores: ["takePicture","reload"],miniatura: "cuadroMiniatura", herramienta2Estados: nil, colores: nil, pinceles: nil))]
}
