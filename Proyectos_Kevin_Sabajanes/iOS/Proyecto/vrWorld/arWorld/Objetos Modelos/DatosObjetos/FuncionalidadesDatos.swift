//
//  FuncionalidadesDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation


public struct FuncDatos {

    public static var funcionalidad: [Funcionalidad] = [Funcionalidad(nombre: "animatable",color: #colorLiteral(red: 1, green: 0.6189445853, blue: 0.6174275279, alpha: 1)),
    Funcionalidad(nombre: "customize",color: #colorLiteral(red: 0.6777824759, green: 0.8537853956, blue: 0.9981455207, alpha: 1)),
    Funcionalidad(nombre: "interaction",color: #colorLiteral(red: 1, green: 0.7910110354, blue: 0.5908911228, alpha: 1)),
    Funcionalidad(nombre: "sound",color: #colorLiteral(red: 0.6903143525, green: 0.9854139686, blue: 0.8851040006, alpha: 1)),
    Funcionalidad(nombre: "stadistics",color: #colorLiteral(red: 1, green: 0.9257304072, blue: 0.6906376481, alpha: 1)),
    Funcionalidad(nombre: "time",color: #colorLiteral(red: 0.810831964, green: 0.7652289271, blue: 0.9964482188, alpha: 1)),
    Funcionalidad(nombre: "pet",color: #colorLiteral(red: 1, green: 0.7530116439, blue: 0.9956466556, alpha: 1)),
    Funcionalidad(nombre: "level",color: #colorLiteral(red: 0.8522671461, green: 0.9740851521, blue: 0.7005500197, alpha: 1))]
    
}
