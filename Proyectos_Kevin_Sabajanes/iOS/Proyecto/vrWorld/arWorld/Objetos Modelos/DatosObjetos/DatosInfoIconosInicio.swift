//
//  DatosInfoIconosInicio.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct DatosInfoElementosInicio {
    
    public static var infoElementos: [InfoElementoInicio] = [InfoElementoInicio(nombre: "Nota", nombreIcono: "NotaInfo", descripcion: "Coloca una nota en cualquier superficie plana, para que tus amigos o cualquier persona que pase por ahí pueda leerla."),
    InfoElementoInicio(nombre: "Dibujo", nombreIcono: "DibujarInfo", descripcion: "Dibuja lo que quieras donde quieras, demuestra a los demas que tu creatividad no tiene limites tu entorno es tu lienzo."),
    InfoElementoInicio(nombre: "AR Element", nombreIcono: "ARelementInfo", descripcion: "Se trata de un elemento tridimensional, hay una gran variedad de estos elementos que pueden ir desde objetos inanimados hasta personajes o mascotas que cobran vida."),
    InfoElementoInicio(nombre: "Cuadro", nombreIcono: "CuadroInfo", descripcion: "Hazte un selfie y colocalo en cualquier pared de tu entorno, para que otras personas o solo tus amigos puedan verlo."),
    InfoElementoInicio(nombre: "Lugar", nombreIcono: "SiteInfo", descripcion: "Te ofrece información actualizada de lugares de tu entorno (En desarrollo)."),
    InfoElementoInicio(nombre: "Tienda", nombreIcono: "TiendaInfo", descripcion: "La tienda, en ella podrás adquirir todos los elementos y funcionalidades de este mundo siempre y cuando poseas una gran cantidad de Polys."),
    InfoElementoInicio(nombre: "Polys", nombreIcono: "PolyInfo", descripcion: "Se trata de la moneda de este mundo, con ella puedes adquirir cosas en la tienda, se consigue de multiples maneras, descubrelas.")]
    
}
