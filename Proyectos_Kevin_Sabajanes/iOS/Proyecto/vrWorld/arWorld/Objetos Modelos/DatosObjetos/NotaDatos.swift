//
//  notaDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 11/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct NotasDatos {
    
    public static var notas: [Nota] = [Nota(nombre: "Nota básica", nombresTexturas: ["amarilla","azul","verde","rosa"], nombreIcono: "note", nombre3DModel: "nota.dae", herramientas: Herramientas(iconosInferiores: ["editText","color","rotateRight","rotateLeft","reload"],miniatura: "notaMiniatura",texturas: ["notaAmarilla","notaAzul","notaVerde","notaRosa"]), texto: "NoTiene")]
}
