//
//  paletaDibujarDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation


public struct PaletaDatos {
    
    public static var paletas: [PaletaDibujar] = [PaletaDibujar(nombre: "Paleta Básica", herramientas: Herramientas(iconosInferiores: ["3D","colors","brush","reload"],miniatura: "artpintar", herramienta2Estados: nil, colores: [#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1),#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0, green: 0.7589928026, blue: 0.9568627451, alpha: 1),#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 1, green: 0.1025861391, blue: 0, alpha: 1),#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1),#colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1),#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1),#colorLiteral(red: 0, green: 0.9255362153, blue: 0.671253575, alpha: 1),#colorLiteral(red: 0.9255362153, green: 0.4644031429, blue: 0.8933646131, alpha: 1)], pinceles: [PincelDatos.pinceles[0],PincelDatos.pinceles[1]]), desbloqueado: true)]
    
}
