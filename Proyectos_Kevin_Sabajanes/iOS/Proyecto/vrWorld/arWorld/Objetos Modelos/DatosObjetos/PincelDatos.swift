//
//  PincelDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 10/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct PincelDatos {
    
    public static var pinceles: [Pincel] = [Pincel(nombre: "Normal",iconoImagen: "pincelNormal"),
                                                        Pincel(nombre: "Glow",iconoImagen: "pincelGradienteAnimado")]
    
}
