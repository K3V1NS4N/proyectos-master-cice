//
//  Funcionalidad.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
public struct Funcionalidad{
    
    var nombre: String
    var color: UIColor

    
    public init(nombre: String, color: UIColor) {
        self.nombre = nombre
        self.color = color
 
    }
}
