//
//  Pincel.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 10/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct Pincel{
    
    var nombre: String
    var iconoImagen: String
   
    
    public init(nombre: String, iconoImagen: String) {
        
        self.nombre = nombre
        self.iconoImagen = iconoImagen
        
    }
}

