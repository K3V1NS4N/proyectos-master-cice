//
//  seccionAjuste.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 02/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Es el objeto del nombre y la descripccion de las categorias dentro de la pantalla de ajustes
public struct SeccionAjuste{
    
    var nombre: String
    var descripcion: String
        
    public init(nombre: String,
                descripcion: String) {
        
        self.nombre = nombre
        self.descripcion = descripcion
        
    }
}
