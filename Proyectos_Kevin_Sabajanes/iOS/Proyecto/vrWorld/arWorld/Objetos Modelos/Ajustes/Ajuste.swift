//
//  Ajustes.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 30/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Crea un ajuste dentro de la pantalla de ajustes
public struct Ajuste{
    
    var ajuste: (String,String)
    var categoriasAjustes: Int
    var tipoCelda: TipoCelda.nombre

     init(ajuste: (String,String), categoriasAjustes: Int, tipoCelda: TipoCelda.nombre) {
        self.ajuste = ajuste
        self.categoriasAjustes = categoriasAjustes
        //Tipo de celda de la ventana de ajustes (Switch o picker)
        self.tipoCelda = tipoCelda
    }
}
