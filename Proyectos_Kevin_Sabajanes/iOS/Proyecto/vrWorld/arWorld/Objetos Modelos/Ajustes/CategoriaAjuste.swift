//
//  Ajuste.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

//Objeto categorias de ajustes
public struct CategoriaAjuste{

    var nombresApartado: String
    var ajustes = [Ajuste]()
    var secciones = [SeccionAjuste]()
    var ajustesIcono: String
    
    public init(nombresApartado: String, ajustes: [Ajuste], secciones: [SeccionAjuste], ajustesIcono: String) {
        self.nombresApartado = nombresApartado
        self.ajustes = ajustes
        self.secciones = secciones
        self.ajustesIcono = ajustesIcono
    }
    
    // Sirve para crear un objeto restaurante con incializadores vacios
    init(){
        
        self.init(nombresApartado: "", ajustes: [Ajuste](), secciones: [SeccionAjuste](), ajustesIcono: "")
        
    }
}
