//
//  ajustesDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Datos del menu de seleccion de ajustes
public struct CategoriasAjustesDatos {
    private var iconos = ["security","notification","map","world","batery","info"]
    public static var categoriaAjustes: [CategoriaAjuste] = [CategoriaAjuste(nombresApartado: "Privacidad y seguridad", ajustes: AjustesDatos.seguridad, secciones: [Secciones.SeccionPrivacidadElementos],ajustesIcono: "security"),
    CategoriaAjuste(nombresApartado: "Notificaciones", ajustes: AjustesDatos.notificaciones, secciones: [Secciones.SeccionNotificacionesElementos],ajustesIcono: "notification"),
    CategoriaAjuste(nombresApartado: "Mapa", ajustes: AjustesDatos.seguridad, secciones: [Secciones.SeccionPrivacidadElementos],ajustesIcono: "map"),
    CategoriaAjuste(nombresApartado: "Lenguaje", ajustes: AjustesDatos.seguridad, secciones: [Secciones.SeccionPrivacidadElementos],ajustesIcono: "world"),
    CategoriaAjuste(nombresApartado: "Bateria", ajustes: AjustesDatos.seguridad,secciones: [Secciones.SeccionPrivacidadElementos],ajustesIcono: "batery"),
    CategoriaAjuste(nombresApartado: "Información", ajustes: AjustesDatos.seguridad,secciones: [Secciones.SeccionPrivacidadElementos],ajustesIcono: "info")]
    
}
