//
//  File.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 02/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Secciones de los ajustes
public struct Secciones {

    public static var SeccionPrivacidadElementos = SeccionAjuste(nombre: "Privacidad elementos", descripcion: "Te permite ajustar la privacidad a la hora compartir elementos tridimensionales con los demás.")
    
    public static var SeccionNotificacionesElementos = SeccionAjuste(nombre: "Notificaciones", descripcion: "Te permite ajustar las notificaciones al pasar cerca de un elemento tridimensional.")

}
