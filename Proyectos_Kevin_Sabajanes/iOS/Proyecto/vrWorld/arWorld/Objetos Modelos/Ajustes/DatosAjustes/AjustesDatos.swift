//
//  AjustesDatos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 30/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Datos de los ajustes
public struct AjustesDatos {
    

    public static var seguridad: [Ajuste] = [Ajuste(ajuste: ("Ajuste11" , "Privacidad elementos"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker),
                                                     Ajuste(ajuste: ("Ajuste12" , "Mostrar foto de perfil"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste13" , "Cambiar contraseña"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker)]
    
    public static var notificaciones: [Ajuste] = [Ajuste(ajuste: ("Ajuste21" , "Segundo Plano"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste22" , "Vibración"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste23" , "Sonido"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste24" , "Notificaciones de"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker)]
    
    public static var mapa: [Ajuste] = [Ajuste(ajuste: ("Ajuste31" , "Privacidad elementos"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker),
                                                     Ajuste(ajuste: ("Ajuste32" , "Mostrar foto de perfil"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste33" , "Cambiar contraseña"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker)]
    
    
    public static var lenguaje: [Ajuste] = [Ajuste(ajuste: ("Ajuste41" , "Privacidad elementos"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker),
                                                     Ajuste(ajuste: ("Ajuste42" , "Mostrar foto de perfil"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste43" , "Cambiar contraseña"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker)]
    
    
    public static var bateria: [Ajuste] = [Ajuste(ajuste: ("Ajuste51" , "Privacidad elementos"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker),
                                                     Ajuste(ajuste: ("Ajuste52" , "Mostrar foto de perfil"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste53" , "Cambiar contraseña"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker)]
    
    public static var informacion: [Ajuste] = [Ajuste(ajuste: ("Ajuste61" , "Privacidad elementos"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker),
                                                     Ajuste(ajuste: ("Ajuste62" , "Mostrar foto de perfil"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.swich),
                                                     Ajuste(ajuste: ("Ajuste63" , "Cambiar contraseña"), categoriasAjustes: 0,tipoCelda: TipoCelda.nombre.picker)]
    
    
    
}
