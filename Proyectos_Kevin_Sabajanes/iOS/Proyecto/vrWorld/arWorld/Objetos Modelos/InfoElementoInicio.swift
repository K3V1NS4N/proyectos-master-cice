//
//  InfoElementoInicio.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Se encarga de almacenar la informacion la pantalla informativa de los elementos del comienzo.
public class InfoElementoInicio{
    
    var nombre: String
    var nombreIcono: String
    var descripcion: String

    public init(nombre: String, nombreIcono: String, descripcion: String) {
        self.nombre = nombre
        self.nombreIcono = nombreIcono
        self.descripcion = descripcion
    }
}
