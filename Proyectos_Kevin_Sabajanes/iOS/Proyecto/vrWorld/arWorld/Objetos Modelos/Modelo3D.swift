//
//  Modelo3D.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

public struct Modelo3D{
    
    var nombre: String
    var iconoImagen: String
    var archivo3D: String
    var funcionalidades = [Funcionalidad]()
    var herramientas = Herramientas()
    var altura: String
    var peso: String
    var personalidad: String
    var rareza: String
    var autor: String
    var desbloqueado: Bool
    var superficie: Int

    
    public init(nombre: String,
                iconoImagen: String,
                archivo3D: String,
                funcionalidades: [Funcionalidad],
                herramientas: Herramientas,
                superficie: Int,
                altura: String,
                peso: String,
                personalidad: String,
                rareza: String,
                autor: String,
                desbloqueado: Bool) {
        
        self.nombre = nombre
        self.iconoImagen = iconoImagen
        self.archivo3D = archivo3D
        self.funcionalidades = funcionalidades
        self.herramientas = herramientas
        self.superficie = superficie
        self.altura = altura
        self.peso = peso
        self.personalidad = personalidad
        self.rareza = rareza
        self.autor = autor
        self.desbloqueado = desbloqueado

    }
}
