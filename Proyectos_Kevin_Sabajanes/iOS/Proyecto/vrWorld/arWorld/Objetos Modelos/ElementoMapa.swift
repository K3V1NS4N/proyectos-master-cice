//
//  elementosMapa.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 26/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public struct ElementoMapa{
    
    var latitud: Double
    var longitud: Double
    var icono: Int
    
    
    public init(latitud: Double,
                longitud: Double,
                icono: Int) {
        self.latitud = latitud
        self.longitud = longitud
        self.icono = icono
        
    }
}
