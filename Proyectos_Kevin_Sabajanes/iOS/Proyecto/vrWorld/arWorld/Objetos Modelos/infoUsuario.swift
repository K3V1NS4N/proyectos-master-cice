//
//  infoUsuario.swift
//  arWorld
//
//  Created by Kevin Sabajane on 27/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

struct Info {
    static var userName = ""
    static var coins = 0
    static var bearing = 0.0
}
