//
//  PaletaDibujar.swift
//  arWorld
//
//  Created by Kevinsan on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

public struct PaletaDibujar{
    
    var nombre: String
    var iconoImagen: UIImage
    var colores = [UIColor]()
    var pinceles: [Pincel]
    var desbloqueado: Bool = false
    //Pantalla AR
    var herramientas = [String]()
    var herramienta2Estados: (String, String)
    
    public init(nombre: String, colores: [UIColor],pinceles: [Pincel], herramientas: [String], herramienta2Estados: (String, String), desbloqueado: Bool) {
        self.nombre = nombre
        self.iconoImagen = UIImage(named: "artpintar")!
        self.colores = colores
        self.pinceles = pinceles
        self.herramientas = herramientas
        self.herramienta2Estados = herramienta2Estados
        self.desbloqueado = desbloqueado
    }
}
