//
//  cuadro.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 05/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

public class Cuadro{
    
    var nombre: String
    var nombreIcono: String
    var nombre3DModel: String
    var herramientas = Herramientas()
    //Almacena que tipo de superficie va a tener que obtener 
    var superficie: Int = 1
    
    public init(nombre: String, nombreIcono: String,nombre3DModel: String, herramientas: Herramientas) {
        self.nombre = nombre
        self.nombreIcono = nombreIcono
        self.nombre3DModel = nombre3DModel
        self.herramientas = herramientas
    }
}
