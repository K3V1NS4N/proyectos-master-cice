//
//  Herramientas.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 12/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

public class Herramientas{
    
    let iconosComunes = ["botonAddVr","mapMode","home"]
    var iconosInferiores = [String]()
    var miniatura: String?
    var herramienta2Estados: (String, String)?
    var colores: [UIColor]?
    var pinceles: [Pincel]?
    var texturas = [String]()
    
    public init(iconosInferiores: [String], miniatura: String, herramienta2Estados: (String, String)?, colores: [UIColor]?,pinceles: [Pincel]?) {
        self.iconosInferiores = iconosInferiores
        self.miniatura = miniatura
        self.herramienta2Estados = herramienta2Estados
        self.colores = colores
        self.pinceles = pinceles
    }
    public init(iconosInferiores: [String], miniatura: String, texturas: [String]) {
        self.iconosInferiores = iconosInferiores
        self.miniatura = miniatura
        self.texturas = texturas
    }
    public init() {

    }
}
