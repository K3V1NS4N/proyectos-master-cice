//
//  Nota.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 07/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
public struct Nota{
    
    var nombre: String
    var nombresTexturas = [String]()
    var nombreIcono: String
    var nombre3DModel: String
    var herramientas = Herramientas()
    var texto: String
    var superficie: Int = 1
    
    public init(nombre: String, nombresTexturas: [String],nombreIcono: String,nombre3DModel: String, herramientas: Herramientas, texto: String) {
        self.nombre = nombre
        self.nombresTexturas = nombresTexturas
        self.nombreIcono = nombreIcono
        self.nombre3DModel = nombre3DModel
        self.herramientas = herramientas
        self.texto = texto
    }
    
}

