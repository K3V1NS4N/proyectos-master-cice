//
//  PaletaDibujos.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 13/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

public struct PaletaDibujar{
    
    var nombre: String
    var iconoImagen: UIImage
    var desbloqueado: Bool = false
    var superficie: Int = 2
    //Pantalla AR
    var herramientas = Herramientas()
    
    public init(nombre: String, herramientas: Herramientas, desbloqueado: Bool) {
        self.nombre = nombre
        self.iconoImagen = UIImage(named: "artpintar")!
        self.herramientas = herramientas
        self.desbloqueado = desbloqueado
    }
}
