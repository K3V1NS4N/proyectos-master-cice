//
//  Funcionalidades.swift
//  arWorld
//
//  Created by Kevinsan on 09/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

public struct Funcionalidad{
    
    var nombre: String
    var iconoImagen: UIImage
    var colores  = [UIColor]()
    var desbloqueado: Bool = false
    //Pantalla AR
    var herramientas = [String]()
    var herramienta2Estados: (String, String)
    
    public init(nombre: String, colores: [UIColor], herramientas: [String], herramienta2Estados: (String, String), desbloqueado: Bool) {
        self.nombre = nombre
        self.iconoImagen = UIImage(named: "artpintar")!
        self.colores = colores
        self.herramientas = herramientas
        self.herramienta2Estados = herramienta2Estados
        self.desbloqueado = desbloqueado
    }
}
