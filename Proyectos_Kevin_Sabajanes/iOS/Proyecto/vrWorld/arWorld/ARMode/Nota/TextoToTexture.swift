//
//  TextoToTexture.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 01/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

class TextoToTexture {

    //Se encarga de añadir un texto a una imagen, en este caso a la textura de la nota
    class func GetImage(drawText text: NSString, inImage image: UIImage) -> UIImage {
        
        //Crea un contexto gráfico basado en mapas de bits y lo convierte en el contexto actual a patir del UIImage por parametro
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        
        //Atributos del texto
        let font = UIFont(name: "Helvetica-Bold", size: 20)!
        let text_style = NSMutableParagraphStyle()
        //        text_style.alignment=NSTextAlignment.center
        let text_color = UIColor.black
        let attributes = [NSAttributedString.Key.font:font,
                          NSAttributedString.Key.paragraphStyle:text_style,
                          NSAttributedString.Key.foregroundColor:text_color]
        
        //Añadimos el texto en la parte correspondiente para que quede bien
        let text_h = font.lineHeight
        let text_y = (image.size.height-text_h)/1.8
        let text_rect = CGRect(x: 120, y: text_y, width: image.size.width-300, height: 150)
        text.draw(in: text_rect.integral, withAttributes: attributes)
        //Devuelve una imagen basada en el contenido del contexto gráfico actual basado en mapas de bits.
        let result=UIGraphicsGetImageFromCurrentImageContext()
        //Elimina el contexto gráfico actual basado en mapas de bits de la parte superior del stack.
        UIGraphicsEndImageContext()
        return result!
    }
}
