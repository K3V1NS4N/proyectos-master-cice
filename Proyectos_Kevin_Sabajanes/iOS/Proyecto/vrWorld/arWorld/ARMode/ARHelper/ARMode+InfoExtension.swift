//
//  ARMode+InfoExtension.swift
//  arWorld
//
//  Created by Kevinsan on 11/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//
import ARKit

extension ARCamera.TrackingState {
    var presentationString: String {
        switch self {
        case .notAvailable:
            return "No es posible detectar una superficie"
        case .normal:
            return "Rastreo Normal"
        case .limited(.excessiveMotion):
            return "Rastreo Limitado: Demasiado movimiento"
        case .limited(.insufficientFeatures):
            return "Rastreo Limitado: Bajo Detalle"
        case .limited(.initializing):
            return "Incializando"
        case .limited(.relocalizing):
            return "Recuperandose de la interrupción"
        }
    }
    
    var recommendation: String? {
        switch self {
        case .limited(.excessiveMotion):
            return "Intente reducir el movimiento o reinicie la sesión."
        case .limited(.insufficientFeatures):
            return "Intente apuntar a una superficie plana o reinicie la sesión."
        case .limited(.relocalizing):
            return "Vuelva al lugar donde lo dejó o intente restablecer la sesión."
        default:
            return nil
        }
    }
}
