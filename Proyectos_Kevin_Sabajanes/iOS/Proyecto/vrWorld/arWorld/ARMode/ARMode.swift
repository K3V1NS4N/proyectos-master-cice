//
//  *********************************************************************
//  Created by Kevin Sabajanes García-Fraile on 10/5/19.
//  Copyright © 2019 Kevin Sabajanes García-Fraile. All rights reserved.
//  *********************************************************************
//

import UIKit
import MapKit
import SceneKit
import ARKit

class ARMode: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    // MARK: **************** [DIBUJAR] ****************
    //Tamaño por defecto del pincel
    var lineWidth:Double = 0.005
    //Variables para la creación de nodos para dibujar
    var currentNode:MQNode?
    private var rootNodeVaule:SCNNode? = nil
    var rootNode:SCNNode! {
        get {
            if rootNodeVaule == nil {
                rootNodeVaule = SCNNode()
                sceneView.scene.rootNode.addChildNode(rootNodeVaule!)
            }
            return rootNodeVaule
        }
        set {
            rootNodeVaule = newValue
        }
    }
    //Variable de espera para reducir el numero de nodos durante el trazo
    var espera = false
    // FIXME: Mejorar la manera de gestionar esto
    var colores = false
    var trazos = false
    var gradienteAnimado = false
    //Colorines para los trazos
    let colorArray = PaletaDatos.paletas[0].herramientas.colores
    var colorEscogido = 4
    var modoAnimado = false
    var modo3DTouch = false
    var modo3D = false
    private var coloresBarraDerecha: [UIColor]?
    @IBOutlet weak var tamañoPincel: UISlider!
    // MARK: **************** [NOTAS] ****************
    //Es la textura modificable que se le asigna a las notas
    var texturaNota: UIImage?
    var texturaSeleccionada = 0
    @IBOutlet weak var texto: UITextField!
    var notaAñadida: Bool = false
    
    // MARK: **************** [CUADRO] ****************
    var texturaCuadro: UIImage?
    
    
    // MARK: **************** [MAPA] ****************
    // Localizacion mini mapa
    var locationManager = CLLocationManager()
    //Imagen cobertura
    @IBOutlet weak var badAccuracyImagen: UIImageView!
    //MINI Mapa
    @IBOutlet weak var botonInvisibleMapa: UIButton!
    @IBOutlet weak var mapView: MKMapView!{
        didSet{
            self.mapView.layer.cornerRadius = 25
        }
    }
    
    // MARK: **************** [AR] ****************
    
    //Escena AR
    @IBOutlet weak var sceneView: ARSCNView!
    //Determina la categoria de herramienta que se van a cargar
    var herramientasObjectoSeleccionado = PaletaDatos.paletas[0].herramientas
    //Determina el tipo de superficie que tiene que cargar (0->Horizontal,1->vertical,2->ambos)
    var superficie = -1
    //Es el elemento 3D (Nota, Muñeco) que se ha seleccionado y cargado para poscionarlo
    var Elemento3DSeleccionado: SCNScene? = nil
    // TODO: No tiene funcioanlidad
    @IBOutlet weak var progreso: UIProgressView!
    //Es la categoria que se ha seleccionado
    var tipoElementoSeleccionado: Element.Categoria? = nil
    //Es el numero del elemento de la categoria seleccionada
    var numElementoSeleccionado: Int = 0
    //Es la variable a la que se le asigna el modelo 3D para almacenarlo temporalmente
    var nodeActual: SCNNode?
    //Rejillas
    var planos = [Plano]()
    var planoDetectado = false
    var movido = false
    var dibujado = false
    var fotoTomada = false
    
  

    
    
    // Se encarga de mostrar la información del estado de la sesion AR
    @IBOutlet weak var labelInformation: UILabel!
    //Imagen luz insuficiente
    @IBOutlet weak var nolightimage: UIImageView!
    //Imagen que muestra los gestos que debes hacer
    @IBOutlet weak var gestureAnimation: UIImageView!
    //Imagen que muestra que debes detectar el entorno
    @IBOutlet weak var animacionBuscarSuperficie: UIImageView!
    //Comprueba que se ha añadido el elemento 3D
    var añadido: Bool = false
    //Icono que muestra que estas suficientemente cerca
    @IBOutlet weak var ok: UIImageView!
    @IBOutlet weak var mensajeGestures: UILabel!
    @IBOutlet weak var arrowAnimation: UIImageView!
    
    
    
    
    // MARK: **************** [INTERFAZ] ****************
    //Info AR
    
    var imagenAbierta: Bool = false
    @IBOutlet weak var openImageLocation: UIButton!
    @IBOutlet weak var infoAr: viewDesignable!
    @IBOutlet weak var arPickerController: UIButton!
    //Comprueba que has abierto una herramienta del menú
    var flechaActivada = false
    //Es el numero de elementos que hay a la derecha del menu (Pueden variar)
    var numElementosBarraDerecha = 4
    //Son los iconos que aparecen en cualquier modo elegido
    private var iconosComuneBarraDerecha = ["botonAddVr","upload","mapMode","home"]
    //Son los iconos que pueden variar
    private var iconosBarraDerecha = ["botonAddVr","upload","mapMode","home"]
    //Variable que comprueba si el menú de herramientas se ha abierto o no
    var menuAbierto:Bool = false
    
    // MARK: **************** [OutletDebugger] ****************
    @IBOutlet weak var lightDebug: UILabel!
    @IBOutlet weak var planeWDebug: UILabel!  
    @IBOutlet weak var planeHDebug: UILabel!
    @IBOutlet weak var planeDistance: UILabel!
    @IBOutlet weak var currentLocation: UILabel!
    @IBOutlet weak var nElementosMapa: UILabel!
    @IBOutlet weak var gpsPrecision: UILabel!
   
    
    // MARK: **************** [OutletsInterfaz] ****************
    @IBOutlet weak var vistaInvisibleCerrarTeclado: UIView!
    @IBOutlet weak var arPickerElements: UITableView!
    @IBOutlet weak var imagenElementoEscogido: UIImageView!
    @IBOutlet weak var herramientas: UIStackView!
    @IBOutlet weak var openCloseButton: UIButton!
    //Botones parte posterior del menú de herramientas
    @IBOutlet weak var tool1: UIButton!
    @IBOutlet weak var tool2: UIButton!
    @IBOutlet weak var tool3: UIButton!
    @IBOutlet weak var tool4: UIButton!
    @IBOutlet weak var tool5: UIButton!
    
    // MARK: **************** [Persistencia] ****************
    var isRelocalizingMap = false
    var virtualObjectAnchor: ARAnchor?
    let virtualObjectAnchorName = "virtualObject"
    //ARREGLAR
    var virtualObject: SCNNode = {
        guard let sceneURL = Bundle.main.url(forResource: "nota", withExtension: "dae", subdirectory: "art.scnassets/"),
            let referenceNode = SCNReferenceNode(url: sceneURL) else {
                fatalError("can't load virtual object")
        }
        referenceNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        referenceNode.load()
        
        return referenceNode
    }()
    
    // MARK: - Persistence: Saving and Loading
    lazy var mapSaveURL: URL = {
        do {
            return try FileManager.default
                .url(for: .documentDirectory,
                     in: .userDomainMask,
                     appropriateFor: nil,
                     create: true)
                .appendingPathComponent("map.arexperience")
        } catch {
            fatalError("Can't get file save URL: \(error.localizedDescription)")
        }
    }()
    //Imagen del lugar donde se ha colcoado el objeto
    @IBOutlet weak var snapshotThumbnail: UIImageView!{
        
        didSet{
            snapshotThumbnail.layer.cornerRadius = 15.0
            snapshotThumbnail.clipsToBounds = true
            self.snapshotThumbnail.layer.masksToBounds = true
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard ARWorldTrackingConfiguration.isSupported else {
            fatalError("""
                ARKit is not available on this device. For apps that require ARKit
                for core functionality, use the `arkit` key in the key in the
                `UIRequiredDeviceCapabilities` section of the Info.plist to prevent
                the app from installing. (If the app can't be installed, this error
                can't be triggered in a production scenario.)
                In apps where AR is an additive feature, use `isSupported` to
                determine whether to show UI for launching AR experiences.
            """) // For details, see https://developer.apple.com/documentation/arkit
        }
        
        //Cierra el teclado cuando pulsas fuera
        self.hideKeyboardWhenTappedAround()
        /* *************[DIBUJAR]************* */
        //Rota el slider encargado de variar el tamaño del pincel
        tamañoPincel.transform = CGAffineTransform(rotationAngle: (1.5708))
        
        /* *************[INTERFAZ]************* */
        openCloseButton.isHidden = true
        
        
        /* *************[AR]************* */
        //Configuracion escena -AR
        
        let scene = SCNScene()
        /*
            for direction in CardinalDirection.defaults {
                scene.rootNode.addChildNode(direction.sphere())
                scene.rootNode.addChildNode(direction.text())
            }
        */
        sceneView.scene = scene
        sceneView.delegate = self
        
        //Debug
        //sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        /* *************[MAP]************* */
        //Configuracion Mapa -MAP
        mapView.delegate = self
        configureLocationServices()
        
        //Configuracion localización -MAP
        if let userLocation = locationManager.location?.coordinate {
            //Mostramos la localizacion del usuario
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 60, longitudinalMeters: 60)
            mapView.setRegion(viewRegion, animated: false)
        }
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
        //Añadimos los iconos de los elementos en el mini mapa
        addIconElementMap()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Si existe un elemento entonces cargamos el elemento alamcenado
//        if mapDataFromFile != nil {
//            openImageLocation.isHidden = false
//            cargarElementoPersistente()
//        }
 
    }
    
    
    func arrowAnimationHelp(){
        
        if !fotoTomada {
        
            var distancia = 0
            if (menuAbierto){
                distancia = -60
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self.arrowAnimation.transform = CGAffineTransform.identity.translatedBy(x: CGFloat(distancia), y: 0)
                UIView.animate(withDuration: 0.8) {
                    
                    self.arrowAnimation.alpha = 1
                    self.arrowAnimation.transform = CGAffineTransform.identity.translatedBy(x: CGFloat(distancia), y: 70)
                    
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                    UIView.animate(withDuration: 0.5) {
                        
                        self.arrowAnimation.alpha = 0
                    }
                    self.arrowAnimationHelp()
                }
            }
        }
    }
    
    
    weak var shapeLayer: CAShapeLayer?

    func animacionGestosDibujar(){
        
        //https://stackoverflow.com/questions/42978418/draw-line-animated
        if !dibujado{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                let path = UIBezierPath()
                path.move(to: CGPoint(x: 200, y: 150))
                path.addCurve(to: CGPoint(x: 275, y: 375), controlPoint1: CGPoint(x: 200, y: 150), controlPoint2: CGPoint(x: -25, y: 302))
                
                let shapeLayer = CAShapeLayer()
                shapeLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
                shapeLayer.strokeColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1).cgColor
                shapeLayer.lineWidth = 7
                shapeLayer.path = path.cgPath
                shapeLayer.strokeStart = 1.0
                
                let startAnimation = CABasicAnimation(keyPath: "strokeStart")
                startAnimation.fromValue = 0
                startAnimation.toValue = 0.8
                
                let endAnimation = CABasicAnimation(keyPath: "strokeEnd")
                endAnimation.fromValue = 0.2
                endAnimation.toValue = 1.0
                
                let animation = CAAnimationGroup()
                animation.animations = [startAnimation, endAnimation]
                animation.duration = 1.25
                shapeLayer.add(animation, forKey: "MyAnimation")
            
                self.shapeLayer = shapeLayer
                self.view.layer.addSublayer(shapeLayer)
               
                
            }
            self.gestureAnimation.transform = CGAffineTransform(translationX: 0, y: -200)
          
                UIView.animate(withDuration: 1.5) {
                    self.mensajeGestures.text = "Traza con el dedo para comenzar a dibujar"
                    self.gestureAnimation.image = UIImage(named: "touch2")
                    self.gestureAnimation.alpha = 1
                    self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: -200, y: -20)
                    self.mensajeGestures.alpha = 1
                }
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 1.5) {
          
                    self.gestureAnimation.image = UIImage(named: "touch2")
                    self.gestureAnimation.alpha = 1
                    self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: 300, y: 0)
                    self.mensajeGestures.alpha = 1
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                UIView.animate(withDuration: 1.5) {
            
                    self.gestureAnimation.image = UIImage(named: "touch2")
                    self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: -100, y: 100)
                }
                UIView.animate(withDuration: 1.5) {
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                UIView.animate(withDuration: 1.0) {
                    self.mensajeGestures.alpha = 0
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                UIView.animate(withDuration: 1.0) {
                   
                    self.gestureAnimation.alpha = 0
           
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) { // Change `2.0` to the desired number of seconds.
               
                self.animacionGestosDibujar()
            }
        }
    }
    
    func animacionGestos(){
        if (!añadido){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) { // Change `2.0` to the desired number of seconds.
                
                self.gestureAnimation.transform = CGAffineTransform(translationX: 0, y: 0)
                UIView.animate(withDuration: 0.5) {
                    self.mensajeGestures.text = "Toca una superficie para colocar el objeto"
                    self.gestureAnimation.image = UIImage(named: "touch2")
                    self.gestureAnimation.alpha = 1
                    self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -20).scaledBy(x: 0.75, y: 0.75)
                    self.mensajeGestures.alpha = 1
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // Change `2.0` to the desired number of seconds.
                    
                    self.gestureAnimation.image = UIImage(named: "touch")
                    self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -24).scaledBy(x: 0.75, y: 0.75)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // Change `2.0` to the desired number of seconds.
                    UIView.animate(withDuration: 0.4) {
                        
                        self.gestureAnimation.alpha = 0
                        self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -24).scaledBy(x: 0.75, y: 0.75)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.animacionGestos()
                        }
                    }
                }
            }
        }else{
            self.mensajeGestures.alpha = 0
        }
    }
    
    func animacionBusquedaSuperficie(){
        
        let tiempo = 0.8
        if (!planoDetectado){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) { // Change `2.0` to the desired number of seconds.
                
                UIView.animate(withDuration: tiempo) {
                    self.mensajeGestures.alpha = 1
                    self.animacionBuscarSuperficie.transform = CGAffineTransform(translationX: 0, y: -100)
                    self.animacionBuscarSuperficie.alpha = 1
                    
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + tiempo) { //
                UIView.animate(withDuration: tiempo) {
                    self.animacionBuscarSuperficie.transform = CGAffineTransform(translationX: -150, y: -100)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + tiempo*2) { //
                
                UIView.animate(withDuration: tiempo) {
                    self.animacionBuscarSuperficie.transform = CGAffineTransform(translationX: -150, y:0)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + tiempo*3) { //
                UIView.animate(withDuration: tiempo) {
                    self.animacionBuscarSuperficie.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.animacionBuscarSuperficie.alpha = 0
                    self.mensajeGestures.alpha = 0
                    DispatchQueue.main.asyncAfter(deadline: .now() + tiempo + 1.0) { //
                        self.animacionBusquedaSuperficie()
                    }
                }
            }
            
        }
    }
    
    func animacionMover(){
        
        var contador = 0
        if !movido {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // Change `2.0` to the desired number of seconds.
                
                self.gestureAnimation.transform = CGAffineTransform(translationX: 50, y: 0)
                UIView.animate(withDuration: 0.5) {
                    self.mensajeGestures.text = "Deplaza el dedo por la pantalla para mover el objeto"
                    self.gestureAnimation.image = UIImage(named: "touch2")
                    self.gestureAnimation.alpha = 1
                    self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: -50, y: 0)
                    self.mensajeGestures.alpha = 1
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { //
                    UIView.animate(withDuration: 0.5) {
                        self.gestureAnimation.image = UIImage(named: "touch2")
                        self.gestureAnimation.transform = CGAffineTransform.identity.translatedBy(x: 50, y: 0)
                        self.mensajeGestures.alpha = 1
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.8) { //
                    UIView.animate(withDuration: 0.25) {
                
                        self.gestureAnimation.alpha = 0
                        self.mensajeGestures.alpha = 0
            
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { //
                    if (contador == 5) {
                        
                        self.movido = true
                        
                    }
                    contador += 1
                    
                    self.animacionMover()
                }
            }
        }
        
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        vistaInvisibleCerrarTeclado.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard() {
        vistaInvisibleCerrarTeclado.isHidden = true
        view.endEditing(true)
    }
    
    //Se encarga de esconder la barra de estado
    override var prefersStatusBarHidden: Bool{ return true }

    // MARK: **************** [IBAction] ****************
    
    /* *************[IBAction-DIBUJAR]************* */
    @IBAction func valorTamañoPincel(_ sender: UISlider) {
        lineWidth = Double((sender.value)/1000)
    }
    /* *************[IBAction-INTERFAZ]************* */
    //Botones parte posterior del menú de herramientas
    @IBAction func boton1(_ sender: Any) {
        
        if let elementoSeleccionado = tipoElementoSeleccionado{
            botonPulsado(numBoton: 1, nombreElemento: elementoSeleccionado)
        }
    }
    @IBAction func boton2(_ sender: Any) {
        if let elementoSeleccionado = tipoElementoSeleccionado{
            botonPulsado(numBoton: 2, nombreElemento: elementoSeleccionado)
        }
    }
    @IBAction func boton3(_ sender: Any) {
        if let elementoSeleccionado = tipoElementoSeleccionado{
            botonPulsado(numBoton: 3, nombreElemento: elementoSeleccionado)
        }
    }
    @IBAction func boton4(_ sender: Any) {
        if let elementoSeleccionado = tipoElementoSeleccionado{
            botonPulsado(numBoton: 4, nombreElemento: elementoSeleccionado)
        }
    }
    
    @IBAction func boton5(_ sender: Any) {
        if let elementoSeleccionado = tipoElementoSeleccionado{
            botonPulsado(numBoton: 5, nombreElemento: elementoSeleccionado)
        }
        imagenElementoEscogido.image = UIImage(named: "backArrow")
        
    }
    
    //Se encarga abrir ls lista de herramientas de la derecha, activa la flecha para volver atras.
    func activarFlecha(){
        imagenElementoEscogido.image = UIImage(named: "backArrow")
        flechaActivada = true
    }
    
    //Recarga el tableView de la derecha
    func recargarCeldasDerecha(){
        iconosBarraDerecha.removeAll()
        arPickerElements.reloadData()
    }
    
    //Borrar todos los elementos
    func reiniciarEscena(){
        //Borramos todos los elementos que se hayan podido insertar anteriormente
        self.sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            //Los que no tienen asignados un nombre tienden a ser luces o aquellos que no queremos borrar
            // TODO: Los trazos de dibujos no los borra
            print("elemento a borrar \(existingNode.name)")
            if existingNode.name != nil {
                existingNode.removeFromParentNode()
            }
        }
        
        nodeActual = nil
        añadido = false
        
        cargarElementoSeleccionado(nombreElemento: tipoElementoSeleccionado!, numElemento: numElementoSeleccionado)
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        configuration.environmentTexturing = .automatic
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        //Debug
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
    }
    
    
    //Se encarga de gestionar a que funcionalidad concreta corresponde cada boton dependiendo de la categoria & herramienta seleccionada.
    private func botonPulsado(numBoton: Int, nombreElemento: Element.Categoria){

        switch (numBoton,nombreElemento) {
            case (1,.nota):
                if notaAñadida {
                    //Abre el teclado el teclado
                    DispatchQueue.main.async {
                        self.vistaInvisibleCerrarTeclado.isHidden = false
                        self.texto.becomeFirstResponder()
                    }
                }
            case (1,.dibujar):
                    
                if modo3D{
                    tool1.setImage(UIImage(named: "3D"), for: .normal)
                    modo3D = false
                        
                }else{
                    tool1.setImage(UIImage(named: "2D"), for: .normal)
                    modo3D = true
                }
            case (1,.objectos3D):
                reiniciarEscena()
            case (1,.cuadro):
                hacerFoto()
            case (1,.caso5):
                print("a")
            case (1,.caso6):
                print("a")
            
            case (2,.nota):
                coloresBarraDerecha?.removeAll()
                tamañoPincel.isHidden = true
                let texturas = herramientasObjectoSeleccionado.texturas
                    print(texturas)
                    numElementosBarraDerecha = texturas.count
                    recargarCeldasDerecha()
                    for textura in texturas{
                        iconosBarraDerecha.append(textura)
                    }
                activarFlecha()
            case (2,.dibujar):
                colores = true
                if let colores = herramientasObjectoSeleccionado.colores {
                    numElementosBarraDerecha = colores.count
                    coloresBarraDerecha = colores
                }
                tamañoPincel.isHidden = true
                recargarCeldasDerecha()
                activarFlecha()
        
            case (2,.objectos3D):
                    print("a")
            case (2,.cuadro):
                    print("a")
            case (2,.caso5):
                    print("a")
            case (2,.caso6):
                    print("a")
            
            case (3,.nota):
                if let orientation = nodeActual?.orientation{
                    var glQuaternion = GLKQuaternionMake(orientation.x, orientation.y, orientation.z, orientation.w)
                    // Rotate around Z axis
                    let multiplier = GLKQuaternionMakeWithAngleAndAxis(0.05, 0, 1, 0)
                    glQuaternion = GLKQuaternionMultiply(glQuaternion, multiplier)
                    nodeActual?.orientation = SCNQuaternion(x: glQuaternion.x, y: glQuaternion.y, z: glQuaternion.z, w: glQuaternion.w)
                }
            case (3,.dibujar):
                coloresBarraDerecha?.removeAll()
                tamañoPincel.isHidden = false
                if let pinceles = herramientasObjectoSeleccionado.pinceles {
                    print(pinceles)
                    numElementosBarraDerecha = pinceles.count
                    recargarCeldasDerecha()
                    for pincel in pinceles{
                        iconosBarraDerecha.append(pincel.iconoImagen)
                    }
                        
                    trazos = true
                }
                activarFlecha()
            case (3,.objectos3D):
                reiniciarEscena()
            case (3,.cuadro):
                print("a")
            case (3,.caso5):
                print("a")
            case (3,.caso6):
                print("a")
            case (4,.nota):
                if let orientation = nodeActual?.orientation{
                    var glQuaternion = GLKQuaternionMake(orientation.x, orientation.y, orientation.z, orientation.w)
                    // Rotate around Z axis
                    let multiplier = GLKQuaternionMakeWithAngleAndAxis(-0.05, 0, 1, 0)
                    glQuaternion = GLKQuaternionMultiply(glQuaternion, multiplier)
                    nodeActual?.orientation = SCNQuaternion(x: glQuaternion.x, y: glQuaternion.y, z: glQuaternion.z, w: glQuaternion.w)
                }
            case (4,.dibujar):
                reiniciarEscena()
            case (4,.objectos3D):
                print("a")
            case (4,.cuadro):
                print("a")
            case (4,.caso5):
                print("a")
            case (4,.caso6):
                print("a")
            case (5,.dibujar):
                reiniciarEscena()
            case (5,.objectos3D):
                print("a")
            case (5,.cuadro):
                print("a")
            case (5,.caso5):
                print("a")
            case (5,.caso6):
                print("a")
            case (5,.nota):
                reiniciarEscena()
        default:
            print("Error")
        }
    }
    
    
    @IBAction func openLocationImage(_ sender: Any) {
        
        imagenAbierta = true
        snapshotThumbnail.isHidden = false

    }
    
    //Abre y cierra el las herramientas del modo AR
    @IBAction func openClosePicker(_ sender: Any) {
        
        if flechaActivada{
            
            coloresBarraDerecha?.removeAll()
            iconosBarraDerecha = iconosComuneBarraDerecha
            numElementosBarraDerecha = iconosComuneBarraDerecha.count
            imagenElementoEscogido.image = UIImage(named: "close")
            flechaActivada = false
            tamañoPincel.isHidden = true
            arPickerElements.reloadData()
            
        }else{
            if !menuAbierto{
                menuAbierto = true
                //Definimos el estado inicial
                //muebles.alpha = 0
                herramientas.isHidden = false
                arPickerElements.isHidden = false
                
                animarAlpha()
                imagenElementoEscogido.image = UIImage(named: "close")
                
                tool1.layer.transform = CATransform3DMakeTranslation(50, 0, 0)
                tool2.layer.transform = CATransform3DMakeTranslation(100, 0, 0)
                tool3.layer.transform = CATransform3DMakeTranslation(150, 0, 0)
                tool4.layer.transform = CATransform3DMakeTranslation(200, 0, 0)
                tool5.layer.transform = CATransform3DMakeTranslation(250, 0, 0)
                
                //Creamos la animación
                UIView.animate(withDuration: 0.5){
                    
                    //Lo vuelve a colocar donde deberia estar
                    self.tool1.layer.transform = CATransform3DIdentity
                    self.tool2.layer.transform = CATransform3DIdentity
                    self.tool3.layer.transform = CATransform3DIdentity
                    self.tool4.layer.transform = CATransform3DIdentity
                    self.tool5.layer.transform = CATransform3DIdentity
                    
                    //Definimos el estado final
                    self.tool1.alpha = 1.0
                    self.tool2.alpha = 1.0
                    self.tool3.alpha = 1.0
                    self.tool4.alpha = 1.0
                    self.tool5.alpha = 1.0
                }
                
            }else{
                
                if let iconoMiniatura = herramientasObjectoSeleccionado.miniatura{
                   imagenElementoEscogido.image = UIImage(named: iconoMiniatura)
                }

                menuAbierto = false
                //Creamos la animación
                UIView.animate(withDuration: 0.25){
                    //Lo vuelve a colocar donde deberia estar
                    self.tool1.layer.transform = CATransform3DMakeTranslation(50, 0, 0)
                    self.tool2.layer.transform = CATransform3DMakeTranslation(100, 0, 0)
                    self.tool3.layer.transform = CATransform3DMakeTranslation(150, 0, 0)
                    self.tool4.layer.transform = CATransform3DMakeTranslation(200, 0, 0)
                    self.tool5.layer.transform = CATransform3DMakeTranslation(250, 0, 0)
                    
                    //Definimos el estado final
                    self.tool1.alpha = 0.0
                    self.tool2.alpha = 0.0
                    self.tool3.alpha = 0.0
                    self.tool4.alpha = 0.0
                    self.tool5.alpha = 0.0
                    self.arPickerElements.alpha = 0.0
                }
            }
        }
    }
    
    /* *************[IBAction-VOLVER]************* */
    //se le llama al elegir un elemento en el menu de eleccion de elementos AR
    @IBAction func unwindPicker(segue:UIStoryboardSegue) {
        
        //Borramos todos los elementos que se hayan podido insertar anteriormente
        self.sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            //Los que no tienen asignados un nombre tienden a ser luces o aquellos que no queremos borrar
            if existingNode.name != nil {
                existingNode.removeFromParentNode()
            }
        }
    
        nodeActual = nil
        añadido = false
        mapView.isHidden = true
        badAccuracyImagen.isHidden = true
        botonInvisibleMapa.isHidden = true
        
        if segue.source is MenuPickerAR {
            if let senderVC = segue.source as? MenuPickerAR {
                tipoElementoSeleccionado = senderVC.categoriaSeleccionada
                numElementoSeleccionado = senderVC.numElementoSeleccionado!
                if let elementoSeleccionado = tipoElementoSeleccionado {
                    cargarElementoSeleccionado(nombreElemento: elementoSeleccionado, numElemento: numElementoSeleccionado)
                    print(elementoSeleccionado)
                    print(numElementoSeleccionado)
                }
            }
        }
        
        tool1.isHidden = true
        tool2.isHidden = true
        tool3.isHidden = true
        tool4.isHidden = true
        tool5.isHidden = true
        let herramientas = herramientasObjectoSeleccionado.iconosInferiores
        print(herramientas)
        let herramienta2Estados = herramientasObjectoSeleccionado.herramienta2Estados
        var unoCero = 0
        if herramienta2Estados != nil {
            unoCero = 1
        }
        //Recorremos todos los nombres almacenados en el elemento elegido para añadir los iconos correpondientes a sus botones
        for index in 0 ... herramientas.count-1+unoCero {
            switch index {
            case 0:
                tool1.isHidden = false
                if let herramienta2Estados = herramienta2Estados{
                    tool1.setImage(UIImage(named: herramienta2Estados.0), for: .normal)
                }else{
                    tool1.setImage(UIImage(named: herramientas[index-unoCero]), for: .normal)
                }
            case 1:
                tool2.isHidden = false
                tool2.setImage(UIImage(named: herramientas[index-unoCero]), for: .normal)
            case 2:
                tool3.isHidden = false
                tool3.setImage(UIImage(named: herramientas[index-unoCero]), for: .normal)
            case 3:
                tool4.isHidden = false
                tool4.setImage(UIImage(named: herramientas[index-unoCero]), for: .normal)
                
            case 4:
                tool5.isHidden = false
                tool5.setImage(UIImage(named: herramientas[index-unoCero]), for: .normal)
            default:
                print("ERROR")
            }
        }
        print(herramientasObjectoSeleccionado.miniatura)
        print(herramientasObjectoSeleccionado.iconosInferiores)
        if let iconoMiniatura = herramientasObjectoSeleccionado.miniatura{
            imagenElementoEscogido.image = UIImage(named: iconoMiniatura)
        }
        arPickerController.isHidden = true
        imagenElementoEscogido.isHidden = false
        openCloseButton.isHidden = false
        badAccuracyImagen.isHidden = true
        
    }
    //Al pulsar en el mapa vuelves al mapa principal de Google
    @IBAction func volver(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: **************** [Persistencia] ****************

 
    private func guardarElemento(){
        
        sceneView.session.getCurrentWorldMap { worldMap, error in
            guard let map = worldMap
                else { print("cant show map"); return }
            
            // Add a snapshot image indicating where the map was captured.
            guard let snapshotAnchor = SnapshotAnchor(capturing: self.sceneView)
                else { fatalError("Can't take snapshot") }
            map.anchors.append(snapshotAnchor)
            
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: map, requiringSecureCoding: true)
                try data.write(to: self.mapSaveURL, options: [.atomic])
            } catch {
                fatalError("Can't save map: \(error.localizedDescription)")
            }
        }
    }
    
    var mapDataFromFile: Data? {
        return try? Data(contentsOf: mapSaveURL)
    }
    
    private func cargarElementoPersistente(){
        /// - Tag: ReadWorldMap
        let worldMap: ARWorldMap = {
            guard let data = mapDataFromFile
                else { fatalError("Map data should already be verified to exist before Load button is enabled.") }
            do {
                guard let worldMap = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data)
                    else { fatalError("No ARWorldMap in archive.") }
                return worldMap
            } catch {
                fatalError("Can't unarchive ARWorldMap from file data: \(error)")
            }
        }()
        
        // Display the snapshot image stored in the world map to aid user in relocalizing.
        if let snapshotData = worldMap.snapshotAnchor?.imageData,
            let snapshot = UIImage(data: snapshotData) {
            self.snapshotThumbnail.image = snapshot
        } else {
            print("No snapshot image in world map")
        }
        // Remove the snapshot anchor from the world map since we do not need it in the scene.
        worldMap.anchors.removeAll(where: { $0 is SnapshotAnchor })
        
        let configuration = self.defaultConfiguration // this app's standard world tracking settings
        configuration.initialWorldMap = worldMap
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        
        
        isRelocalizingMap = true
        virtualObjectAnchor = nil

    }
    
    var defaultConfiguration: ARWorldTrackingConfiguration {
        let configuration = ARWorldTrackingConfiguration()
        if superficie == 1{
            configuration.planeDetection = .vertical
        }else if superficie == 0 {
            configuration.planeDetection = .horizontal
        }else if superficie == 2{
            configuration.planeDetection = [.horizontal, .vertical]
        }
        return configuration
    }
    

    
    /* *************[IBAction-NOTAS]************* */
    //Añade texto a la textura de la nota
    

    
    @IBAction func cambia(_ sender: Any) {
        changeTextureNote()
    }
    

    //------------- [END - IBAction] ------------- //

    // MARK: **************** [CargarElementosObjectoSeleccionado] ****************
    func cargarElementoSeleccionado(nombreElemento: Element.Categoria, numElemento: Int){
        
        switch nombreElemento {
            
        case .nota:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { //
                self.animacionBusquedaSuperficie()
            }
            //Asignamos los nombres de las herramientas que corresponden al nombre de los iconos
            herramientasObjectoSeleccionado = NotasDatos.notas[numElemento].herramientas
            //Configuración de gestos de toque, para poder añadir así las notas
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
            //Añadimos dicho gesto a la escena
            sceneView.addGestureRecognizer(tapGestureRecognizer)
            DispatchQueue.global(qos: .background).async {
                print("NOTA")
                self.Elemento3DSeleccionado =  SCNScene(named: "art.scnassets/\(NotasDatos.notas[numElemento].nombre3DModel)")
            }
            superficie = 1
        case .dibujar:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { //
                self.animacionBusquedaSuperficie()
            }
            herramientasObjectoSeleccionado = PaletaDatos.paletas[numElemento].herramientas
            superficie = 2
        case .objectos3D:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { //
                self.animacionBusquedaSuperficie()
            }
            print("modelo 3d seleccionado")
            //Configuración de gestos de toque
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
            //Añadimos dicho gesto a la escena
            sceneView.addGestureRecognizer(tapGestureRecognizer)
            DispatchQueue.global(qos: .background).async {
                print("3D")
                self.Elemento3DSeleccionado =  SCNScene(named: "art.scnassets/\(ModelosDatos.modelos3D[numElemento].archivo3D).usdz")
            }
            herramientasObjectoSeleccionado = ModelosDatos.modelos3D[numElemento].herramientas
            superficie = 0
        case .cuadro:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { //
                self.arrowAnimationHelp()
            }
            print("cuadro seleccionado")
            //Configuración de gestos de toque
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
            //Añadimos dicho gesto a la escena
            sceneView.addGestureRecognizer(tapGestureRecognizer)
            DispatchQueue.global(qos: .background).async {
                print("cuadro")
                self.Elemento3DSeleccionado =  SCNScene(named: "art.scnassets/\(CuadrosDatos.cuadros[numElemento].nombre3DModel)")
            }
            herramientasObjectoSeleccionado = CuadrosDatos.cuadros[numElemento].herramientas
            print(herramientasObjectoSeleccionado)
            superficie = 1
            
        default:
            print("Error")
        }
    }
    
    //  *********************************************************************
    // MARK: ************************ [TableView] ***************************
    //  *********************************************************************
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return numElementosBarraDerecha
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath)
            as! ArModePickerCell

        cell.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
        cell.celda.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        cell.celda.layer.borderWidth = 0
        cell.celda.clipsToBounds = true
        cell.celda.layer.cornerRadius = 20
        cell.iconoCelda.image = nil
        cell.celda.backgroundColor = nil
        if iconosBarraDerecha.count == numElementosBarraDerecha && iconosBarraDerecha != nil {
            
            cell.iconoCelda.image = UIImage(named: iconosBarraDerecha[indexPath.row])
        }
        
        let indexWeCareAbout = indexPath.row

        guard let color = coloresBarraDerecha?[safe: indexPath.row] else {
            
            return cell
        }
   
        cell.celda.backgroundColor = color
        return cell
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ArModePickerCell
        cell.celda.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        cell.celda.layer.borderWidth = 0
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !flechaActivada {
            
            switch indexPath.row {
            case 0:
                
                 performSegue(withIdentifier: "pickerMode", sender: nil)
                
                
            case 1:
                //Desactivado para no subir o gestionar ningun tipo de dato sensible por parte del usuario ya que no hay terminos, condiciones privacidad etc.
//                var myNewDictArray2 = ["E": 0,"la": DEMO.0077550,"lo": DEMO.5706951]
//                let upload = Upload()
//                upload.element(latitud: "DEMO_008", longitud: "-DEMO_571", elementoMapa: myNewDictArray2)
                //guardarElemento()
                
                let alert = UIAlertController(title: "Alerta", message: "La función de \"subir un elemento\" esta desactivada en la versión DEMO", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                self.present(alert, animated: true, completion: nil)
                
                
            case 2:
                
                 dismiss(animated: true, completion: nil)
            case 3:
                
                 dismiss(animated: true, completion: nil)
                
            default:
                print("ERROR")
                
            }
        }
        
        // TODO: HAY QUE ORGANIZAR LA MANERA EN LA QUE SE GESTIONA ESTO
        if trazos{
            
            if indexPath.row == 0{
                print("TRAZO NORMAL")
                gradienteAnimado = false
            }else if indexPath.row == 1{
                print("TRAZO GRADIENTE")
                gradienteAnimado = true
            }
        }
        if colores {
            let cell = tableView.cellForRow(at: indexPath) as! ArModePickerCell
            if indexPath.row == 0{
                cell.celda.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }else{
                cell.celda.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            cell.celda.layer.borderWidth = 2
            colorEscogido = indexPath.row
        }
        
        texturaSeleccionada = indexPath.row
        changeTextureNote()
        
    }
    
    //  *********************************************************************
    // MARK: ************************** [AR] ********************************
    //  *********************************************************************

    //Añade luz a los objetos tridimensionales
    func createDirectionalLight(){
        
        let spotLight = SCNNode()
        spotLight.light = SCNLight()
        spotLight.scale = SCNVector3(1,1,1)
        spotLight.light?.intensity = 1000
        spotLight.castsShadow = true
        spotLight.position = SCNVector3Zero
        spotLight.light?.type = SCNLight.LightType.directional
        spotLight.light?.color = UIColor.white
    }
    //Se encarga de gestionar el tipo de plano que se va a mostrar
    override func viewWillAppear(_ animated: Bool) {
        let configuracion = ARWorldTrackingConfiguration()
        
        // Align the real world on z(North-South) x(West-East) axis
//        configuracion.worldAlignment = .gravityAndHeading
        if superficie == 1{
            
            configuracion.planeDetection = .vertical
        
        }else if superficie == 0 {
            configuracion.planeDetection = .horizontal
            
        }else if superficie == 2{

            configuracion.planeDetection = [.horizontal, .vertical]
            
        }
        
        sceneView.session.run(configuracion)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sceneView.session.pause()
    }
    
    //Se encarga de añadir el elemento al puslar en ujn plano
    @objc func tapped(gesture: UITapGestureRecognizer) {
        // Get 2D position of touch event on screen
        let touchPosition = gesture.location(in: sceneView)
        
        // Translate those 2D points to 3D points using hitTest (existing plane)
        let hitTestResults = sceneView.hitTest(touchPosition, types: .existingPlaneUsingExtent)
        
        // Get hitTest results and ensure that the hitTest corresponds to a grid that has been placed on a wall
        guard let hitTest = hitTestResults.first, let anchor = hitTest.anchor as? ARPlaneAnchor, let planoIndex = planos.index(where: { $0.anchor == anchor }) else {
            return
        }
        addElement(hitTest, planos[planoIndex])

    }
    
    //Se encarga de añadir el elemento 3D
    func addElement(_ hitResult: ARHitTestResult, _ grid: Plano) {
        
        // Disable placing objects when the session is still relocalizing
        if isRelocalizingMap && virtualObjectAnchor == nil {
            return
        }
        
        if !añadido{
            if let elementoSeleccionado = tipoElementoSeleccionado{
                switch elementoSeleccionado {
                case .nota:
                    createNote(coordinates: hitResult.worldTransform)
                case .objectos3D:
                    create3DModel(coordinates: hitResult.worldTransform)
                case .cuadro:
                    createCuadro(coordinates: hitResult.worldTransform)
                    
                default:
                    print("error")
                }
            }
            añadido = true
            animacionMover()
            //Borramos el plano
            grid.removeFromParentNode()
            
        }else{
            nodeActual?.position = SCNVector3(hitResult.worldTransform.columns.3.x,hitResult.worldTransform.columns.3.y,hitResult.worldTransform.columns.3.z)
        }
        // Borramos los anclajes existentes
        if let existingAnchor = virtualObjectAnchor {
            sceneView.session.remove(anchor: existingAnchor)
        }
        
        
        //Almacenamos los anclajes
        virtualObjectAnchor = ARAnchor(name: virtualObjectAnchorName, transform: hitResult.worldTransform)
        sceneView.session.add(anchor: virtualObjectAnchor!)
        
    }
    
    
    func changeTextureNote(){
        
        print(currentNode?.childNode(withName: "fo", recursively: true))
        nodeActual?.childNode(withName: "fo", recursively: true)?.geometry?.firstMaterial?.diffuse.contents = TextoToTexture.GetImage(drawText: texto?.text as! NSString, inImage: UIImage(named: NotasDatos.notas[0].nombresTexturas[texturaSeleccionada])!)
        
        
    }
    
    func createNote(coordinates: simd_float4x4){
        
        let scene = Elemento3DSeleccionado
        var node = SCNNode()

        /* *************[NOTA]************* */
        texturaNota = TextoToTexture.GetImage(drawText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...", inImage: UIImage(named: NotasDatos.notas[0].nombresTexturas[0])!)
        print(texturaNota)
        //Recorre los nodos que hay dentro de la escena, luego comprueba si el elemento tiene dicho nombre para añadirle una textura concreta.
        for childNode in (scene?.rootNode.childNodes)!{
            if childNode.name == "fo" {
                childNode.geometry?.firstMaterial?.diffuse.contents = texturaNota
                print("Nota")
                node.addChildNode(childNode)
            }else{
                print("Chincheta")
                childNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
                node.addChildNode(childNode)
            }
            node.addChildNode(childNode)
        }
        nodeActual = node
        node.position = SCNVector3(coordinates.columns.3.x,coordinates.columns.3.y,coordinates.columns.3.z)
        node.scale = SCNVector3(0.03, 0.03, 0.03)
        
        sceneView.scene.rootNode.addChildNode(node)
        //Abrimos el teclado al añadir la nota
        if !notaAñadida{
            notaAñadida = true
            DispatchQueue.main.async {
                self.vistaInvisibleCerrarTeclado.isHidden = false
                self.texto.becomeFirstResponder()
            }
        }
        
    }
    
    func createCuadro(coordinates: simd_float4x4){
        
        let scene = Elemento3DSeleccionado
        var node = SCNNode()
  
        //Recorre los nodos que hay dentro de la escena, luego comprueba si el elemento tiene dicho nombre para añadirle una textura concreta.
        for childNode in (scene?.rootNode.childNodes)!{

            print("SDJFNSJKDFNJKSD")
            if childNode.name == "Box001_Untitled"{
            
                childNode.geometry?.firstMaterial?.diffuse.contents = texturaCuadro
            }
            node.addChildNode(childNode)
        }
        
        nodeActual = node
        node.position = SCNVector3(coordinates.columns.3.x,coordinates.columns.3.y,coordinates.columns.3.z)
        node.scale = SCNVector3(0.03, 0.03, 0.03)
        
        sceneView.scene.rootNode.addChildNode(node)
    }
    
    func create3DModel(coordinates: simd_float4x4){
        
        let scene = Elemento3DSeleccionado
        print("ESTO ES EL ELEMENTO: \(Elemento3DSeleccionado)")
        print("ESTO ES EL ELEMENTO: \(coordinates)")
        
        let textGeometry = SCNText(string: "Demo mode", extrusionDepth: 1.0)
        textGeometry.font = UIFont(name: "Arial", size: 2)
        textGeometry.firstMaterial!.diffuse.contents = UIColor.black
        let textNode = SCNNode(geometry: textGeometry)
        
        // Update object's pivot to its center
        // https://stackoverflow.com/questions/44828764/arkit-placing-an-scntext-at-a-particular-point-in-front-of-the-camera
        let (min, max) = textGeometry.boundingBox
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = min.y + 0.5 * (max.y - min.y)
        let dz = min.z + 0.5 * (max.z - min.z)
        textNode.pivot = SCNMatrix4MakeTranslation(dx, dy, dz)
        
        textNode.scale = SCNVector3(0.01, 0.01, 0.01)
        textNode.name = "Texto"
        
        let plane = SCNPlane(width: 0.2, height: 0.15)
        
        plane.cornerRadius = 0.02
        let blueMaterial = SCNMaterial()
        blueMaterial.diffuse.contents = UIColor(red: 205.0/255.0, green: 205.0/255.0, blue: 205.0/255.0, alpha: 0.85)
        plane.firstMaterial = blueMaterial
        let parentNode = SCNNode(geometry: plane) // this node will hold our text node
        parentNode.name = "Plano"
        let yFreeConstraint = SCNBillboardConstraint()
        yFreeConstraint.freeAxes = .Y // optionally
        parentNode.constraints = [yFreeConstraint] // apply the constraint to the parent node
        
        parentNode.position = SCNVector3(coordinates.columns.3.x+0.1,coordinates.columns.3.y+0.4,coordinates.columns.3.z)
        parentNode.addChildNode(textNode)
        
        sceneView.scene.rootNode.addChildNode(parentNode)

        let node = SCNNode()
        
        for childNode in (scene?.rootNode.childNodes)!{
            node.addChildNode(childNode)
        }
        nodeActual = node
       
        node.position = SCNVector3(coordinates.columns.3.x,coordinates.columns.3.y,coordinates.columns.3.z)
        //Es la altura que van a tener todos los modelos
        let ySize: Float = 0.3
        //Es el encargado de aumentar la escala de los modelos base a la altura que tiene asignada cada uno
        let multiplicador = Float(ModelosDatos.modelos3D[numElementoSeleccionado].altura) ?? 1
        //Calculamos cuanto tendriamos que aumentar o reducir la escala del modelo 3D para que este tuviera la altura que se ha establecio
        print(node.boundingBox.max.y)
        let scale = ySize/node.boundingBox.max.y * multiplicador
        //Dependiendo del resultado anterior escalaremos todos sus ejes respetando la proporción
        node.scale = SCNVector3Make(scale, scale, scale)
        
        sceneView.scene.rootNode.addChildNode(node)
    }
    
    
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    //Se llama a esta funcion cuando se mueve el dedo por encima de la pantalla -DIBUJAR
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        //Comprueba que el dedo esta tocando la vista de ARKIT
//        guard let point = touches.first?.location(in: sceneView) else {
//            return
//        }
//        //Convierte los toques en la pantalla a puntos 3D en el mundo real, tomando en cuenta el plano existente que fue detectado.
//        let hitTestResults = sceneView.hitTest(point, types: .existingPlaneUsingExtent)
//        //Comprueba que el resultado anterior corresponde a un punto del plano detectado
//        guard let hitTest = hitTestResults.first, let anchor = hitTest.anchor as? ARPlaneAnchor, let gridIndex = grids.index(where: { $0.anchor == anchor }) else {
//            return
//        }
//
//        let pointDirecction = SCNVector3(hitTest.worldTransform.columns.3.x,hitTest.worldTransform.columns.3.y,hitTest.worldTransform.columns.3.z)
//
//
//        if let previousPoint = previousPoint {
//            let twoPointsNode = SCNNode()
//            _ = twoPointsNode.buildLineInTwoPointsWithRotation(
//                from: previousPoint,
//                to: pointDirecction,
//                radius: lineWidth/10000,
//                color: colorArray[colorEscogido])
////            twoPointsNode.highlightNodeWithDurarion(5)
//            sceneView.scene.rootNode.addChildNode(twoPointsNode)
//        }
//
//
//        let hitTestResult = sceneView.hitTest(self.view.center, options: nil)
//
//        previousPoint = pointDirecction
//
//
////        //Forma antigua de dibujar en donde se usaban esferas y tenia algunas desventajas
////        //Generamos una esfera que simulará el trazo de la pintura
////        let sphereNode = SCNNode(geometry: SCNSphere(radius: 0.02))
////
////        //Añadimos a la esfera la posición y la orientación obtenida anteriormente
////        sphereNode.position = SCNVector3(hitTest.worldTransform.columns.3.x,hitTest.worldTransform.columns.3.y,hitTest.worldTransform.columns.3.z)
////
////        //Añadimos la esfera a la escena
////        self.sceneView.scene.rootNode.addChildNode(sphereNode)
////        //Le añadimos su color correspondiente
////        sphereNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "golden")
////        sphereNode.highlightNodeWithDurarion(5)
////        sceneView.scene.rootNode.addChildNode(sphereNode)
//
//    }
    
    
    func showTrackingQualityInfo(for trackingState: ARCamera.TrackingState, autoHide: Bool) {
        labelInformation.text = trackingState.presentationString
    }
    
    func escalateFeedback(for trackingState: ARCamera.TrackingState, inSeconds seconds: TimeInterval){
    
            var message = trackingState.presentationString
            if let recommendation = trackingState.recommendation {
                message.append(": \(recommendation)")
            }
            labelInformation.text = message
    
    }


    
    // MARK: **************** [Gestos] ****************
    var curTickleStart:CGPoint = CGPoint.zero
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            self.curTickleStart = touch.location(in: self.view)
            print(touch.location(in: self.view))
        }
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
        self.herramientas.alpha = 0.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.imagenElementoEscogido.alpha = 0.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.openCloseButton.alpha = 0.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.arPickerElements.alpha = 0.0
        }, completion: nil)
        
        self.currentNode = nil
    }
    //Se llama a esta funcion cuando se mueve el dedo por encima de la pantalla -DIBUJAR
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //Se encarga de girar los modelos
        if let touch = touches.first {
            let ticklePoint = touch.location(in: self.view)
            let moveAmt = ticklePoint.x - curTickleStart.x
            //
            dibujado = true
            if let orientation = nodeActual?.orientation{
                
                movido = true
                
                var glQuaternion = GLKQuaternionMake(orientation.x, orientation.y, orientation.z, orientation.w)
                let multiplier = GLKQuaternionMakeWithAngleAndAxis(Float(moveAmt)/5000, 0, 1, 0)
                glQuaternion = GLKQuaternionMultiply(glQuaternion, multiplier)
                nodeActual?.orientation = SCNQuaternion(x: glQuaternion.x, y: glQuaternion.y, z: glQuaternion.z, w: glQuaternion.w)
            }

        }
        if tipoElementoSeleccionado == .dibujar{
            //Comprueba que el dedo esta tocando la vista de ARKIT
            guard let point = touches.first?.location(in: sceneView) else {
                return
            }
            
            lineWidth = Double((touches.first?.force ?? 0)/100)
            generarTrazo(point: point, lineWidth: lineWidth)
        }
        
 
    }
    
    func generarTrazo( point: CGPoint, lineWidth: Double){
        var pointDirecction = SCNVector3(0, 0, 0)
        
        if modo3D {
            //Convierte los toques en la pantalla a puntos 3D en el mundo real, tomando en cuenta el plano existente que fue detectado.
            let hitTestResults = sceneView.hitTest(point, types: .existingPlaneUsingExtent)
            //Comprueba que el resultado anterior corresponde a un punto del plano detectado
            guard let hitTest = hitTestResults.first, let anchor = hitTest.anchor as? ARPlaneAnchor, let planoIndex = planos.index(where: { $0.anchor == anchor }) else {
                return
            }
            //Dibuja en una superficie
            pointDirecction = SCNVector3(hitTest.worldTransform.columns.3.x,hitTest.worldTransform.columns.3.y,hitTest.worldTransform.columns.3.z)
        }else{
            //Dibuja en el aire
            pointDirecction = sceneView.unprojectPoint(SCNVector3(point.x, point.y, 0.997))
        }
       
        if !espera{
            //Mejoramos el rendimiento de la aplicacion al dibujar, reduciendo hasta torno a un 50% la cantidad de nodos que se crean
            self.espera = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.04) { 
                self.espera = false
            }
            if self.currentNode == nil{
                
                self.currentNode = MQNode()
                
                self.addNode(self.currentNode!)
            }

            self.currentNode?.addVertices(vertice: pointDirecction, color: (colorArray?[colorEscogido])!,lineWidth: lineWidth, gradienteAnimado: gradienteAnimado )
            let hitTestResult = sceneView.hitTest(self.view.center, options: nil)
            
        }
    }

    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if imagenAbierta {
            snapshotThumbnail.isHidden = true
        }
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.herramientas.alpha = 1.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.imagenElementoEscogido.alpha = 1.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.openCloseButton.alpha = 1.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.arPickerElements.alpha = 1.0
        }, completion: nil)
        self.currentNode = nil
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.herramientas.alpha = 1.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.imagenElementoEscogido.alpha = 1.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.openCloseButton.alpha = 1.0
        }, completion: nil)
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
            self.arPickerElements.alpha = 1.0
        }, completion: nil)
        self.currentNode = nil
    }
    //MARK: - MQNode
    func addNode(_ node: MQNode) {
        self.rootNode.addChildNode(node)
    }
    
    //  *********************************************************************
    // MARK: ************************ [MAPA] ********************************
    //  *********************************************************************
    
    private func configureLocationServices() {
        locationManager.delegate = self
//        let status = CLLocationManager.authorizationStatus()
//
//        if status == .notDetermined {
//            locationManager.requestAlwaysAuthorization()
//        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
//            beginLocationUpdates(locationManager: locationManager)
//        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }

    //Se encarga de añadir los iconos en el mapa -MAP
    private func addIconElementMap() {
        /*
        let prueba1 = MKPointAnnotation()
        prueba1.title = "prueba1"
        prueba1.coordinate = CLLocationCoordinate2D(latitude: DEMO.00775508470332 , longitude: DEMO.571092076599598)
        
        let prueba2 = MKPointAnnotation()
        prueba2.title = "prueba2"
        prueba2.coordinate = CLLocationCoordinate2D(latitude: DEMO.008174192835185 , longitude: DEMO.5709351673722267)
        
        mapView.addAnnotation(prueba1)
        mapView.addAnnotation(prueba2)
         */

        
    }
    
    /*---------------- [END] ----------------*/
    
    
    // MARK: **************** [ANIMACIONES] ****************
    //ANIMACION FADE IN de las celdas
    func animarAlpha(){
        
        //Definimos el estado inicial
        arPickerElements.alpha = 0
        //Rotacion tridimensional
        let rotacionAplicada = CATransform3DMakeTranslation(-100, 0, 0)
        arPickerElements.layer.transform = rotacionAplicada

        //Creamos la animación
        UIView.animate(withDuration: 0.5){
            
            //Lo vuelve a colocar donde deberia estar
            self.arPickerElements.layer.transform = CATransform3DIdentity
            //Definimos el estado final
            self.arPickerElements.alpha = 1.0
            
            
        }
        self.arPickerElements.transform = CGAffineTransform(rotationAngle: (-.pi))
    }
    /*---------------- [END] ----------------*/
    
    
    //https://stackoverflow.com/questions/35946499/how-to-truncate-decimals-to-x-places-in-swift
    //Muestra el numero de decimales que desees.
    func truncateDigitsAfterDecimal(number: CGFloat, afterDecimalDigits: Int) -> Double {
        if afterDecimalDigits < 1 || afterDecimalDigits > 512 {return 0.0}
        return Double(String(format: "%.\(afterDecimalDigits)f", number))!
    }
 
    func hacerFoto(){
        
        //Hacer la foto
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true)
            {
                //print("hacerfoto")
            }
        }else {
            let image = UIImagePickerController()
            image.delegate = self
            
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = true
            
            self.present(image, animated: true)
            {
                //print("hacerfoto")
            }
        }
    }
    
    //Obtener la foto tomada
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            texturaCuadro = UIImage.imageByMergingImages(topImage: image, bottomImage: UIImage(named: "paint")!)
            fotoTomada = true
            if !planoDetectado {
                
                animacionBusquedaSuperficie()
                
            }else{
                
                animacionGestos()
            }
        }
        else{
            print("Unable To fetch image")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


// MARK: **************** [DELEGADO MAPA] ****************
extension ARMode: CLLocationManagerDelegate {
    //Se encarga de actualizar la localizacion del usuario -MAP
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // print("Did get latest location")
        if let horizontal = locations.first?.horizontalAccuracy{
            if let vertical = locations.first?.verticalAccuracy{
                //Obtiene la precision horizontal y vertical del GPS
                gpsPrecision.text = "\(horizontal)"
                //Comprobamos si la localizacion es buena o mala
                if horizontal <= 10.0{
                    badAccuracyImagen.isHidden = true
                 
                }else{
                    badAccuracyImagen.isHidden = false
                    let pulse1 = CASpringAnimation(keyPath: "transform.scale")
                    pulse1.duration = 0.6
                    pulse1.fromValue = 1.0
                    pulse1.toValue = 1.12
                    pulse1.autoreverses = true
                    pulse1.repeatCount = 1
                    pulse1.initialVelocity = 0.5
                    pulse1.damping = 0.8
                    
                    let animationGroup = CAAnimationGroup()
                    animationGroup.duration = 2.7
                    animationGroup.repeatCount = 1000
                    animationGroup.animations = [pulse1]
                    
                    badAccuracyImagen.layer.add(animationGroup, forKey: "pulse")
                    
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("The status changed")
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
}

// Delegado -MAP
extension ARMode: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if let title = annotation.title, title == "prueba1" {
            annotationView?.image = UIImage(named: "nota")
        } else if let title = annotation.title, title == "prueba2" {
            annotationView?.image = UIImage(named: "elementovr")
        }else if annotation === mapView.userLocation {
            annotationView?.image = UIImage(named: "blueDot")
        }
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("The annotation was selected: \(String(describing: view.annotation?.title))")
    }
}
// MARK: **************** [DELEGADO AR] ****************
extension ARMode: ARSCNViewDelegate {
    
//    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor){
//
//        print("superficie")
//
//        if let planeAnchor = anchor as? ARPlaneAnchor{
//            //Si es un plano lo pintamos
//            //Primero determinamos su tamaño
//            let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
//
//            //Luego lo coloreamos con texturas
//            plane.materials.first?.diffuse.contents = UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 0.5)
//
//            //Creamos un plane node para colocar en la escena
//            let planeNode = SCNNode(geometry: plane)
//            planeNode.position = SCNVector3(planeAnchor.center.x, 0.0, planeAnchor.center.z)
//            //En SceneKit
//            planeNode.eulerAngles.x  =  -Float.pi / 2.0
//            //Añadios el node
//            node.addChildNode(planeNode)
//        }
//    }
    
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
       
        
        //Comprueba si el anclaje es el de un plano
        if let planeAnchor = anchor as? ARPlaneAnchor {
            
            if tipoElementoSeleccionado == .dibujar  && planoDetectado == false {
                DispatchQueue.main.async {
                    self.animacionGestosDibujar()
                }
            }else if planoDetectado == false && tipoElementoSeleccionado != .cuadro {
                
                animacionGestos()
            }else if planoDetectado == false && tipoElementoSeleccionado == .cuadro && fotoTomada{
                animacionGestos()
        
            }
            self.planoDetectado = true
           
            
            let plano = Plano(anchor: planeAnchor)
            self.planos.append(plano)
            node.addChildNode(plano)

        }
        if añadido != true{
            //Comprueba si el anclaje obtenido corresponde al del elemento guardado
            guard anchor.name == virtualObjectAnchorName
                else { return }
            
            // Guarda la referencia al ancla del objeto virtual cuando se agrega el ancla para evitar que se relocalice
            if virtualObjectAnchor == nil {
                virtualObjectAnchor = anchor
            }
            
            if let elementoSeleccionado = tipoElementoSeleccionado{
                switch elementoSeleccionado {
                case .nota:
                    createNote(coordinates: virtualObjectAnchor!.transform)
                case .objectos3D:
                    create3DModel(coordinates: virtualObjectAnchor!.transform)
                default:
                    print("error")
                }
            }
            
        }
    }
    

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        let plano = self.planos.filter { plano in
            return plano.anchor.identifier == planeAnchor.identifier
            }.first
        
        guard let foundPlano = plano else {
            return
        }
        for result in sceneView.hitTest(CGPoint(x: 0.5, y: 0.5), types: [.existingPlaneUsingExtent, .featurePoint]) {
            DispatchQueue.main.async {
                self.planeDistance.text = "\(self.truncateDigitsAfterDecimal(number: result.distance, afterDecimalDigits: 3))m"
            }
            DispatchQueue.main.async {
                if result.distance < 0.45 {
                    
                    self.ok.isHidden = false
                }else{
                    self.ok.isHidden = true
                    
                }
            }
        }
        //2. Get The Width & Height Of The Plane
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        DispatchQueue.main.async {
            self.planeWDebug.text = "\(self.truncateDigitsAfterDecimal(number: width, afterDecimalDigits: 3))m"
            self.planeHDebug.text = "\(self.truncateDigitsAfterDecimal(number: height, afterDecimalDigits: 3))m"
        }
        foundPlano.update(anchor: planeAnchor)
    }
    
    
    
    /// - Tag: CheckMappingStatus
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        // Enable Save button only when the mapping status is good and an object has been placed
        switch frame.worldMappingStatus {
        case .extending, .mapped:
            print("EXTENDING")
//            saveExperienceButton.isEnabled =
//                virtualObjectAnchor != nil && frame.anchors.contains(virtualObjectAnchor!)
        default:
            print("default")
//            saveExperienceButton.isHidden = true
        }
    }

    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        showTrackingQualityInfo(for: camera.trackingState, autoHide: true)
        
        switch camera.trackingState {
        case .notAvailable, .limited:
            escalateFeedback(for: camera.trackingState, inSeconds: 3.0)
        case .normal:
            infoAr.isHidden = true
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        guard let lightEstimate = self.sceneView.session.currentFrame?.lightEstimate else { return }
        let ambientLightEstimate = lightEstimate.ambientIntensity
        
        let ambientColourTemperature = lightEstimate.ambientColorTemperature
        
        
        //        print(
        //            """ Luz = \(ambientLightEstimate)
        //            Color de la luz = \(ambientColourTemperature)
        //            """)
        
        
//        print(sceneView.session.currentFrame?.rawFeaturePoints)
        DispatchQueue.main.async {
            self.lightDebug.text = "\(self.truncateDigitsAfterDecimal(number: ambientLightEstimate, afterDecimalDigits: 3))"
        }
        
        if ambientLightEstimate < 150 {
            DispatchQueue.main.async {
                self.nolightimage.isHidden = false
                let pulse1 = CASpringAnimation(keyPath: "transform.scale") 
                pulse1.duration = 0.6
                pulse1.fromValue = 1.0
                pulse1.toValue = 1.12
                pulse1.autoreverses = true
                pulse1.repeatCount = 1
                pulse1.initialVelocity = 0.5
                pulse1.damping = 0.8
                
                let animationGroup = CAAnimationGroup()
                animationGroup.duration = 2.7
                animationGroup.repeatCount = 1000
                animationGroup.animations = [pulse1]
                
                self.nolightimage.layer.add(animationGroup, forKey: "pulse")
                print("Lighting Is Too Dark")
                
            }        
        }else{
            DispatchQueue.main.async {
                
                self.nolightimage.isHidden = true
            }
        }
    }
}
 
extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

extension UIImage {
    
    static func imageByMergingImages(topImage: UIImage, bottomImage: UIImage, scaleForTop: CGFloat = 1.392) -> UIImage {
        let size = bottomImage.size
        let container = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 2.0)
        UIGraphicsGetCurrentContext()!.interpolationQuality = .medium
        bottomImage.draw(in: container)
        
        let topWidth = size.width / scaleForTop
        let topHeight = size.height / scaleForTop
        let topX = (size.width / 2.0) - (topWidth / 2.0)
        let topY = (size.height / 2.0) - (topHeight / 2.0)
        
        topImage.draw(in: CGRect(x: 0, y: 0, width: topWidth, height: topHeight), blendMode: .normal, alpha: 1.0)
        
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
}
