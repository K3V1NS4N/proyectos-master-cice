//
//  inicio.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 10/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

//Pantalla de inicio de la aplicación encargada de gestionar la sesion

protocol InicioMessage {
    func didReceiveData(_ data: String)
    
}
class Inicio: UIViewController, InicioMessage {
    
    //Funcion a la que se llamada cuando el modelo JsonParser recibe la informacion y la formatea
    func didReceiveData(_ data: String) {
       
        let alert = UIAlertController(title: "Alerta", message: data, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Acepto", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var icono: UIImageView!
    
    @IBOutlet weak var boton: buttonDesignables!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var info: UIButton!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var registro: UIButton!
    let getMessage = ServerMessage()
    
    override func viewDidLoad() {
        
        Auth.auth().addStateDidChangeListener() { auth, user in
            //Comprueba si ya hay un usuario logeado
            if user != nil {
                //Procede a animar el logo de la aplicacion
                UIView.animate(withDuration: 0.4, animations: {() -> Void in
                    self.icono?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3, animations: {() -> Void in
                        self.icono?.transform = CGAffineTransform(scaleX: 6, y: 6)
                    })
                })
                //Se ejecuta 0.3 segundos y se encarga de ir a la pantalla principal del mapa
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.performSegue(withIdentifier: "logeado", sender: nil)
                })
            
            }else{
                //En caso de no estar logeados simplemente mostramos la pantalla tal cual
                UIView.animate(withDuration: 0.25, animations: {
                    self.icono.alpha = 0.0
                })
                //Mostramos sus elementos
                self.boton.isHidden = false
                self.titulo.isHidden = false
                self.info.isHidden = false
                self.background.isHidden = false
                self.registro.isHidden = false

            }
        }
      getMessage.getMessage()
      getMessage.delegate = self
    }
    
    //Boton de info acerca de la app
    @IBAction func info(_ sender: Any) {
        
        let alert = UIAlertController(title: "Developed and Designed by Kevin Sabajanes García-Fraile", message: "\n© 2019 Todos los Derechos Reservados \n\n Puede escribirme a este correo en caso de querer comunicarme algo: Kevinsan95@gmail.com", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
}
