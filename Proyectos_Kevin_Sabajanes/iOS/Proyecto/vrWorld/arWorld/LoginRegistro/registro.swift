//
//  registro.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 20/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class registro: UIViewController {
    
    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var correoElectronico: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var boxTerminos: CheckBox!
    @IBOutlet weak var terminosButton: UIButton!
    @IBOutlet weak var registroButton: buttonDesignables!{
        //Deshabilitamos el bootn de registro
        didSet{
            registroButton.isEnabled = false
        }
        
    }

    var isChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround2()
        //Se encarga de añadir los observer para gestionar cuando el teclado aparece o desaparece
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        let alert = UIAlertController(title: "Alerta", message: "Ningún dato que se introduzca será almacenado en nuestros servidores, en su lugar se insertaran datos aleatorios para proceder con el registro.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Acepto", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefvwxyzABCDEFGWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    @IBAction func registrarse(_ sender: Any) {

        //Comprueba tanto si se ha introducido un email como una contraseña en caso contrario no se procedera.
        guard let  email = correoElectronico.text, email.isNotEmpty,
        let password = password.text, password.isNotEmpty else { return }
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        //Se procede al registro mandando la informacion a firebase
        Auth.auth().createUser(withEmail: "\(randomString(length: 6))@gmail.com", password: randomString(length: 15)) { (authResult, error) in
            // ...
            if let error = error {
                //Comprobamos el error mandandoselo al gestor de errores en caso de que lo haya
                self.handleFireAuthError(error)
                debugPrint(error.localizedDescription)
                return
                    
            }
            //Comprueba que se haya introducido un usuario
            guard let user = authResult?.user else { return }
                
            //Almacena dicho nombre en la base de datos para poder usarlo mas adelante
            ref.child("users").child(user.uid).setValue(["username": self.randomString(length: 5)]){
                    
                (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    print("Data could not be saved: \(error).")
                } else {
                    //Establecemos una cantidad inicial de monedas para el usuario
                    ref.child("/coins").setValue(0){
                        (error:Error?, ref:DatabaseReference) in
                        if let error = error {
                            print("Data could not be saved: \(error).")
                        } else {
                            //Navegamos a la pantalla de comienzo
                            self.performSegue(withIdentifier: "start", sender: nil)
                        }
                    }
                }
            }
        }
    }

    //Estos IBAction se encargar de llamar el metodo que comprueba si se han insertado datos en todos los campos
    @IBAction func comprueba1(_ sender: Any) {
        habilitarBotonRegistro()
    }
    
    @IBAction func comprueba2(_ sender: Any) {
        habilitarBotonRegistro()
    }
    
    @IBAction func comprueba3(_ sender: Any) {
        habilitarBotonRegistro()
    }
    @IBAction func comprueba4(_ sender: Any) {
        
        if isChecked == true {
            isChecked = false
        } else {
            isChecked = true
        }
        habilitarBotonRegistro()
    }
    
    //Comprueba si se han insertado datos en todos los campos
    func habilitarBotonRegistro(){
        registroButton.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
        print(boxTerminos.isChecked)
        if usuario.text!.isNotEmpty && correoElectronico.text!.isNotEmpty && password.text!.isNotEmpty && isChecked {
            
            registroButton.backgroundColor = UIColor(red: 67/255, green: 83/255, blue: 95/255, alpha: 1)
            registroButton.isEnabled = true
        }
    }

    //https://stackoverflow.com/questions/26070242/move-view-with-keyboard-using-swift/56193255
    @IBAction func salir(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //Metodos encargados de gestionar cuando el teclado aparece o desaparece para poder subir los elementos visuales
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height/2.5
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround2() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard2))
        view.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard2() {
        
        view.endEditing(true)
    }
}
