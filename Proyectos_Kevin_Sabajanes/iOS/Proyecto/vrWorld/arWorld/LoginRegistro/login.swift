//
//  login.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 20/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
import Firebase

//Se encarga de logear al usuario
class login: UIViewController{

    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var contraseña: UITextField!
    @IBOutlet weak var vista: UIView!
    var keyboardPresent = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround2()
        //Se encarga de añadir los observer para gestionar cuando el teclado aparece o desaparece
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //Boton encargado de logearte
    @IBAction func login(_ sender: Any) {
        //Comprueba tanto si se ha introducido un email como una contraseña en caso contrario no se procedera.
        guard let  email = usuario.text, email.isNotEmpty,
            let password = contraseña.text, password.isNotEmpty else { return }
        
        //Se procede al logeo mandando la informacion a firebase (Se trata de un login de prueba)
        Auth.auth().signIn(withEmail: "prueba@gmail.com", password: "prueba123") { (user, error) in
            if let error = error {
                //Comprobamos el error mandandoselo al gestor de errores en caso de que lo haya
                self.handleFireAuthError(error)
                debugPrint(error.localizedDescription)
                return
            }
            //Navegamos a la pantalla de comienzo
            self.performSegue(withIdentifier: "start", sender: nil)
        }
    }
    
    
    @IBAction func salir(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    //https://stackoverflow.com/questions/26070242/move-view-with-keyboard-using-swift/56193255
    
    //Metodos encargados de gestionar cuando el teclado aparece o desaparece para poder subir los elementos visuales
    @objc func keyboardWillShow(notification: NSNotification) {
        print("se muestra")
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if !keyboardPresent{
                
                keyboardPresent = true
                UIView.animate(withDuration: 0.1) {
                    
                    self.vista.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height/2.7)
                }
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        print("se esconde")
        if keyboardPresent{
            
            keyboardPresent = false
            UIView.animate(withDuration: 0.1) {
                
                self.vista.transform = CGAffineTransform(translationX: 0, y: 0)
            }
        }
    }
}

