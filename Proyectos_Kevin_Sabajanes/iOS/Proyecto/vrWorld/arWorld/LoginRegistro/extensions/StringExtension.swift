//
//  StringExtension.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 24/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation

//Extension que comprueba si un String esta vacio
extension String {
    
    var isNotEmpty: Bool {
        return !isEmpty
    }
}
