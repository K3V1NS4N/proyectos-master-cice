//
//  gestorErrores.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 26/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import Firebase

//Es el gestor de errores de la pantalla de login
extension UIViewController{
    
    func handleFireAuthError(_ error: Error){
        if let errorCode = AuthErrorCode (rawValue: error._code){
            let alert = UIAlertController(title: "Error", message: errorCode.mensajeError, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",style: .default, handler: nil)
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension AuthErrorCode {
    
    var mensajeError: String {
        switch self {
        case .emailAlreadyInUse:
            return "Este correo ya está siendo usado por otro usuario"
        case .userDisabled:
            return "Este usuario ha sido deshabilitado"
        case .operationNotAllowed:
            return "Operación no permitida"
        case .invalidEmail:
            return "Correo electrónico no valido"
        case .wrongPassword:
            return "Contraseña incorrecta"
        case .userNotFound:
            return "No se encontró cuenta del usuario con el correo especificado"
        case .networkError:
            return "Problema al intentar conectar al servidor"
        case .weakPassword:
            return "Contraseña muy debil o no válida"
        case .missingEmail:
            return "Hace falta registrar un correo electrónico"
        case .internalError:
            return "Error interno"
        case .invalidCustomToken:
            return "Token personalizado invalido"
        case .tooManyRequests:
            return "Ya se han enviado muchas solicitudes al servidor"
        default:
            return "Sorry, something went wrong"
        }
    }
}
