//
//  ArModePickerCell.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 03/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

class ArModePickerCell: UITableViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var celda: UIView!
    @IBOutlet weak var iconoCelda: UIImageView!
    
}
