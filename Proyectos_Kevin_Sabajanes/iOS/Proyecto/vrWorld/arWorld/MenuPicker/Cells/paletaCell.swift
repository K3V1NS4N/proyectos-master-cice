//
//  paletaCell.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 18/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

//establece los elementos para la celda de modo dibujo, pinceles, colores etc
class paletaCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var nombrePaleta: UILabel!
    @IBOutlet weak var pincelUno: UIImageView!
    @IBOutlet weak var pincelDos: UIImageView!
    @IBOutlet weak var descripcionPincelUno: UILabel!
    @IBOutlet weak var descripcionPincelDos: UILabel!
    @IBOutlet weak var coloresPaleta: UIImageView!
    
}
