//
//  estandarCell.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 18/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

//Establece los elementos para la celda de los personajes u elementos AR
class estandarCell: UICollectionViewCell {
    
    @IBOutlet weak var imagenEstandar: UIImageView!
    @IBOutlet weak var nombreEstandar: UILabel!
    @IBOutlet weak var quickLook: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
