//
//  PreviewController.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 24/05/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
import SceneKit

//Es la clase que gestiona la ventanita de previsualizacion de las caracteristicas de cada modelo
class PreviewControllerCustom: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
 
    var nombreTexto: String?
    var numElement: Int?
    let datosModelos = ModelosDatos.modelos3D
    //Array con las funcionalidades de ese objeto que se quiere visualizar
    var funcionalidades: [Funcionalidad]?
    let colorArray = [#colorLiteral(red: 1, green: 0.6189445853, blue: 0.6174275279, alpha: 1),#colorLiteral(red: 0.6777824759, green: 0.8537853956, blue: 0.9981455207, alpha: 1),#colorLiteral(red: 1, green: 0.7910110354, blue: 0.5908911228, alpha: 1),#colorLiteral(red: 0.6903143525, green: 0.9854139686, blue: 0.8851040006, alpha: 1),#colorLiteral(red: 1, green: 0.9257304072, blue: 0.6906376481, alpha: 1),#colorLiteral(red: 0.810831964, green: 0.7652289271, blue: 0.9964482188, alpha: 1),#colorLiteral(red: 1, green: 0.7530116439, blue: 0.9956466556, alpha: 1),#colorLiteral(red: 0.8522671461, green: 0.9740851521, blue: 0.7005500197, alpha: 1)]
    @IBOutlet weak var sceneView: SCNView!
    @IBOutlet weak var collectionViewFuncionalities: UICollectionView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var altura: UILabel!
    @IBOutlet weak var peso: UILabel!
    @IBOutlet weak var personalidad: UILabel!
    @IBOutlet weak var rareza: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Se establece la informacion base a la informacion que se obtiene del ARpicker
        nombre.text = nombreTexto
        if let numElement = numElement {
            altura.text = "\(datosModelos[numElement].altura)m"
            peso.text = datosModelos[numElement].peso
            personalidad.text = datosModelos[numElement].personalidad
            rareza.text = datosModelos[numElement].rareza

        }
        
        //Nombre de la escena donde se mostrará la visualizaciond de los objetos 3D
        let scene = SCNScene(named:"art.scnassets/vision.scn")!
        sceneView.scene = scene
        
        let camera = SCNCamera()
        camera.usesOrthographicProjection = true
        scene.rootNode.camera = camera
 
        if let nombre = nombreTexto{
            //Obtenemos el modelo tridimensional ya formateado con su escala para mostrarlo
            let model = ArObjectShow.getModel(name: "art.scnassets/\(nombre).usdz")
            ArObjectShow.startRotation(forNode: model)
            scene.rootNode.addChildNode(model)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let funcionalidades = funcionalidades else {
            return 0
        }
        return funcionalidades.count
    }
    
    //Muestra las funcionalidades del elemento a visualizar
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "functionalityCell", for: indexPath) as! functionalityCell
        if let funcionalidades = funcionalidades{
            //Imagen de la funcionalidad
            cell.funcionalityIcon.image = UIImage(named: funcionalidades[indexPath.row].nombre)
        }
        //Establece los colores de cada funcionalidad
        //TODO: solo se deberia establecer un solo color, designable te obliga a tener que establecer 2 (Mejorable)
        cell.cellView.topColor = funcionalidades?[indexPath.row].color ?? UIColor.black
        cell.cellView.bottomColor = funcionalidades?[indexPath.row].color ?? UIColor.black
        return cell
    }
}
