//
//  functionalityCell.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 03/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

//Establece que elementos va a contener cada celda del collection view de funcionalidades del 3D touch visualizador
class functionalityCell: UICollectionViewCell {
 
    @IBOutlet weak var funcionalityIcon: UIImageView!
    @IBOutlet weak var cellView: viewDesignable!

}
