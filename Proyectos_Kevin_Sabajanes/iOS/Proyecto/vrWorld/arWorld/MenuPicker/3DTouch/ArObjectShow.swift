//
//  arObjectShow.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 31/05/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import SceneKit

//Se encarga de mostrar una previsualización del objeto 3D girando
class ArObjectShow {
    
    class func getModel(name: String) -> SCNNode{
        
        let obj = SCNScene(named: name)
        let node = SCNNode()
        
        //Recorre los nodos que hay dentro de la escena
        for childNode in (obj?.rootNode.childNodes)!{
            node.addChildNode(childNode)
        }
        //Es la altura que van a tener todos los modelos
        // TODO: MEJORABLE
        let zSize: Float = 0.5
        let zSize3: Float = 0.5
        let zSize2: Float = 0.5
        //Calculamos cuanto tendriamos que aumentar o reducir la escala del modelo 3D para que este tuviera la altura que se ha establecio
        let scale = zSize/node.boundingBox.max.y + zSize2/node.boundingBox.max.z + zSize3/node.boundingBox.max.x
        //Dependiendo del resultado anterior escalaremos todos sus ejes respetando la proporción
        node.scale = SCNVector3Make(scale, scale, scale)
        node.position = SCNVector3Make(-1, -1.7, -1)
        
        return node
    }
    
    //Se encarga de girar el modelo de forma animada
    class func startRotation(forNode node: SCNNode){
        let rotate = SCNAction.repeatForever(.rotateBy(x: 0, y: CGFloat(0.05 * Double.pi), z: 0, duration: 0.1))
        node.runAction(rotate)
    }
}
