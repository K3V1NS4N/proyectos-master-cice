//
//  menuModelosVR.swift
//  arWorld
//
//  Created by Kevin Sabajanes García-Fraile on 20/05/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit
import QuickLook


class MenuPickerAR: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, QLPreviewControllerDelegate, QLPreviewControllerDataSource, UIViewControllerPreviewingDelegate {

 
    @IBOutlet weak var comingSoon: UILabel!
    
    // MARK: **************** [3D MODELS] ****************
    private var modelos3D = ModelosDatos.modelos3D
    // MARK: **************** [NOTAS] ****************
    private var notas = NotasDatos.notas
    // MARK: **************** [DIBUJAR] ****************
    private var paletasColores = PaletaDatos.paletas
    // MARK: **************** [DIBUJAR] ****************
    private var cuadros = CuadrosDatos.cuadros
    // MARK: **************** [INTERFAZ] ****************
    var tipoElementoSeleccionado: Categoria = .objectos3D
    var numElementoSeleccionado: Int?
    @IBOutlet weak var coins: UILabel!
    //Categorias a elegir
    enum Categoria {
        case nota
        case dibujar
        case objectos3D
        case caso4
        case caso5
        case caso6
    }
    var indexPathSelected: IndexPath?
    var categoriaSeleccionada = Element.Categoria.objectos3D

    //Numero de elementos a mostrar
    var numElementos = ModelosDatos.modelos3D.count

    private var dataImages: [UIImage?] = []
    private var quickLookIndex = 0
    //Son los nombres de los iconos de las categorias a elegir
    private var IconoGris = ["notaGrisMenu","artGrisMenu","arElementGris","paintingGris","siteGris","tiendaGris"]
    private var IconoColor = ["notaColorMenu","artButton","arElement2","picture","site","tienda"]

    @IBOutlet weak var  collectionView: UICollectionView!
    @IBOutlet weak var collectionViewButton: UICollectionView!
    //Identificador de la celda
    fileprivate let cellId = "estandarCell"
    fileprivate let cellId2 = "paletaCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //3D Touch preview
        registerForPreviewing(with: self, sourceView: collectionView)

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        collectionView.allowsMultipleSelection = false

        //Registramos la celda
        let nib = UINib(nibName: cellId, bundle: nil)
        collectionView.register( nib, forCellWithReuseIdentifier: cellId)
        
        let nib2 = UINib(nibName: cellId2, bundle: nil)
        collectionView.register( nib2, forCellWithReuseIdentifier: cellId2)
        coins.text = String(Info.coins)
    }
    
    // MARK: **************** [Botones] ****************/
    
    //Se encarga de vovler al controller ARMODE
    @IBAction func exit(_ sender: Any) {
        categoriaSeleccionada = Element.Categoria.caso6
        self.dismiss(animated: true, completion: nil)
    }
    
    /* Se le llama cuando se pulsa el botoncito del ojo de la celda.
     - Parámetro: recibe el identificador del boton pulsado. */
    @objc func editGroupAction(sender: UIButton) {
        
        quickLookIndex = sender.tag
        let previewController = QLPreviewController()
        previewController.dataSource = self
        previewController.delegate = self
        present(previewController, animated: true)
    }
    
    // MARK: **************** [CollectionView] ****************/

    //Numero de elementos de la celda
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == self.collectionView {
        
            return numElementos
        }
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "estandarCell", for: indexPath) as! estandarCell
            switch categoriaSeleccionada {
                case Element.Categoria.nota:

                     cell.imagenEstandar.image = UIImage(named: "note")
                     cell.nombreEstandar.text = "Nota"
                return cell

                case Element.Categoria.dibujar:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paletaCell", for: indexPath) as! paletaCell
                return cell
                case Element.Categoria.objectos3D:
                    cell.imagenEstandar.image = UIImage(named: modelos3D[indexPath.row].iconoImagen)
                    cell.nombreEstandar.text = modelos3D[indexPath.row].nombre
                    cell.quickLook.tag = indexPath.row
                    //Añadimos el selector para que llame al metodo indicado al pulsarse
                    cell.quickLook.addTarget(self, action: #selector(editGroupAction(sender:)), for: .touchUpInside)
                    cell.quickLook.contentEdgeInsets = UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
                    return cell
                case Element.Categoria.cuadro:
                    
                    cell.imagenEstandar.image = UIImage(named: cuadros[indexPath.row].nombreIcono)
                    cell.nombreEstandar.text = cuadros[indexPath.row].nombre
                    
                    return cell

                case Element.Categoria.caso5:  return cell

                case Element.Categoria.caso6:  return cell

                default:
                    print("Algo no salió bien")
                     return cell
            }
        
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellButton", for: indexPath) as! menuVRCollectionViewCell
           
            switch indexPath.row {
                case 0:
                    // var cellImg : UIImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 25, height: 25))
                    // cellImg.image = UIImage(named: "notablanco")
                    // cell.addSubview(cellImg)
                    cell.imagenBoton.image = UIImage(named:IconoGris[indexPath.row])
                case 1:
                    cell.imagenBoton.image = UIImage(named:IconoGris[indexPath.row])
                case 2:
                    cell.imagenBoton.image = UIImage(named:IconoColor[indexPath.row])
                    indexPathSelected = indexPath
                case 3:
                    cell.imagenBoton.image = UIImage(named:IconoGris[indexPath.row])
                
                case 4:
                    cell.imagenBoton.image = UIImage(named:IconoGris[indexPath.row])
                case 5:
                    cell.imagenBoton.image = UIImage(named:IconoGris[indexPath.row])
                default:
                    print("Algo no salió bien")
            }
            
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {

            numElementoSeleccionado = indexPath.row
            performSegue(withIdentifier: "sendElement", sender: title)
            
        }else{
            
            if indexPathSelected != indexPath {
                //Añade la Categoria respecto a la posicion del ENUM
                //                categoriaSeleccionada = Categoria(rawValue: indexPath.row)
                cambiarCategoria(num: indexPath.row)
                cambiarImagenCategoria(num: indexPath,collection: collectionView)
                
            }
        }
    }

    // MARK: **************** [ARQUICK LOOK] ****************/
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        
        
        let url = Bundle.main.url(forResource: "art.scnassets/\(modelos3D[quickLookIndex].archivo3D)", withExtension: "usdz")!
        return url as QLPreviewItem
        
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
  
    // MARK: **************** [INTERFAZ] ****************/
    //Se encarga de cambiar los elementos a mostrar dependiendo al cambiar de categoría
    func cambiarCategoria(num: Int){
        
        switch num {
        case 0:
            comingSoon.isHidden = true
            collectionView.isHidden = false
            categoriaSeleccionada = Element.Categoria.nota
            numElementos = notas.count
            DispatchQueue.main.async{
                self.collectionView.reloadData()
            }
        case 1:
            comingSoon.isHidden = true
            collectionView.isHidden = false
            categoriaSeleccionada = Element.Categoria.dibujar
            numElementos = paletasColores.count
            DispatchQueue.main.async{
                
                self.collectionView.reloadData()
            }
            print("caso\(num)")
        case 2:
            comingSoon.isHidden = true
            collectionView.isHidden = false
            categoriaSeleccionada = Element.Categoria.objectos3D
            numElementos = modelos3D.count
            DispatchQueue.main.async{
                
                self.collectionView.reloadData()
            }
        case 3:
            comingSoon.isHidden = true
            collectionView.isHidden = false
            categoriaSeleccionada = Element.Categoria.cuadro
            numElementos = cuadros.count
            DispatchQueue.main.async{
                self.collectionView.reloadData()
            }
            
        case 4:
            comingSoon.isHidden = false
            collectionView.isHidden = true
        case 5:
            comingSoon.isHidden = false
            collectionView.isHidden = true
        default:
            print("Otherwise, do something else.")
        }
    }
    
    //Muestra el icono de la categoría pulsada de forma colorida para destacarla
    func cambiarImagenCategoria(num: IndexPath, collection: UICollectionView){
        
        //Comprobamos que se haya seleccionado un elemento
        if let indexPathSelected = indexPathSelected {
            
            let cell = collection.cellForItem(at: indexPathSelected) as! menuVRCollectionViewCell
            print(indexPathSelected.row)
            print(cell)
             let imagen = UIImage(named: IconoGris[(indexPathSelected.row)])!
            cell.imagenBoton.image = imagen
            
            let cellSelected = collection.cellForItem(at: num) as! menuVRCollectionViewCell
            let imagenSelected = UIImage(named: IconoColor[num.row])!
            cellSelected.imagenBoton.image = imagenSelected
            self.indexPathSelected = num
            
        } 
    }

    // MARK: **************** [3D TOUCH] ****************/
    // 3D Touch que se ejecuta al presionar ligeramente
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {

        guard let indexPath = collectionView.indexPathForItem(at: location),
            let cell = collectionView.cellForItem(at: indexPath)
            else { return nil }
        
        // Enable blurring of other UI elements, and a zoom in animation while peeking.
        previewingContext.sourceRect = cell.frame
        // Create and configure an instance of the color item view controller to show for the peek.
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "PreviewControllerCustom") as? PreviewControllerCustom
            else { preconditionFailure("Expected a ColorItemViewController") }
        viewController.nombreTexto = modelos3D[indexPath.row].archivo3D
        viewController.funcionalidades = modelos3D[indexPath.row].funcionalidades
        
        viewController.numElement = indexPath.row
        
        return viewController
    }
    
    // 3D Touch  Se ejecuta cuando presionas hasta el final
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        // No hace nada
    }
}

