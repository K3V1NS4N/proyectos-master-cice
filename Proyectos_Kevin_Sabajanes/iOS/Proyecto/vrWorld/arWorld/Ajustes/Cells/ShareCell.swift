//
//  ShareCell.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 21/05/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

class ShareCell: UICollectionViewCell {

	@IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var button: viewDesignable!
    
    @IBOutlet weak var nombreAjuste: UILabel!
    
    override func awakeFromNib() {
		super.awakeFromNib()
	}	
}
