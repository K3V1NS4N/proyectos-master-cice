//
//  AjustesCell.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 06/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

//Se encarga de gestionar cuando un switch cambia su estado en la pantalla de ajustes
// TODO: Falta funcionalidad
class SwitchCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let switchView = UISwitch(frame: .zero)
        switchView.setOn(false, animated: false)
        switchView.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        accessoryView = switchView
    }
    @objc func valueChanged(sender: UISwitch) {
        print((textLabel?.text ?? "") + " switch is " + (sender.isOn ? "ON" : "OFF"))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
