//
//  AjustesController.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 06/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//


import UIKit

//Es la clase encargada de mostrar los ajustes de una categoria, sus secciones.
class AjustesController: UITableViewController {

    var ajustesDict = [String : [String]]()
    var ajustesSectionTitle = [String]()
    //Categoria seleccionada, fundamental para gestionar el tipo de ajsutes que se van a mostrar
    var categoriaSeleccionada = CategoriaAjuste()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        navigationController?.setNavigationBarHidden(false, animated: true)
 
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return categoriaSeleccionada.secciones.count
    }
    //Añade la seccion de los ajustes
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categoriaSeleccionada.secciones[section].nombre
    }
    
    //Cuenta el numero de secciones
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return categoriaSeleccionada.ajustes.count
    }
    
    //Rellena la tabla con sus respectivas celdas dependiendo del tipo de celda añadiremos una u otra ( switch o picker )
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let funcionalidad = categoriaSeleccionada.ajustes[indexPath.row]
        
        if funcionalidad.tipoCelda == TipoCelda.nombre.swich{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwitchCell
            cell.textLabel?.text = funcionalidad.ajuste.1
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath) as! PickerCell
            
            cell.textLabel?.text = funcionalidad.ajuste.1
   
            return cell
        }
    }
    
    //Añadimos el footer a la tabla, encargado de mostrar la descripcion de esa seccion y ponerlo bonito :))
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        //Margenes
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width/1.3, height: 80))
        label.center = CGPoint(x:tableView.frame.size.width/2, y: 35)
        //Alineacion
        label.textAlignment = .center
        //Num de lineas
        label.numberOfLines = 0
        //Texto a patir del objeto
        label.text = categoriaSeleccionada.secciones[section].descripcion
        //Tamaño del texto
        label.font = label.font.withSize(15)
        //Color
        label.textColor = UIColor.lightGray
        //Se añade a la vista del footer
        footerView.addSubview(label)
        //Color del background
        footerView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
        return footerView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        guard let indice = ajustesSectionTitle.firstIndex(of: title) else {
            return -1
        }
        return indice
    }
    
    //Se encarga de mostrar la parte superior de la seccion y darle un formato para que quede guay
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        //Customizarla
        headerView.backgroundView?.backgroundColor = #colorLiteral(red: 0.9354761243, green: 0.9356574416, blue: 0.956404388, alpha: 1)
        headerView.textLabel?.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.6004120291)
//        headerView.textLabel?.font = UIFont(name: "Chalkduster", size: 15)
        headerView.textLabel?.numberOfLines = 0
        headerView.textLabel?.lineBreakMode = .byWordWrapping
        headerView.textLabel?.sizeToFit()
        
    }
}
