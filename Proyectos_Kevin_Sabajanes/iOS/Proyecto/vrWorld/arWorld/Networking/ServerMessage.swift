//
//  ServerMessage.swift
//  arWorld
//
//  Created by kevinsan on 25/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase


class ServerMessage{
    
    var ref: DatabaseReference!
    var delegate: InicioMessage?
    func getMessage() {

        //Referencia a la base de datos
        ref = Database.database().reference()
    
        //Base al identificador del usuario obtenemos sus datos
        ref.child("mensaje").observeSingleEvent(of: .value, with: { (snapshot) in
            // Dicha captura la alamcenamos como un diccionario
            let value = snapshot.value as? String
            if let value = value {
                if value.isNotEmpty{
                    //Le mandamos los elementos obtenidos al MainMapController
                    self.delegate?.didReceiveData(value)
                    print("aaaaaaaaaaaaaaa\(value)")
                }
            }

        }) { (error) in
            print(error.localizedDescription)
        }
        
        
    }
 
}
