//
//  getJsonMap.swift
//  arWorld
//
//  Created by kevinsan on 26/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

 class Download{
    var ref: DatabaseReference!
    
     func getMap(latitude: Double, longitude: Double, json: JsonParser) {
        
        //Accedemos a la localizaciones (Elementos subidos) que hay alamcenados en el rango correspondiente a nuestra localizacion
        var latitudeString = String(latitude).replacingOccurrences(of: ".", with: "_", options: .literal, range: nil)
        var longitudeString = String(longitude).replacingOccurrences(of: ".", with: "_", options: .literal, range: nil)
        
        var value: NSDictionary?
        ref = Database.database().reference()
        ref.child("rangos").child("\(latitudeString)").child("\(longitudeString)").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            value = snapshot.value as? NSDictionary
            if let value = value {
                
                json.parsearJsonMapa(jsonNSData: value)
            }

        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
     func getUserInfo() {
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            Info.userName = value?["username"] as? String ?? ""
            Info.coins = value?["coins"] as? Int ?? 0
    
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
}

