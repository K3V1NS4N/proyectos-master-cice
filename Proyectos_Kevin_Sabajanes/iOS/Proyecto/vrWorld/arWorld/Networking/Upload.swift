//
//  uploadData.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 03/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class Upload{

    var ref: DatabaseReference!
    
    /// //Se encacarga de subir la localizacion del elemento base a la localizacion
    /// (Desactivado por no tener terminos y condiciones de privacidaz)
    ///
    /// - Parameters:
    ///     - latitud: Latitud del elemento.
    ///     - longitud: longitud del elemento.
    ///     - elementoMapa: Tipo de elemento que se va a subir y el nombre que identifica ese valor para llamarlo.
    func element(latitud: String, longitud: String, elementoMapa: [String : Double]){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        ref.child("rangos").child(latitud).child(longitud).child("l").childByAutoId().setValue(elementoMapa){
            (error:Error?, ref:DatabaseReference) in
                
            if let error = error {
                print("Data could not be saved: \(error).")
            }
        }
    }
}
