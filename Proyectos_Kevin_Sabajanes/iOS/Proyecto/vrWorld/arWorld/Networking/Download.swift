//
//  getJsonMap.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 26/06/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

 class Download{
    var ref: DatabaseReference!
    
     func getMap(latitude: Double, longitude: Double, json: JsonParser) {
        
        //Accedemos a la localizaciones (Elementos subidos) que hay alamcenados en el rango correspondiente a nuestra localizacion
        // Remplza los puntos por _ por que en Firebase no se pueden almacenar dichos caracteres
        var latitudeString = String(latitude).replacingOccurrences(of: ".", with: "_", options: .literal, range: nil)
        var longitudeString = String(longitude).replacingOccurrences(of: ".", with: "_", options: .literal, range: nil)
        
        var value: NSDictionary?
        //Referencia a la base de datos
        ref = Database.database().reference()
        //Obtenemos los elementos de una localizacion concreta
        ref.child("rangos").child("\(latitudeString)").child("\(longitudeString)").observeSingleEvent(of: .value, with: { (snapshot) in
            // Obtenemos los valores como diccionario
            value = snapshot.value as? NSDictionary
            if let value = value {
                // Parseamos el resultado
                json.parsearJsonMapa(jsonNSData: value)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    //Obtiene la informacion del usuario
     func getUserInfo() {
        //Referencia a la base de datos
        ref = Database.database().reference()
        //Obtenemos la id del usuario en la sesion actual
        let userID = Auth.auth().currentUser?.uid
        //Base al identificador del usuario obtenemos sus datos
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Dicha captura la alamcenamos como un diccionario
            let value = snapshot.value as? NSDictionary
            //Obtenemos su nombre de usuario
            Info.userName = value?["username"] as? String ?? ""
            //Obtenemos las monedas que tiene almacenadas
            Info.coins = value?["coins"] as? Int ?? 0
    
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    func getMessageFromServer() {
        
        
        
        
    }
}

