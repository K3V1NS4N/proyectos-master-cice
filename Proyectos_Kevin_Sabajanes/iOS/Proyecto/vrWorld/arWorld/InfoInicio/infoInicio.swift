//
//  infoInicio.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit

//Se encarga de dar cierta informacion acerca de los elementos que hay
class infoInicio: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let infoElementos = DatosInfoElementosInicio.infoElementos
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoElementos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCellInfoInicio
        cell.icon?.image = UIImage(named: infoElementos[indexPath.row].nombreIcono)
        
        cell.nombre?.text = infoElementos[indexPath.row].nombre
        
        return cell
    }

}
