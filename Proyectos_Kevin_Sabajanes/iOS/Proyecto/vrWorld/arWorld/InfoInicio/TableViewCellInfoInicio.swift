//
//  TableViewCellInfoInicio.swift
//  arWorld
//
//  Created by Kevin Sabajanes on 09/07/2019.
//  Copyright © 2019 K3V1NS4N. All rights reserved.
//

import UIKit


class TableViewCellInfoInicio: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nombre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
