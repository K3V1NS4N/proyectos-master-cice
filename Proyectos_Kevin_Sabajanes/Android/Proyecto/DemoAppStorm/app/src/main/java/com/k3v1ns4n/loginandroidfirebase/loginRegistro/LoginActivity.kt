package com.k3v1ns4n.loginandroidfirebase.loginRegistro

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.google.firebase.auth.*
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterLoginButton
import kotlinx.android.synthetic.main.activity_main2.*
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.DefaultLogger
import com.twitter.sdk.android.core.TwitterConfig
import com.google.firebase.auth.FacebookAuthProvider
import com.k3v1ns4n.loginandroidfirebase.R
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import java.util.regex.Pattern


/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */


class LoginActivity : AppCompatActivity() {


    // Se inicializa la variable de autenticacion de firebase
    private val auth = FirebaseAuth.getInstance()


    var callbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {

        //<<<<<<<<<<<<<<<<<<<<<<< [TWITTER] >>>>>>>>>>>>>>>>>>>>>>>>

        // Se añade la configuracion del login de twitter
        val config = TwitterConfig.Builder(this)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(TwitterAuthConfig("gEChsiMls591K07OMQnOiB9pI", "7s7FL03llC5IToOWZmdEghi0sKR2jISj2Nt1OJAhwcKnZlUpv5"))
            .debug(true)
            .build()
        Twitter.initialize(config)
        setContentView(R.layout.activity_main2)

        // Se declara el boton de twitter
        val twitterButton :TwitterLoginButton = findViewById(R.id.twitter_button)
        super.onCreate(savedInstanceState)



        /**
         * Desactivado el logeo con Twitter para no tratar datos personales de nadie, pero es totalmente funcional.
         */

        /*
          Adding a callback to loginButton
          These statements will execute when loginButton is clicked

        twitterButton.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                Log.d("TWITTERLOG", "twitterLogin:success$result")
                handleTwitterSession(result.data)
                mensaje("SUCCESSS")
            }

            override fun failure(exception: TwitterException) {
                Log.w("TWITTERLOG1", "twitterLogin:failure$exception", exception)
                mensaje("FAILURE")
                //updateUI(null)

            }
        }

        */
        /**
         * Desactivado el logeo con Facebook para no tratar datos personales de nadie, pero es totalmente funcional.
         */
        /*

        //<<<<<<<<<<<<<<<<<<<<<< [FACEBOOK] >>>>>>>>>>>>>>>>>>>>>>>>


        // [START initialize_fblogin]
        // Initialize Facebook Login button
        callbackManager = CallbackManager.Factory.create()

        buttonFacebookLogin.setReadPermissions("email", "public_profile")
        buttonFacebookLogin.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {

            override fun onSuccess(loginResult: LoginResult) {
                Log.d("FACEBOOKLOG", "facebook:onSuccess:$loginResult")
                mensaje("SUCCESSFUL")
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                Log.d("FACEBOOKLOG", "facebook:onCancel")
                mensaje("CANCELADO")
                // [START_EXCLUDE]
                updateUI(null)
                // [END_EXCLUDE]
            }

            override fun onError(error: FacebookException) {
                Log.d("FACEBOOKLOG", "facebook:onError", error)
                mensaje("ERROR")
                // [START_EXCLUDE]
                updateUI(null)
                // [END_EXCLUDE]
            }
        })

        // [END initialize_fblogin]

        */







        /*
        //Añadimos la animacion de carga que se repite de forma indefinida
        val avd = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_anim)
        val iv = (findViewById(R.id.loader) as ImageView).apply {
            setImageDrawable(avd)
        }
        avd?.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
            override fun onAnimationEnd(drawable: Drawable?) {
                iv.post { avd.start() }
            }
        })
        avd?.start()
        */

    }

    /**
     * @param requestCode - we'll set it to REQUEST_CAMERA
     * @param resultCode - this will store the result code
     * @param data - data will store an intent
     */

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        val twitterAuthClient = TwitterAuthClient()
        //Esta comprobacion es importante para que no de error
        if (twitterAuthClient.requestCode == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data)
        }

        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)

    }

    // [START auth_with_twitter]
    // Se encarga de manejar la sesion en el momento de logearse y dar autorizacion
    private fun handleTwitterSession(session: TwitterSession) {
        Log.d("TWITTERLOG", "handleTwitterSession:$session")
        // [START_EXCLUDE silent]
        //showProgressDialog()
        // [END_EXCLUDE]

        val credential = TwitterAuthProvider.getCredential(
            session.authToken.token,
            session.authToken.secret)

        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("TWITTERLOG", "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("TWITTERLOG", "signInWithCredential:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }

                // [START_EXCLUDE]
                //hideProgressDialog()
                // [END_EXCLUDE]
            }
    }
    // [END auth_with_twitter]



    // [START auth_with_facebook]
    // Se encarga de manejar la sesion en el momento de logearse y dar autorizacion
    private fun handleFacebookAccessToken(token: AccessToken) {
        mensaje("SUCCESSFUL")
        Log.d("FACEBOOKLOG", "handleFacebookAccessToken:$token")
        // [START_EXCLUDE silent]
        //showProgressDialog()
        // [END_EXCLUDE]

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("FACEBOOKLOG", "signInWithCredential:success")
                    mensaje("SUCCESSFUL")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("FACEBOOKLOG", "signInWithCredential:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }

                // [START_EXCLUDE]
                //hideProgressDialog()
                // [END_EXCLUDE]
            }
    }
    // [END auth_with_facebook]



    // Se ejecuta al inicio para comprobar si hay un usuario logeado
    public override fun onStart() {
        super.onStart()

        // Comprueba si se ha logeado y base  a eso avanzamos a la siguiente apntalla
        val currentUser = auth?.currentUser
        if(currentUser!= null){
            updateUI(currentUser)
        }
    }

    //Metodo al que se llama al pulsar el boton de logearse
    fun login2 (view: View){

        // Reiniciar los erroes
        user.error = null

        // Se obtienen los valores de los campos
        val email = user.text.toString()
        val pass =  pass.text.toString()

        // Se comprueba que se pasa un email valido
        if (TextUtils.isEmpty(email)) {
            user.error = getString(R.string.error_field_required)
        } else if (!validaEmail(email)) {
            user.error = getString(R.string.error_invalid_email)
        }else{

            // Se ocultan los elementos y se muestra una animacion de carga
            user.visibility = View.GONE
            this.pass.visibility = View.GONE
            loader.visibility = View.VISIBLE

            /**
             *
             * Se logea con un usuario predefinido por lo que no se envia ningun tipo de informacion al servidor por parte
             * del posible usuario que pueda querer logearse.
             */
            //Manda el email y la contraseña al servidor de firebase
            auth?.signInWithEmailAndPassword("prueba@prueba.com", "12345prueba")
                ?.addOnCompleteListener(this
                ) { p0 ->
                    if (p0.isSuccessful) {

                        // Sign in success, update UI with the signed-in user's information
                        Log.d("LOGIN1", "signInWithEmail:success")

                        val user = auth?.currentUser
                        updateUI(user)

                    } else {

                        // If sign in fails, display a message to the user.
                        Log.w("LOGIN1", "signInWithEmail:failure", p0.exception?.cause)

                        val error = p0.exception?.toString()
                        val mensaje = error?.split(":")
                        mensaje("${mensaje!![1]}")
                        // Se ocultan los elementos y se muestra una animacion de carga
                        user.visibility = View.VISIBLE
                        this.pass.visibility = View.VISIBLE
                        loader.visibility = View.GONE
                    }
                }

        }

    }

    // Al pulsar el boton de anonymous se logeara automaticamente como anonimo
    /*
    fun anonymousLogin(view: View){

        auth?.signInAnonymously()
            ?.addOnCompleteListener(this
            ) { p0 ->
                if (p0.isSuccessful) {

                    // Sign in success, update UI with the signed-in user's information
                    Log.d("LOGIN1", "signInWithEmail:success")

                    mensaje("Satisfactorio")

                    val user = auth.CurrentUser
                    updateUI(user)

                } else {

                    // If sign in fails, display a message to the user.
                    Log.w("LOGIN1", "signInWithEmail:failure", p0.exception)
                    mensaje("Fallido")

                    updateUI(null)
                }
            }
    }
    */

    private fun validaEmail(email: String):Boolean {

        //Se obtiene un pattern ya creado y mas efectivo para validar el email
        val pattern: Pattern = Patterns.EMAIL_ADDRESS

        //Se comprueba si el email introducido es un email valido a partir del pattern
        return pattern.matcher(email).matches()

    }

    fun registrarse(view: View){

        //Abre la activity encargada de registrarse
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)

    }

    //Accede a la pantalla de acceso
    fun updateUI(usuario: FirebaseUser?){

        Toast.makeText(this, "Te has logeado como ${usuario?.email}",Toast.LENGTH_SHORT).show()
        // Comprobar si tenemos un usuario
        usuario?.email
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        //Se finaliza la actividad para no poder volver hacia atras
        finish()

    }

    fun mensaje (mensaje:String?){
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
    }

    fun onClick(v: View) {
        if (v == fb) {
            buttonFacebookLogin.performClick()
        }
    }
    fun back (v: View){

        this.finish()
    }
}
