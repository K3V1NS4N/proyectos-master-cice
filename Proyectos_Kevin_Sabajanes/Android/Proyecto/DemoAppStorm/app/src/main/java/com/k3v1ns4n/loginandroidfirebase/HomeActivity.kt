package com.k3v1ns4n.loginandroidfirebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_home.*
import android.view.View
import com.k3v1ns4n.loginandroidfirebase.AñadirFuncionalidad.addIdea
import android.net.NetworkInfo
import android.net.ConnectivityManager
import android.os.Handler
import android.util.Log
import com.k3v1ns4n.loginandroidfirebase.Objetos.CurrentUser
import com.k3v1ns4n.loginandroidfirebase.Objetos.Ideas
import com.k3v1ns4n.loginandroidfirebase.Perfil.Profile

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class HomeActivity : AppCompatActivity() {

    // Se inicializa la variable de autenticacion de firebase
    private val auth = FirebaseAuth.getInstance()
    val bd = BD()

    //Se declara la referencia a la base de datos
    private lateinit var FirebaseBD: DatabaseReference // BD FIREBASE

    /**[1] & [2]**/
    //Instancia de la clase BD necesaria para obtener y subir vinos
    lateinit var BD_vinos: BD

    /**[1] & [2]**/
    //Instancia de firebase necesaria para subir y descargar las fotos
    val storage = FirebaseStorage.getInstance()

    var scrolled: Boolean = false
    var hidden: Boolean = false

    /**[1] & [2]**/
    //Instancia de firebase necesaria para subir y descargar las fotos
    var taskListener: ValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {

            Log.i("ERROR", "ERROR $p0")
        }


        override fun onDataChange(snapshot: DataSnapshot) {
            conexionStatus.visibility = View.INVISIBLE
            /**[1]**/
            //Esta variable es una forma de contar las comprobaciones al recurso IdeaObjects:ArrayList<Idea>
            // de la clase BD ya que hay cierto delay al cargar las fotos y generar los objetos Idea.
            var intento = 0
            Thread {
                while(intento!=30) {

                    val userID = CurrentUser.UID
                    if (userID != null && userID.isNotEmpty()) {
                        bd.loadLikes(snapshot.child("users").child(userID).child("likes"))
                    }

                    Thread.sleep(550)
                    bd.cargarIdeas(snapshot)
                    Thread.sleep(100)
                    //Comprobamos que se hayan obtenido los elementos necesarios
                    if((Ideas.ideas?.size != 0) && (Ideas.ideasFirebaseCount).toInt() == Ideas.ideas?.size){
                        intento = 30

                        runOnUiThread {
                            recycler_view.adapter = HomeAdapter(this@HomeActivity,false)
                            swiperefresh.isRefreshing = false
                        }
                    }else{

                        intento++

                    }
                }
            }.start()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this) /**[1] & [2]**/
        setContentView(R.layout.activity_home)

        Package.instance = this.getPackageName()



        Thread(Runnable {
            // [-----------------<| BD FIREBASE |>-----------------] //
            /**[1]**/
            FirebaseBD = FirebaseDatabase.getInstance().reference //Se obtiene la referencia
            /**
             *
             * If you need to receive data one time you need to use addListenerForSingleValueEvent(...)
             * instead of addValueEventListener(...). Then onDataChange() will return only one time
             *
             * **/

            FirebaseBD.orderByKey().addListenerForSingleValueEvent(taskListener)
            /**[1 & 2]**/
            BD_vinos = BD()


            // Se obtiene la instancia de autenticacion actual
            val currentUser = auth?.currentUser

            //En caso de que el email sea null
            if(currentUser?.email==null){

                //Se mostrara como anónimo
                //email.text = "Anonymous"

            }else{

                //Se muestra el nombre del usuario como email
                //email.text = CurrentUser?.email.toString()

            }


            this@HomeActivity.runOnUiThread(java.lang.Runnable {

                recycler_view.layoutManager = LinearLayoutManager(this)

            })

        }).start()


        fab.setOnClickListener {
            val intent = Intent(this, addIdea::class.java)

            this.startActivity(intent)
        }

        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        swiperefresh.setOnRefreshListener {

            // This method performs the actual data-refresh operation.
            // The method calls setRefreshing(false) when it's finished.
            recycler_view.adapter.notifyDataSetChanged()
            FirebaseBD.orderByKey().addListenerForSingleValueEvent(taskListener)

        }

        profile.setOnClickListener {

            val intent = Intent(this, Profile::class.java)
            startActivity(intent)

        }

        var cm = this.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager

        var activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        var isConnected = activeNetwork != null && activeNetwork!!.isConnectedOrConnecting


        if (!isConnected){

            conexionStatus.visibility = View.VISIBLE

        }

        recycler_view.viewTreeObserver.addOnScrollChangedListener {

            fab.visibility = View.INVISIBLE
            profile.visibility = View.INVISIBLE

            //Prevenimos que se ejecute el handler varias veces
            if (!scrolled){

                scrolled = true
                Handler().postDelayed({
                    scrolled = false
                    fab.visibility = View.VISIBLE
                    profile.visibility = View.VISIBLE
                }, 1500)
            }
        }
    }
}

