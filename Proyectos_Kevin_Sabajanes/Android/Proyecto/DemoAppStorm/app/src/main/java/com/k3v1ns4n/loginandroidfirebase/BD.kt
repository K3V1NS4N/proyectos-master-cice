package com.k3v1ns4n.loginandroidfirebase

import android.util.Log
import com.k3v1ns4n.loginandroidfirebase.AñadirFuncionalidad.*
import java.util.HashMap
import com.google.firebase.FirebaseError
import com.google.firebase.database.*
import com.k3v1ns4n.loginandroidfirebase.Objetos.*


/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class BD {


    /**
     * Método que va en << onDataChange >> encargado de generar los objetos << Idea >>
    * a partir de la petición a FIREBASE que va a recibir.
    *
    * @param  DataSnapshot
    * @return ArrayList<Idea>?
    * @author K3V1NS4N
    *
    */

     fun cargarIdeas(dataSnapshot: DataSnapshot) {

        val ideasFirebase = dataSnapshot.children.iterator()
        Ideas.ideas?.clear()
        if(ideasFirebase.hasNext()) {

            val listaIndex = ideasFirebase.next()
            val itemsIterator = listaIndex.children.iterator()
            //Obtiene el numero de ideas que hay en la base de datos
            Ideas.ideasFirebaseCount = listaIndex.childrenCount
            val likesUser = CurrentUser.likes

            while (itemsIterator.hasNext()) { //Recorre todos los vinos
                //Obtenemos la información de ellos
                val ideaActual = itemsIterator.next()

                val idea = Idea()
                Log.i("IDEA", "${ideaActual}")
                val map = ideaActual.value as HashMap<String, Any>

                //Se añade el objeto Idea con sus propiedades
                Log.i("IDEA", "${idea.comments}")
                //vino.ID = vinoActual.key
                idea.author = map["author"] as String

                var funcionalidades = map["funcionalidades"] as ArrayList<HashMap<String, Any>>

                var todas: ArrayList<FuncionalidadSubida>? = ArrayList()
                for(funcion in funcionalidades){

                    val funcionalidad = FuncionalidadSubida()
                    funcionalidad.nombre  = funcion["nombre"] as String
                    funcionalidad.Icon  = funcion["icon"] as String
                    funcionalidad.f_menores  = funcion["f_menores"] as ArrayList<String>
                    todas?.add(funcionalidad)
                }
                val likesUser = CurrentUser.likes
                likesUser?.get(idea?.key)
                //idea.mainFuncionalitiesIcons = map["mainFuncionalitiesIcons"] as ArrayList<String>
                idea.shortDescription = map["shortDescription"] as String
                idea.likes = map["likes"] as Long
                idea.description = map["description"] as String
                idea.comments = map["comments"] as Long
                idea.funcionalidades = todas
                idea.key = ideaActual.key

                val resultado = likesUser?.get(idea?.key)
                if (resultado!= null && resultado){

                    idea.liked = true
                }

                Ideas.ideas?.add(idea)

            }
        }
    }

    /**
     * Es un método encargado de guardar las ideas en la base de datos Firebase.
     *
     * @param  DatabaseReference, Idea
     * @author K3V1NS4N
     *
     */

    fun guardarIdea(FirebaseBD: DatabaseReference, idea: Idea) {

        var respuesta = false
        //Lo obtenemos de la base de datos
        val identificadorTarea = FirebaseBD.child("ideas").push()

        //Enviamos el vino a traves del id
        identificadorTarea.setValue(idea).addOnSuccessListener {
            // Write was successful!
            BDINSTANCE.respuesta = true
        }.addOnFailureListener {
            // Write failed
            BDINSTANCE.respuesta = false
        }
    }

    /**
     * Guarda la información del usuario una vez se logea.
     *
     * @param  DatabaseReference, UserID, Nombre, Email
     * @author K3V1NS4N
     *
     */
    fun guardarUsuario(FirebaseBD: DatabaseReference,userId: String, name: String, email: String){

        val user = User(name, email)
        FirebaseBD.child("users").child(userId).setValue(user)

    }

    /**
     * Método encargado de dar like o quitarselo, este no solo almacena en el usuario a que fotos has dado like,
     * si no que incrementar o decrementar un contador en la foto para contabilizar el número de likes mediante
     * transiciones.
     *
     * @param  DatabaseReference, FotoID, UserId, Estado (SI SE VA A DAR LIKE O QUITARSELO)
     * @author K3V1NS4N
     *
     */
    fun addLike(FirebaseBD: DatabaseReference,idFoto: String,userId: String,
                estado: Boolean){


        FirebaseBD.child("users").child(userId).child("likes").child(idFoto).setValue(estado)
        incrementCounter(FirebaseBD.child("ideas").child(idFoto).child("likes"),estado)
    }


    /**
     * Método encargado de llevar a cabo la transacción, gracias a esto si varias personas dan like al mismo tiempo
     * no va a haber problemas a la hora de incrementar o decrementar el contador
     *
     * @param  DatabaseReference, FotoID, UserId, Estado (SI SE VA A DAR LIKE O QUITARSELO)
     * @author K3V1NS4N
     *
     */
    fun incrementCounter(FirebaseBD: DatabaseReference, estado: Boolean) {

        FirebaseBD.runTransaction(object : Transaction.Handler {
            override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
               //To change body of created functions use File | Settings | File Templates.
            }

            override fun doTransaction(currentData: MutableData): Transaction.Result {
                //En el caso de no tener valor le damos uno
                if (currentData.value == null) {
                    currentData.value = 1

                   //Si no tiene valor pero se quiere quitar un like por cualquier razón lo inciialziamos a 0
                } else if (currentData.value == null && !estado)  {
                    currentData.value = 0
                }else if (!estado)  { //Decrementa
                    currentData.value = (currentData.value as Long?)!! - 1
                }else { //Incrementa
                    currentData.value = (currentData.value as Long?)!! + 1
                }

                return Transaction.success(currentData)
            }

            fun onComplete(firebaseError: FirebaseError?, committed: Boolean, currentData: DataSnapshot) {
                if (firebaseError != null) {


                } else {

                }
            }
        })
    }

    /**
     * Se encarga de obtener el numero de likes que tiene la foto
     *
     * @param  DataSnapshot
     * @author K3V1NS4N
     *
     */
    fun loadLikes(dataSnapshot: DataSnapshot){

        val likes = dataSnapshot.children.iterator()

        while(likes.hasNext()) {

            val user = CurrentUser
            val ideaActual = likes.next()
            val key = ideaActual.key as String
            val valor = ideaActual.value as Boolean

            user.likes?.put(key,valor)

        }
    }

    /**
     * Se encarga de subir un comentario en la base de datos
     *
     * @param  DatabaseReference, fotoID, comentario a subir
     * @author K3V1NS4N
     *
     */

    fun uploadComment(FirebaseBD: DatabaseReference, ideaID: String, comment: String){

        FirebaseBD.child("zcomments").child(ideaID).push().setValue(comment).addOnSuccessListener {
            // Write was successful!
            incrementCommentsCounter(FirebaseBD.child("ideas").child(ideaID).child("comments"))
            BDINSTANCE.respuesta = true
        }.addOnFailureListener {
            // Write failed
            BDINSTANCE.respuesta = false
        }
    }

    /**
     * Funciona de la misma manera que al dar like solo que aqui simplemente se aumenta 1 por cada comentario en la foto
     *
     * @param  DatabaseReference
     * @author K3V1NS4N
     *
     */
    fun incrementCommentsCounter(FirebaseBD: DatabaseReference) {

        FirebaseBD.runTransaction(object : Transaction.Handler {
            override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun doTransaction(currentData: MutableData): Transaction.Result {

                if (currentData.value == null) {
                    currentData.value = 1
                }else {
                    currentData.value = (currentData.value as Long?)!! + 1
                }

                return Transaction.success(currentData)
            }

            fun onComplete(firebaseError: FirebaseError?, committed: Boolean, currentData: DataSnapshot) {
                if (firebaseError != null) {


                } else {

                }
            }
        })
    }

    /**
     * Se encarga de obtener todos los comentarios de una idea
     *
     * @param  DatabaseReference, idea
     * @author K3V1NS4N
     *
     */
    fun loadComments(dataSnapshot: DataSnapshot,idea: Idea): Idea {

        val comentarios = dataSnapshot.children.iterator()
        //Limpiamos los comentarios para que no se dupliquen si ya se han cargado
        idea.comentarios?.clear()

        while(comentarios.hasNext()) {

            val comentario = Comentario()
            val valor = comentarios.next().value as String
            comentario.comentario = valor
            idea.comentarios?.add(comentario)
        }
        return idea
    }

    /**
     * Carga el mensaje informativo proveniente del servidor (El mensaje al principio de la aplicación)
     *
     * @param  DatabaseReference
     * @author K3V1NS4N
     *
     */
    fun loadMensaje(dataSnapshot: DataSnapshot){

        BDINSTANCE.mensaje = dataSnapshot.value as String
    }
}