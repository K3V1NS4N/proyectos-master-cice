package com.k3v1ns4n.loginandroidfirebase.AñadirFuncionalidad

import com.google.firebase.database.IgnoreExtraProperties

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

//Aqui se almacena la informacion del usuario que se ha logeado
@IgnoreExtraProperties
data class User(
    var username: String? = "",
    var email: String? = ""
)
