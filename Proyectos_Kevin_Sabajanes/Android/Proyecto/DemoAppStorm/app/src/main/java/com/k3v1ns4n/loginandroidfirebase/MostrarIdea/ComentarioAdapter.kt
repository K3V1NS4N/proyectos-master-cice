package com.k3v1ns4n.loginandroidfirebase.MostrarIdea

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.k3v1ns4n.loginandroidfirebase.Objetos.Idea
import com.k3v1ns4n.loginandroidfirebase.R
import kotlinx.android.synthetic.main.comment.view.*

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

//Adaptador para los recuadros sencillos de los comentarios de cada IdEA
class ComentarioAdapter(idea: Idea) : RecyclerView.Adapter<ComentarioAdapter.ViewHolder>() {


    var idea = idea

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val vistaCelda = LayoutInflater.from(parent.context).inflate(R.layout.comment,parent,false)

        return ViewHolder(vistaCelda)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.comentario.text = idea.comentarios?.get(position)?.comentario ?: ""

    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> MEDIA16x9_ACTIONS_VIEW_TYPE
            else -> MEDIA16x9_ACTIONS_VIEW_TYPE
        }
    }

    companion object {
        const val MEDIA16x9_ACTIONS_VIEW_TYPE = 0
    }

    override fun getItemCount(): Int {

        return idea.comentarios?.size!!
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){



        init {



        }
    }
}