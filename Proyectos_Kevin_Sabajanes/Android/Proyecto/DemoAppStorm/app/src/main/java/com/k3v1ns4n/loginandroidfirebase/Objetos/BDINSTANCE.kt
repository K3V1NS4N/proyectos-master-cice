package com.k3v1ns4n.loginandroidfirebase.Objetos

import com.google.firebase.database.DatabaseReference

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */
object BDINSTANCE {

    lateinit var database: DatabaseReference
    var respuesta: Boolean? = null

    //Mensaje proveniente de la BD
    var mensaje: String = "mensaje"

}