package com.k3v1ns4n.loginandroidfirebase.Perfil

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import com.k3v1ns4n.loginandroidfirebase.HomeAdapter
import com.k3v1ns4n.loginandroidfirebase.MiDBHandler
import com.k3v1ns4n.loginandroidfirebase.R

import kotlinx.android.synthetic.main.activity_ideas_saved.*

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

//Se encarga de mostrar las ideas guardadas en SQL
class IdeasSaved : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ideas_saved)

        val dbHandler = MiDBHandler(this, null, null, 1)
        dbHandler.getIdeas()
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = HomeAdapter(null,true)

    }

}
