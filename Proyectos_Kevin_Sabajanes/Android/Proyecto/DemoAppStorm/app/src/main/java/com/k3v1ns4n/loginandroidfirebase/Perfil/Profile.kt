package com.k3v1ns4n.loginandroidfirebase.Perfil

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseAuth
import com.k3v1ns4n.loginandroidfirebase.R
import com.k3v1ns4n.loginandroidfirebase.loginRegistro.WelcomeActivity
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_profile.*


/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */
// Pantala en desarollo para el perfil del usuario
class Profile : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setSupportActionBar(toolbar)

        logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, WelcomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        yourideas.setOnClickListener {

            val intent = Intent(this, IdeasSaved::class.java)
            startActivity(intent)
            finish()
        }
    }
}
