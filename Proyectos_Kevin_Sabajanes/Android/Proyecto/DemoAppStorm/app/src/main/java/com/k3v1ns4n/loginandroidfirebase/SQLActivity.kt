package com.k3v1ns4n.loginandroidfirebase

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.k3v1ns4n.loginandroidfirebase.Objetos.*

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class MiDBHandler(context: Context,
                  nombreDB: String?,
                  factory: SQLiteDatabase.CursorFactory?,
                  version: Int):
    SQLiteOpenHelper(context, DATABASE_NAME,
        factory, DATABASE_VERSION) {


    companion object {

        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "ideasDB.db"


        val TABLE_IDEAS = "ideas"
        val TABLE_IDEAS2 = "funcionalidades"
        val TABLE_IDEAS3 = "funcionalidadesmenores"


        //idea

        val COLUMN_ID = "_id"
        val COLUMN_ALEATORIO = "aleatorio"
        val COLUMN_TITTLE = "tittle"
        val COLUMN_SHORTDESCRIPTION = "shortDescription"
        val COLUMN_DESCRIPTION = "description"

        //funcionalidades de las ideas
        val COLUMN_IDEA_ID = "idea_id"
        val COLUMN_NAME = "name"
        val COLUMN_ICON = "icon"


        //funcioanlidades de las funcionalidades
        val COLUMN_FUNCIONALIDAD_ID = "idea_id"
        val COLUMN_NAME2 = "name"
        val COLUMN_ICON2 = "icon"


    }

    override fun onCreate(p0: SQLiteDatabase?) {
        /*(CREATE TABLE products(_id INTEGER PRIMARY KEY AUTOINCREMENT,
            productName TEXT, quantity INTEGER))
        */

        val CREATE_IDEAS_TABLE = ("CREATE TABLE $TABLE_IDEAS"
                + "(" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_ALEATORIO
                + " TEXT, "
                + COLUMN_TITTLE
                + " TEXT, "
                + COLUMN_SHORTDESCRIPTION
                + " TEXT, "
                + COLUMN_DESCRIPTION
                + " TEXT"
                + ")")


        val CREATE_FUNCIONALIDADES_TABLE = ("CREATE TABLE $TABLE_IDEAS2"
                + "(" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_ALEATORIO
                + " TEXT, "
                + COLUMN_IDEA_ID
                + " INTEGER, "
                + COLUMN_NAME2
                + " TEXT, "
                + COLUMN_ICON2
                + " TEXT"
                + ")")

        val CREATE_FUNCIONALIDADES_MENORES_TABLE2 = ("CREATE TABLE $TABLE_IDEAS3"
                + "(" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_FUNCIONALIDAD_ID
                + " INTEGER, "
                + COLUMN_ICON
                + " TEXT"
                + ")")


        p0?.execSQL(CREATE_IDEAS_TABLE)
        p0?.execSQL(CREATE_FUNCIONALIDADES_TABLE)
        p0?.execSQL(CREATE_FUNCIONALIDADES_MENORES_TABLE2)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        // DROP TABLE IF EXISTS products
        //ELiminamos la tabla
        p0?.execSQL("DROP TABLE IF EXISTS $TABLE_IDEAS")
        p0?.execSQL("DROP TABLE IF EXISTS $TABLE_IDEAS2")
        p0?.execSQL("DROP TABLE IF EXISTS $TABLE_IDEAS3")
        //La volvemos a crear
        onCreate(p0)
    }

    fun generateRandom(): String {
        val chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var passWord = ""
        for (i in 0..15) {
            passWord += chars[Math.floor(Math.random() * chars.length).toInt()]
        }
        return passWord
    }

    override fun getWritableDatabase(): SQLiteDatabase {
        return super.getWritableDatabase()
    }

    /**
     * Se encarga de obtener el ID incremental para hacer consultas
     *
     * @param  aleatorio (Id temporal para acceder a su id autincremental) ,tabla
     * @return ID del elemento
     * @author K3V1NS4N
     *
     */

    fun getId (aleatorio: String, tabla: String): Int? {

        val baseDatos = this.writableDatabase

        val peticion = "SELECT $COLUMN_ID FROM $tabla " +
                "WHERE $COLUMN_ALEATORIO = \"$aleatorio\""
        val cursor = baseDatos.rawQuery(peticion, null)

        var id = 0
        if (cursor.moveToFirst()) {
            cursor.moveToFirst()
             id = Integer.parseInt(cursor.getString(0))
            cursor.close()
        }
        return id

    }

    /**
     *
     * Se encarga de guardar una idea en la base de datos pasandole un objeto idea
     */
    fun addIdea(idea: Idea) {

        val baseDatos = this.writableDatabase
        val registro = ContentValues()
        val registro2 = ContentValues()
        val registro3 = ContentValues()

        //Generamos un aleatorio para poder consultar la ID autoincremental mas tarde
        val aleatorioIdea = generateRandom()

        //Salvamos los datos procedentes del objeto idea creada
        registro.put(COLUMN_TITTLE, idea.tittle)
        registro.put(COLUMN_ALEATORIO, aleatorioIdea)
        registro.put(COLUMN_SHORTDESCRIPTION, idea.shortDescription)
        registro.put(COLUMN_DESCRIPTION, idea.description)

        //Añadimos la idea en su tabla correspondiente
        baseDatos.insert(TABLE_IDEAS, null, registro)

        //Consultamos su ID autoicnremental para poder usarla para referenciar a sus funcionalidades
        val ideaID = getId(aleatorioIdea,TABLE_IDEAS)

        //Obtenemos y recorremos todas las funcionalidades de la idea
        val funcionalidades = idea.funcionalidades
        if (funcionalidades != null) {
            for (funcionalidad in funcionalidades) {

                //Obtenemos sus funcionalidades secundarias
                val funcionalidadesmenores = funcionalidad.f_menores

                //De la misma manera que con la idea enterior, generamos un aleatorio para referenciarlo posteriormente
                val aleatorioIdea = generateRandom()

                //Almacenamos la funcionalidad asignadole la ID de la idea
                registro2.put(COLUMN_IDEA_ID, ideaID)
                registro2.put(COLUMN_ALEATORIO, aleatorioIdea)
                registro2.put(COLUMN_NAME, funcionalidad.nombre)
                registro2.put(COLUMN_ICON, funcionalidad.Icon)
                baseDatos.insert(TABLE_IDEAS2, null, registro2)

                //Obtenemos la idea
                val funcionalidadID = getId(aleatorioIdea,TABLE_IDEAS2)
                for (funcionalidadMenor in funcionalidadesmenores) {
                    //Recorremos las funcionaldiades secundarias para poder obtenerlas
                    registro3.put(COLUMN_FUNCIONALIDAD_ID, funcionalidadID)
                    registro3.put(COLUMN_ICON2, funcionalidadMenor)
                    baseDatos.insert(TABLE_IDEAS3, null, registro3)
                }
            }
        }
        baseDatos.close()
    }



    fun getIdeas (){

        val baseDatos = this.writableDatabase

        val peticion = "SELECT * FROM $TABLE_IDEAS"

        val cursor = baseDatos.rawQuery(peticion, null)

        val ideas = Ideas.ideasSQL
        //Borramos las ideas existentes para evitar que se repitan las ideas
        ideas?.clear()
        val contador = 0
        //Comprobamos si hay resultados
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                val idea = Idea()
                val id = Integer.parseInt(cursor.getString(0))
                val tittle = cursor.getString(2)
                val shortDescription = cursor.getString(3)
                val description = cursor.getString(4)


                idea.tittle = tittle
                idea.description = description
                idea.shortDescription = shortDescription

                val peticion2 = "SELECT * FROM $TABLE_IDEAS2 " +
                        "WHERE $COLUMN_IDEA_ID = \"$id\""


                val cursor2 = baseDatos.rawQuery(peticion2, null)

                if (cursor2.moveToFirst()) {
                    while (!cursor2.isAfterLast()) {

                        val funcionalidad = FuncionalidadSubida()


                        val id2 = Integer.parseInt(cursor2.getString(0))
                        funcionalidad.nombre = cursor2.getString(3)
                        funcionalidad.Icon = cursor2.getString(4)

                        val peticion3 = "SELECT * FROM $TABLE_IDEAS3 " +
                                "WHERE $COLUMN_IDEA_ID = \"$id2\""


                        val cursor3 = baseDatos.rawQuery(peticion3, null)

                        if (cursor3.moveToFirst()) {
                            while (!cursor3.isAfterLast()) {

                                funcionalidad.f_menores.add(cursor3.getString(2))
                                cursor3.moveToNext()
                            }
                        }

                        idea.funcionalidades?.add(funcionalidad)
                        cursor2.moveToNext()
                    }
                }
                ideas?.add(idea)
                cursor.moveToNext()
            }
        }

    }

/*
    fun deleteProduct(productName: String): Boolean {
        val baseDatos = this.writableDatabase
        val peticion = "SELECT * FROM $TABLE_PRODUCTS " +
                "WHERE $COLUMN_PRODUCTNAME = \"$productName\""
        var resultado = false

        val cursor = baseDatos.rawQuery(peticion, null)

        if (cursor.moveToFirst()) {
            cursor.moveToFirst()
            val id = Integer.parseInt(cursor.getString(0))
            //Eliminamos el registro en la BDD
            baseDatos.delete(TABLE_PRODUCTS,
                "$COLUMN_ID = $id",
                null)
            cursor.close()
            resultado = true
        }

        baseDatos.close()
        return resultado
    }
    */

}