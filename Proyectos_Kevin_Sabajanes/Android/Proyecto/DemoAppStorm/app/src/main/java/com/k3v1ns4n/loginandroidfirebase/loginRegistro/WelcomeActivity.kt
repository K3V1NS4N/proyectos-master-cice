package com.k3v1ns4n.loginandroidfirebase.loginRegistro

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.k3v1ns4n.loginandroidfirebase.BD
import com.k3v1ns4n.loginandroidfirebase.Objetos.BDINSTANCE
import com.k3v1ns4n.loginandroidfirebase.HomeActivity
import com.k3v1ns4n.loginandroidfirebase.Objetos.CurrentUser
import com.k3v1ns4n.loginandroidfirebase.R
import kotlinx.android.synthetic.main.welcome_layout.*

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class WelcomeActivity: AppCompatActivity() {


    // Se inicializa la variable de autenticacion de firebase
    private val auth = FirebaseAuth.getInstance()
    val bd = BD()
    var start = false


    //Instancia de firebase necesaria para subir y descargar las fotos
    var taskListener: ValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {

            Log.i("ERROR", "ERROR $p0")
        }


        override fun onDataChange(snapshot: DataSnapshot) {
            //Limita que se cargue los mensajes en caso de que haya alguien logeado
            if(!start){

                var intento = 0

                /**
                 * Muestra un mensaje del servidor en caso de que querer informar al usuario de algo
                 */
                Thread {
                    val bdisntance = BDINSTANCE
                    while(intento!=30) {

                        Thread.sleep(550)

                        //Obtiene el mensaje de la BD
                        bd.loadMensaje(snapshot.child("zmensaje"))

                        Thread.sleep(100)
                        //Si el mensaje no es null o esta vacio
                        if(bdisntance.mensaje != null && bdisntance.mensaje != "" && !start){
                            intento = 30
                            val mensaje = bdisntance.mensaje
                            //Mostramos el mensaje proveniente del servidor
                            this@WelcomeActivity.runOnUiThread(java.lang.Runnable {
                                login.isEnabled = false
                                register.isEnabled = false
                                mensaje(mensaje, "Alerta", true,false, "SALIR")
                            })

                        }else if(bdisntance.mensaje == "" && !start) {
                            intento = 30

                            //En caso de no recibir ningun mensaje del servidor se mostrará aquel que etsa por defecto
                            this@WelcomeActivity.runOnUiThread(java.lang.Runnable {
                                mensaje(
                                    "Esta aplicación es una DEMO que puede ser inestable, se le logeará con un usuario ficticio y no se almacenará ningun dato que intente insertar.",
                                    "Alerta",
                                    true,true,
                                    "Acepto"
                                )
                            })

                        }else{

                            intento++
                        }
                    }

                }.start()

            }
        }
    }

    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcome_layout)

        //Instanciamos la variable de Firebase
        database = FirebaseDatabase.getInstance().reference
        database.orderByKey().addListenerForSingleValueEvent(taskListener)
        val bdInstance = BDINSTANCE
        bdInstance.database = database

        infoauthor.setOnClickListener {

            mensaje("Developed and designed by Kevin Sabajanes © Copyright Todos los derechos reservados." +
                    "\n\nPuede escribirme a este correo en caso de querer comunicarme algo: kevinsan95@gmail.com","DEMO APPSTORM © 2019",false,true, "OK")

        }
    }

    fun registro(view: View){
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)

    }

    fun login(view: View){

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)

    }

    // Se ejecuta al inicio para comprobar si hay un usuario logeado
    public override fun onStart() {
        super.onStart()

        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth?.currentUser
        if(currentUser!= null){
            start=true
            updateUI(currentUser)
        }
    }

    //Accede a la pantalla de acceso
    fun updateUI(usuario: FirebaseUser?){

        val user = CurrentUser
        user.UID = usuario?.uid
        user.username = usuario?.displayName

        Toast.makeText(this, "logeado", Toast.LENGTH_SHORT).show()
        // Comprobar si tenemos un usuario
        usuario?.email
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        //Se finaliza la actividad para no poder volver hacia atras
        finish()

    }



    fun mensaje(mensaje: String , titulo: String, cancelar: Boolean,aceptar: Boolean, boton: String){

        // Inicializa una nueva instancia
        val builder = AlertDialog.Builder(this@WelcomeActivity)

        // Titulo de la alerta
        builder.setTitle(titulo)

        // Mensaje de la alerta
        builder.setMessage(mensaje)
        if (aceptar) {
            // Añade un boton positivo
            builder.setPositiveButton(boton) { _, _ ->

            }
        }
        if (cancelar){
            // Añade un boton neutral
            builder.setNeutralButton("Salir"){_,_ ->
                finish()
            }
        }

        // Finalmente crea la alerta
        val dialog: AlertDialog = builder.create()

        // Muestra la alerta
        dialog.show()
    }
}