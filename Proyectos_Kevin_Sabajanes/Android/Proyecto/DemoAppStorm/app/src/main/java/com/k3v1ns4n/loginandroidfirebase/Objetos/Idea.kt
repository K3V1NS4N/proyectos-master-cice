package com.k3v1ns4n.loginandroidfirebase.Objetos

class Idea {

    var key: String? = null
    var author: String? = null
    var comments: Long? = null
    var likes: Long? = null
    var shortDescription: String? = null
    var tittle: String? = null
    var description: String? = null
    var funcionalidades: ArrayList<FuncionalidadSubida>? = ArrayList()
    var liked: Boolean = false
    var comentarios: ArrayList<Comentario>? = ArrayList()
}
