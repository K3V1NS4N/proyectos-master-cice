package com.k3v1ns4n.loginandroidfirebase.AñadirFuncionalidad

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

// Es la lista de las funcionalidades principales a elegir que aparecen al principio, a si como el nombre vinculado a ellas.
enum class MainFuncionalititesIcons(val nombre: String) {

    analytics_svg("Analytics"), camera_svg("Camera"), childprotection_svg("Family"), contact_svg("Contact"), create_svg("Create"), date_svg("Date"), drawing_svg("Drawing"), event_svg("Event"), exercise_svg("Exercise"), gallery_svg("Gallery"), game_svg("Game"), learning_svg("Learning"), map_svg("Map"), money_svg("Money"), multimedia_svg("Multimedia"), music_svg("Music"), news_svg("News"), notification_svg("Alert"), payment_svg("Payment"), presentation_svg("Keynote"), print_svg("Print"), searcher_svg("Searcher"), shop_svg("Shop"), social_svg("Social"), sport_svg("Sport"), translation_svg("Translation"), trips_svg("Trip"), ic_add("Add")
}