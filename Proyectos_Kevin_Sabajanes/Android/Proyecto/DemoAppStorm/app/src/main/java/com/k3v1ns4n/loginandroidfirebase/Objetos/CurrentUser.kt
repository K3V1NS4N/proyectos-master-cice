package com.k3v1ns4n.loginandroidfirebase.Objetos

import java.util.HashMap

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */
//Informacion relativa al usuario
object CurrentUser {

    var username: String? = ""
    var email: String? = ""
    var UID: String? = null
    var likes: HashMap<String, Boolean>? = HashMap()
    var IDlikesFotos: ArrayList<String> = ArrayList()
    var liked: ArrayList<Boolean> = ArrayList()

}