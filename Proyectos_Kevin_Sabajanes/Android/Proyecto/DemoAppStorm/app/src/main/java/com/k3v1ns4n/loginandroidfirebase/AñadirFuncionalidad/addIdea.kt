package com.k3v1ns4n.loginandroidfirebase.AñadirFuncionalidad

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout

import kotlinx.android.synthetic.main.android_main_funcionality.view.*
import kotlinx.android.synthetic.main.content_add_idea.*
import android.graphics.Color
import android.util.TypedValue
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.k3v1ns4n.loginandroidfirebase.*
import kotlinx.android.synthetic.main.android_main_funcionality.view.icon
import kotlinx.android.synthetic.main.minor_funcionality.view.*
import android.app.Activity
import android.app.AlertDialog
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.EditText
import com.k3v1ns4n.loginandroidfirebase.Objetos.*


/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class addIdea : AppCompatActivity() {

    companion object {
        val respuesta = null
    }
    // Se inicializa la variable de autenticacion de firebase
    private val auth = FirebaseAuth.getInstance()
    val bd = BD()

    //Se declara la referencia a la base de datos
    private lateinit var FirebaseBD: DatabaseReference // BD FIREBASE

    /**[1] & [2]**/
    //Instancia de la clase BD necesaria para obtener y subir vinos
    lateinit var BD_vinos: BD

    /**[1] & [2]**/
    //Instancia de firebase necesaria para subir y descargar las fotos
    val storage = FirebaseStorage.getInstance()

    //Son los iconos de las funcionalidades principales
    val iconos = MainFuncionalititesIcons.values()
    //Son los iconos de las funcionalidades menores
    val iconos2 = MainFuncionalititesIcons.values()
    //Contador del bucle para a crear las funcionalidades menores
    var contador2 = 0
    //Cuenta el numero de funcionalidades menores seleccionadas respecto a la funcionalidad principal
    var contadorFuncionalidades = 0
    //Guardara los elementos visuales menores
    var arrayFuncionalidadesMayores = ArrayList<View>()
    //Guardara los elementos visuales menores
    var arrayElementosMenores = ArrayList<ArrayList<View>>()
    //Gestiona si se ha llegado a añadir una funcionaldiad menor
    var funcionalidadAñadida = false
    //Declara el arrayElementosMenores de objectos funcionalidad
    val funcionalidades = Funcionalidades
    //Corresponde a la posición de la funcionalidad seleccionada
    var elementSelected = 0
    //Almacena el elemento funcionalidad elegido por ultima vez
    var lastFuncionalityChoosed: Funcionalidad? = null
    //Se almacena los nombres de las funcionalidades principales
    var mainFuncionalityNames = ArrayList<String>()

    var escogido: Boolean = false
    //Contador del bucle principal
    var contador = 0
    var contador3 = 0

    //Contador del bucle principal
    var seleccionado = 0

    var añadido:Boolean = false

    //Son los arrays encargados de almacenar los iconos y las vistas de los circulos mini
    var secondFuncionality: ArrayList<String> = ArrayList()
    var secondFuncionalityView: ArrayList<View> = ArrayList()

    //Se trata del numero de localizacion de cada circulo grande
    var idMain = 0
    //Se trata del numero de localizacion de cada circulo pequeño
    var idSecondary = 0

    //Es la varriable encargada de gestionar si se tiene que mostrar una alerta de que no se han guarado los cambios.
    var mensajeGuardado: Boolean = false

    //-----[Variables de subida]-----
    //Almacena todas las funcionalidades
    var funcionalidadesSubida = FuncionalidadesSubida().funcionalidades


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        setContentView(R.layout.activity_add_idea)

        /**BD firebase**/
        FirebaseBD = FirebaseDatabase.getInstance().reference //Se obtiene la referencia

        /**BD**/
        BD_vinos = BD()

        //Reiniciamos los colores a los recuadros
        shortDescription.background.colorFilter = null
        description.background.colorFilter = null

        //Limpiamos el arrayElementosMenores de funcionalidades
        Funcionalidades.funcionalidades?.clear()

        /**
         * Bucle principal encargado de generar todas las vistas circulares a si como donde se van a almacenar
         * **/
        Thread {

            for (i in 1..4) {
                //Crea los circulos de las funcionalidades principales 4X5
                for (x in contador..contador + 4) {
                    contador++
                    //Se llama al metodo encargado de crear los elementos Principales
                    configurarElementosPrincipales(i, x, contador)
                    //Por cada circulo que se crea se añaden sus funcionalidades menores ( Circulos mini )
                    arrayElementosMenores.add(secondFuncionalityView)
                    //Borramos el array que se encarga de almacenar las vistas menores de forma temporal
                    secondFuncionality.clear()

                }
                //Crea los circulos de las funcionalidades principales (4X3)X6
                for (y in 1..3) {
                    for (x in contador2..contador2 + 5) {
                        //Se llama al metodo encargado de crear los elementos secundarios
                        configurarElementosSecundarios(i, y, x)
                        contador2++
                    }
                }
                contador2 = 0
            }
            //Mostramos las funcionalidades al cargarse
            runOnUiThread {
                loader.visibility = View.GONE
                linear1.visibility = View.VISIBLE
                linear2.visibility = View.VISIBLE
                linear3.visibility = View.VISIBLE
                linear4.visibility = View.VISIBLE
            }
        }.start()
        /**
         * Se encarga de subir las ideas al servidor
         * La idea puede tener multiples Funcionalidades y cada funcionalidad a su vez puede tener diferentes funcionalidades.
         * **/
        upload.setOnClickListener {

            // Se llama al metodo encargado de hacer las comprobacioes pertinentes
            if(comprobacion()){

                val idea = prepararIdea(false)

                //Creamos un hilo para subir la idea a la base de datos
                Thread {
                    //Comprueba que se haya añadido al menos una funcionalidad
                    if (idea.funcionalidades?.size != 0){
                        //Se establece la respuesta de la base de datos a null
                        BDINSTANCE.respuesta = null
                        //Se llama al metodo de la clase BD encargada de realizar la subida de la idea
                        BD_vinos.guardarIdea(FirebaseBD, idea)

                        /**
                         * Al mandar la petición de subida se empiezan a hacer verificaciones cada 0.1 segundos
                         * para comprobar si se ha recibido una respuesta por parte del servidor
                         * **/
                        var contador = 0
                        while (BDINSTANCE.respuesta == null){
                            contador++
                            if (contador == 30){
                                break
                            }
                            Thread.sleep(100)
                        }

                        //Si hemos recibido una respuesta y dicha respuesta es positiva
                        //se procera a informar al usuario de que la subida ha sido correcta
                        if(BDINSTANCE.respuesta != null && BDINSTANCE.respuesta!!){
                            runOnUiThread {
                                mensajeGuardado = false
                                //Snackbar(view)
                                val snackbar = Snackbar.make(it, "Se ha subido su idea con éxito",
                                    Snackbar.LENGTH_LONG).setAction("Action", null)

                                snackbar.show()
                                Handler().postDelayed({
                                    finish()
                                }, 1000)

                            }

                            //En caso contrario, informaremos igual
                        }else{
                            runOnUiThread {
                                //Snackbar(view)
                                val snackbar = Snackbar.make(it, "Ha ocurrido un error pero se procedera a subir la idea automaticamente cuando sea posible.",
                                    Snackbar.LENGTH_LONG).setAction("Action", null)
                                snackbar.show()
                                finish()
                            }
                        }
                        //Si no se ha seleccionado ninguan funcionaldiad
                    }else{
                        runOnUiThread {
                            dialog("Error","Tienes que seleccionar al menos una funcionalidad")
                        }

                    }
                }.start()
            }
        }

        //Guarda la idea en SQL
        saveSQL.setOnClickListener {

            if(comprobacion()) {

                val idea = prepararIdea(true)
                if (idea.funcionalidades?.size != 0) {
                    val dbHandler = MiDBHandler(this, null, null, 1)
                    dbHandler.addIdea(idea)
                    //Snackbar(view)
                    val snackbar = Snackbar.make(
                        it, "Se ha guardado su idea con éxito",
                        Snackbar.LENGTH_LONG
                    ).setAction("Action", null)

                    snackbar.show()
                    Handler().postDelayed({
                        finish()
                    }, 1000)
                }else{
                    runOnUiThread {
                        dialog("Error","Tienes que seleccionar al menos una funcionalidad")
                    }

                }

            }
        }
    }

    private fun prepararIdea(sql: Boolean):Idea{

        //Reiniciamos las variables
        var contador = 0
        var contador2 = 0
        val idea = Idea()

        //Se recorre el array que almacena las funcionalidades
        for (funcionalidad in Funcionalidades.funcionalidades!!){

            //En caso de que la funcionalidad actual haya sido añadida, se procedera a obtener la informacion
            if (funcionalidad.anadido){

                //Se obtiene la informacion de la funcionalidad principal añadida
                val funcionalidadSubida = FuncionalidadSubida()
                funcionalidadSubida.nombre = mainFuncionalityNames[contador]
                funcionalidadSubida.Icon = Funcionalidades.mainFuncionalities[contador]
                Log.i("SUBIDA", "Contador $contador")
                Log.i("SUBIDA", "AñadidoNombre ${funcionalidad.nombre} e" +
                        " ${Funcionalidades.mainFuncionalities[contador]} ")

                //Se recorren sus funcionaliades menores
                for (funcionalidadMenor in funcionalidad.ChoosedFuncionalities ){

                    if (funcionalidadMenor){

                        Log.i("SUBIDA", "FuncionalidadMenor ${funcionalidad.secondFuncionalities[contador2]} ")
                        funcionalidadSubida.f_menores.add(funcionalidad.secondFuncionalities[contador2])
                    }
                    contador2++
                }
                //Se añaden las funcionalidades a la idea
                idea.funcionalidades?.add(funcionalidadSubida)
            }
            contador2=0
            contador++
        }

        //Se establece la información extra relativa a la idea que se va a almacenar
        idea.comments = 0
        idea.author = "Kevin"
        idea.likes = 0

        //Si se quiere almacenar en el dispositivo
        if (sql){
            idea.shortDescription = shortDescription.text.toString()
            idea.description = description.text.toString()

        }else{ //En caso de querer guardarlo en el servidor, se almacenara texto de prueba por temas de licencias.

            idea.description = "Nulla ullamcorper accumsan tortor quis efficitur." +
                    " Pellentesque est turpis, auctor sit amet convallis ut, pellentesque commodo lectus." +
                    " Pellentesque pulvinar placerat nulla, ut maximus ipsum finibus vehicula." +
                    " Integer rutrum sodales augue, in aliquet metus aliquet vitae. In velit sem, tincidunt nec lectus a, dignissim iaculis libero."

            idea.shortDescription = "Cras tincidunt eu mi eu condimentum. In at nisl risus. Porta in lectus et, pulvinar finibus tellus"
        }

        idea.tittle = titulo.text.toString()
        return idea
    }

    /**
     * Se encarga de hacer las comprobaciones de los campos de texto de la idea.
     * **/
    fun comprobacion():Boolean{

        val short = shortDescription.text.toString()
        val tittle = titulo.text.toString()
        val long = description.text.toString()

        //Comprueba si los recuadeos de texto tienen algo
        if(tittle.trim().length == 0) {
            titulo.requestFocus()
            return false
        }else if(short.trim().length == 0){
            shortDescription.requestFocus()
            shortDescription.background.setColorFilter(ContextCompat.getColor(this, R.color.red),
                android.graphics.PorterDuff.Mode.SRC_IN)
            return false
        }else if(long.trim().length == 0){
            description.requestFocus()
            description.background.setColorFilter(ContextCompat.getColor(this, R.color.red),
                android.graphics.PorterDuff.Mode.SRC_IN)
            return false

        }else{
            return true
        }
    }

    //Muestra una dialogo
    fun dialog(titulo: String, Mensaje: String){

        // Inicializa una nueva instancia
        val builder = AlertDialog.Builder(this@addIdea)

        // Titulo de la alerta
        builder.setTitle(titulo)

        // Mensaje de la alerta
        builder.setMessage(Mensaje)

        // Añade un boton positivo
        builder.setPositiveButton("OK"){_, _ ->

        }
        // Finalmente crea la alerta
        val dialog: AlertDialog = builder.create()

        // Muestra la alerta
        dialog.show()

    }

    //Flechita para ir hacia atras de la toolbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    //Metodo encargado de gestionar lo que hace el boton atras fisico.
    override fun onBackPressed() {
        if(mensajeGuardado){

            val returnIntent = Intent()
            returnIntent.putExtra("hasBackPressed", true)
            setResult(Activity.RESULT_OK, returnIntent)

            // Inicializa una nueva instancia
            val builder = AlertDialog.Builder(this@addIdea)

            // Titulo de la alerta
            builder.setTitle("Alerta")

            // Mensaje de la alerta
            builder.setMessage("¿Seguro que quiere salir sin guardar la idea? Se borraran los cambios realizados.")

            // Añade un boton positivo
            builder.setPositiveButton("Salir sin guardar"){_, _ ->
                // Do something when user press the positive button
                finish()

            }
            // Añade un boton neutral
            builder.setNeutralButton("Cancelar"){_,_ ->

            }
            // Finalmente crea la alerta
            val dialog: AlertDialog = builder.create()

            // Muestra la alerta
            dialog.show()

        }else{
            finish()
        }
    }

    /**
     * Se llama a este metodo cuando se vuelve de elegir un icono de la lista de iconos,
     * dependiendo del elemento seleccionado se devolvera un resultado u otro.
     * TODO HAY COSAS MEJORABLES :=)
     * **/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {



        //Se quiso cambiar el icono de una funcionalidad principal
        if (requestCode == 1 && data != null && resultCode == RESULT_OK) {

            val drawable = data.getStringExtra("Name")
            val num = data.getIntExtra("Name2", 0)



            //Creamos el recurso del icono guardado en un enum de iconos para las funcionalidades
            val ressourceId = this.resources.getIdentifier(
                drawable,
                "drawable",
                Package.instance
            )

            Funcionalidades.funcionalidades?.get(num-1)?.vista?.icon?.setBackgroundResource(ressourceId)
            Funcionalidades.mainFuncionalities[num-1] = drawable

            showCreateCategoryDialog(num,Funcionalidades.funcionalidades?.get(num-1))

        //Se quiso cambiar el icono de una funcionalidad secundaria
        }else if (requestCode == 2 && data != null && resultCode == RESULT_OK) {
            //Es el nombre del icono seleccionado
            val drawable = data.getStringExtra("Name")
            //Es el numero  que determina la funcionalidad principal a la que vamos a modificar
            val num = data.getIntExtra("Name2", 0)
            val seleccionado = data.getIntExtra("seleccionado", 0)
            //Es el numero de la funcionalidad la que vamos a modificar su icono
            val mainID = data.getIntExtra("Name3", 0)

            //Creamos el recurso del icono guardado en un enum de iconos para las funcionalidades
            val ressourceId = this.resources.getIdentifier(
                drawable,
                "drawable",
                Package.instance
            )
            //Obtiene la referencia al recurso que muestra la imagen del icono del ciruclo pequeño de una funcionalidad concreta
            // EJ:________________________________________
            // -Funcionalidad Principal (Map)  num
            //      -Funcionalidad Secundaria (Alerta) mainID

            val icono = arrayElementosMenores[num-1][mainID+(18*seleccionado)].icon

            //Se salva el icono para la funcionalidad secundaria correspondiente
            Funcionalidades.funcionalidades?.get(num-1)?.secondFuncionalities?.set(mainID, drawable)
            //Se elimina el anterior
            icono.setBackgroundResource(0)
            //Se añade el nuevo
            icono.setBackgroundResource(ressourceId)

            //Obtenemos las dimensiones del icono
            val iW=icono.background.intrinsicWidth
            val ih=icono.background.intrinsicHeight
            //La hacemos mas pequeña respetando la proporcion de cada icono (que son diferentes entre si)
            val dimensionInDp =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (iW/5).toFloat(), resources.displayMetrics)
                    .toInt()

            val dimensionInDp2 =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (ih/5).toFloat(), resources.displayMetrics)
                    .toInt()
            //Eestablecemos las modificaciones
            icono.icon.layoutParams.height = dimensionInDp2
            icono.icon.layoutParams.width = dimensionInDp
            icono.icon.requestLayout()

        }else if (resultCode == RESULT_CANCELED) {

            //Error
        }
    }

        //Funcionalidades principales
        fun configurarElementosPrincipales(num1: Int, num2: Int, num3: Int){

            mainFuncionalityNames.add(iconos[num3].nombre)

            var ressourceId:Int

            if (!escogido){

                Funcionalidades.mainFuncionalities.add(iconos[num2].name)
                //Creamos el recurso del icono guardado en un enum de iconos para las funcionalidades
                 ressourceId = this.resources.getIdentifier(
                    iconos[num2].name,
                    "drawable",
                     Package.instance
                )
            }else {

                //Creamos el recurso del icono guardado en un enum de iconos para las funcionalidades
                 ressourceId = this.resources.getIdentifier(
                     Funcionalidades.mainFuncionalities[num2],
                    "drawable",
                     Package.instance
                )

            }

            //Creamos la vista que mostrara el circulo y texto base a una interfaz XML anteriormente diseañada
            val vista = LayoutInflater.from(this).inflate(R.layout.android_main_funcionality, null)

            //Declaramos el objeto funcionalidad encargado de almacenar la información
            val funcionalidad = Funcionalidad()
            funcionalidad.nombre = iconos[num2].nombre
            funcionalidad.vista = vista


            //Añadimos el padding necesario
            vista.setPadding(15, 0, 15, 0)
            runOnUiThread {
                //Añadimos el icono correspondiente
                vista.icon.setBackgroundResource(ressourceId)
            }

            //Añadimos el texto correspondiente
            vista.iconName.text = iconos[num2].nombre


            /**
             * Dependiendo de la fila de elementos seleccionada se accedera a un linear layout u otro
             * TODO Se puede mejorar con un bucle extra para no repetir codigo :=)
             * **/
            //Dependiendo de la fila de elementos seleccionada se accedera a un linear layout u otro (Mejorable)---*
            when (num1) {
                //Añadimos un listener al presionar la vista de la funcionalidad principal
                1 ->{
                    funcionalidad.linea = 0

                    vista.setOnLongClickListener {
                        mensajeGuardado = true
                        idMain = num3
                        abrirListaIconos(1)

                        true
                    }
                    vista.setOnClickListener {
                        idMain = num3
                        seleccionado = 0
                        //Salvamos en esta variable el numero correspondiente al elemento pulsado para saber a que elemento acceder despues
                        elementSelected = num2

                        //Permite gestionar si una funcionalidad se ha activado (Al pulsarla y desplegar la ventanita con ciruclitos)
                        if (!funcionalidadAñadida) {
                            //Se le elimina el filtro de color añadido
                            Funcionalidades.funcionalidades?.get(contador - 1)?.vista?.circle?.setColorFilter(null)
                        } else {

                            funcionalidadAñadida = false
                        }
                        //Añadimos el filtro de color para resaltarlo
                        vista.circle.setColorFilter((Color.argb(70, 0, 0, 0)))
                        //Salvamos en el arrayElementosMenores de funcionalidades la vista
                        Funcionalidades.funcionalidades?.get(contador - 1)?.vista = vista
                        //Salvamos dicha vista para poder acceder posteriormente
                        lastFuncionalityChoosed = Funcionalidades.funcionalidades?.get(contador - 1)

                        //Mostramos los lienar layout con los elementos
                        linear11.visibility = View.VISIBLE
                        linear12.visibility = View.VISIBLE
                        linear13.visibility = View.VISIBLE

                        var contadorTemporal = 0
                        //Se comprueba las funcionalidades menores que se han elegido
                        val elegidos = Funcionalidades.funcionalidades?.get(num2)?.ChoosedFuncionalities
                        if (elegidos != null) {
                            for( elegido in elegidos){
                                Log.i("LOG_TAG", contadorTemporal.toString())
                                Log.i("LOG_TAG", elegido.toString())
                                if (elegido){

                                    arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter((Color.argb(255, 107, 190, 255)))
                                }else{
                                    arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter((Color.argb(255, 144, 144, 144)))
                                }
                                contadorTemporal++
                            }
                        }
                        runOnUiThread {
                            //Ocultamos los demas que pudieran estar abiertos (Mejorable) ---*
                            linear21.visibility = View.GONE
                            linear22.visibility = View.GONE
                            linear23.visibility = View.GONE

                            linear31.visibility = View.GONE
                            linear32.visibility = View.GONE
                            linear33.visibility = View.GONE

                            linear41.visibility = View.GONE
                            linear42.visibility = View.GONE
                            linear43.visibility = View.GONE
                        }

                    }
                }
                2 ->{
                    funcionalidad.linea = 1

                    vista.setOnLongClickListener {
                        idMain = num3
                        mensajeGuardado = true
                        abrirListaIconos(1)


                        true
                    }
                     vista.setOnClickListener {
                         idMain = num3
                         seleccionado = 1
                         elementSelected = num2
                         if (!funcionalidadAñadida) {

                             Funcionalidades.funcionalidades?.get(contador - 1)?.vista?.circle?.setColorFilter(null)
                         } else {

                             funcionalidadAñadida = false
                         }

                         vista.circle.setColorFilter((Color.argb(70, 0, 0, 0)))
                         Funcionalidades.funcionalidades?.get(contador - 1)?.vista = vista
                         lastFuncionalityChoosed = Funcionalidades.funcionalidades?.get(contador - 1)

                         linear21.visibility = View.VISIBLE
                         linear22.visibility = View.VISIBLE
                         linear23.visibility = View.VISIBLE


                         var contadorTemporal = 18
                         //Se comprueba las funcionalidades menores que se han elegido
                         val a = Funcionalidades.funcionalidades?.get(num2)?.ChoosedFuncionalities

                         if (a != null) {
                             for (elegido in a) {
                                 Log.i("LOG_TAG", contadorTemporal.toString())
                                 Log.i("LOG_TAG", elegido.toString())
                                 if (elegido) {

                                     arrayElementosMenores[num1][contadorTemporal].littleCircle.setColorFilter(
                                         (Color.argb(
                                             255,
                                             107,
                                             190,
                                             255
                                         ))
                                     )
                                 } else {

                                     arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter(
                                         (Color.argb(
                                             255,
                                             144,
                                             144,
                                             144
                                         ))
                                     )
                                 }
                                 contadorTemporal++
                             }
                         }

                         linear11.visibility = View.GONE
                         linear12.visibility = View.GONE
                         linear13.visibility = View.GONE

                         linear31.visibility = View.GONE
                         linear32.visibility = View.GONE
                         linear33.visibility = View.GONE

                         linear41.visibility = View.GONE
                         linear42.visibility = View.GONE
                         linear43.visibility = View.GONE
                     }
                }
                3 ->{
                    funcionalidad.linea = 2
                        vista.setOnLongClickListener {
                            idMain = num3
                            mensajeGuardado = true
                            abrirListaIconos(1)

                            true
                        }
                        vista.setOnClickListener {
                            idMain = num3
                            seleccionado = 2

                            elementSelected = num2

                            if (!funcionalidadAñadida) {

                                Funcionalidades.funcionalidades?.get(contador - 1)?.vista?.circle?.setColorFilter(null)
                            } else {

                                funcionalidadAñadida = false
                            }

                            vista.circle.setColorFilter((Color.argb(70, 0, 0, 0)))
                            Funcionalidades.funcionalidades?.get(contador - 1)?.vista = vista
                            lastFuncionalityChoosed = Funcionalidades.funcionalidades?.get(contador - 1)

                            linear31.visibility = View.VISIBLE
                            linear32.visibility = View.VISIBLE
                            linear33.visibility = View.VISIBLE

                            var contadorTemporal = 36
                            //Se comprueba las funcionalidades menores que se han elegido
                            val a = Funcionalidades.funcionalidades?.get(num2)?.ChoosedFuncionalities


                            if (a != null) {
                                for (elegido in a) {
                                    Log.i("LOG_TAG", contadorTemporal.toString())
                                    Log.i("LOG_TAG", elegido.toString())
                                    if (elegido) {

                                        arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter(
                                            (Color.argb(
                                                255,
                                                107,
                                                190,
                                                255
                                            ))
                                        )
                                    } else {

                                        arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter(
                                            (Color.argb(
                                                255,
                                                144,
                                                144,
                                                144
                                            ))
                                        )
                                    }
                                    contadorTemporal++
                                }
                            }

                            linear21.visibility = View.GONE
                            linear22.visibility = View.GONE
                            linear23.visibility = View.GONE

                            linear11.visibility = View.GONE
                            linear12.visibility = View.GONE
                            linear13.visibility = View.GONE

                            linear41.visibility = View.GONE
                            linear42.visibility = View.GONE
                            linear43.visibility = View.GONE

                        }
                    }
                4 ->{
                    funcionalidad.linea = 3
                        vista.setOnLongClickListener {
                            mensajeGuardado = true
                            idMain = num3
                            abrirListaIconos(1)

                            true
                        }
                        vista.setOnClickListener {
                            idMain = num3
                            seleccionado = 3

                            elementSelected = num2

                            if (!funcionalidadAñadida) {

                                Funcionalidades.funcionalidades?.get(contador - 1)?.vista?.circle?.setColorFilter(null)
                            } else {

                                funcionalidadAñadida = false
                            }

                            vista.circle.setColorFilter((Color.argb(70, 0, 0, 0)))
                            Funcionalidades.funcionalidades?.get(contador - 1)?.vista = vista
                            lastFuncionalityChoosed = Funcionalidades.funcionalidades?.get(contador - 1)

                            linear41.visibility = View.VISIBLE
                            linear42.visibility = View.VISIBLE
                            linear43.visibility = View.VISIBLE

                            var contadorTemporal = 54

                            val a = Funcionalidades.funcionalidades?.get(num2)?.ChoosedFuncionalities

                            if (a != null) {
                                for (elegido in a) {
                                    Log.i("LOG_TAG", contadorTemporal.toString())
                                    Log.i("LOG_TAG", elegido.toString())
                                    if (elegido) {

                                        arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter(
                                            (Color.argb(
                                                255,
                                                107,
                                                190,
                                                255
                                            ))
                                        )
                                    } else {

                                        arrayElementosMenores[num1-1][contadorTemporal].littleCircle.setColorFilter(
                                            (Color.argb(
                                                255,
                                                144,
                                                144,
                                                144
                                            ))
                                        )
                                    }
                                    contadorTemporal++
                                }
                            }

                            linear21.visibility = View.GONE
                            linear22.visibility = View.GONE
                            linear23.visibility = View.GONE

                            linear31.visibility = View.GONE
                            linear32.visibility = View.GONE
                            linear33.visibility = View.GONE

                            linear11.visibility = View.GONE
                            linear12.visibility = View.GONE
                            linear13.visibility = View.GONE

                        }
                    }

                else -> print("de otra forma.")
            }

            //Generara el identificador del TableRow de manera dinámica
            val rowID = resources.getIdentifier(
                "linear$num1",
                "id",
                this.getPackageName()
            )
            //Obtenemos el linear layout correspondiente
            val row = findViewById(rowID) as LinearLayout
            runOnUiThread {
                //Añadimos la vista al linear layout
                row.addView(vista)
            }
            //Añadimos la funcionalidad al al arrayElementosMenores de funcionalidades
            Funcionalidades.funcionalidades?.add(funcionalidad)

        }

        //Funcionalidades menores
        fun configurarElementosSecundarios(num: Int,num2: Int,num3: Int){


            val iconosSecundarios = SecondaryFuncionalititesIcons().iconos
            val rnds = (0 until iconosSecundarios.size).random()
            //Creamos el recurso del icono
            val ressourceId = this.resources.getIdentifier(
                iconosSecundarios[rnds],
                "drawable",
                Package.instance
            )


            secondFuncionality.add(iconosSecundarios[rnds])
            if (secondFuncionality.size == 18){
                for (i in 0..4){

                    Funcionalidades.funcionalidades?.get(contador3)?.secondFuncionalities!!.addAll(secondFuncionality)
                    contador3++
                }
                secondFuncionality.clear()
            }

            //Generara el identificador del TableRow de manera dinámica
            val rowID = resources.getIdentifier(
                "linear$num$num2",
                "id",
                this.getPackageName()
            )
            //Obtenemos el linear layout correspondiente
            val row = findViewById(rowID) as LinearLayout
            runOnUiThread {
                //Escondemos el linear layout
                row.visibility = View.GONE
            }
            //Creamos la vista que mostrara el mini circulo y texto base a una interfaz XML anteriormente diseañada
            val vista = LayoutInflater.from(this).inflate(R.layout.minor_funcionality,null)
            vista.setPadding(15,40,15,0)
            runOnUiThread {
                //Añadimos su icono correspondiente
                vista.icon.setBackgroundResource(ressourceId)
            }

            //ñadimos un listener al presionar la vista de la funcionalidad menor para añadirsela a la principal
            vista.setOnClickListener {
                mensajeGuardado = true
                //obtenemos una referencia a la funcionalidad pulsada anteriormente
                val funcionalidad = Funcionalidades.funcionalidades?.get(elementSelected)
                if (!funcionalidad!!.ChoosedFuncionalities[num3]){

                    // Mostramos el circulito turquesa encargado de contar el numero de funcionalidades seleccionadas
                    funcionalidad.vista?.circleCount?.visibility = View.VISIBLE
                    funcionalidad.vista?.count?.visibility = View.VISIBLE
                    //Obtenemos el numero de funcionalidades almacenadas en el objeto
                    contadorFuncionalidades = funcionalidad.numChoosedFuncionalities[elementSelected]
                    //Sumamos uno mas
                    contadorFuncionalidades++
                    //Salvamos el nuevo numero de funcionalidades menores seleccionadas
                    funcionalidad.numChoosedFuncionalities[elementSelected] = contadorFuncionalidades
                    //Mostramos el contador
                    funcionalidad.vista?.count?.text = contadorFuncionalidades.toString()

                    //Cambiamos su estado a true para guardar su seleccion
                    funcionalidad.ChoosedFuncionalities[num3] = true
                    funcionalidadAñadida = true
                    //Le añadimos un color a la funcionalidad seleccionada
                    vista.littleCircle.setColorFilter((Color.argb(255, 107, 190, 255)))

                    funcionalidad.anadido = true // Se gestiona que este elemento se va a tener que añadir


                }else{


                    // Mostramos el circulito turquesa encargado de contar el numero de funcionalidades seleccionadas
                    funcionalidad.vista?.circleCount?.visibility = View.VISIBLE
                    funcionalidad.vista?.count?.visibility = View.VISIBLE
                    //Obtenemos el numero de funcionalidades almacenadas en el objeto
                    contadorFuncionalidades = funcionalidad.numChoosedFuncionalities[elementSelected]
                    if (contadorFuncionalidades == 1){
                        //Le añadimos un color a la funcionalidad seleccionada
                        vista.littleCircle.setColorFilter((Color.argb(255, 144, 144, 144)))
                        funcionalidad.vista?.circleCount?.visibility = View.GONE
                        funcionalidad.vista?.count?.visibility = View.GONE
                        //restamos
                        contadorFuncionalidades--
                        //Salvamos el nuevo numero de funcionalidades menores seleccionadas
                        funcionalidad.numChoosedFuncionalities[elementSelected] = contadorFuncionalidades

                        //Cambiamos su estado a false para quitar su seleccion
                        funcionalidad.ChoosedFuncionalities[num3] = false
                        funcionalidad.anadido = false


                    }else{
                        vista.littleCircle.setColorFilter((Color.argb(255, 144, 144, 144)))
                        //restamos
                        contadorFuncionalidades--
                        //Salvamos el nuevo numero de funcionalidades menores seleccionadas
                        funcionalidad.numChoosedFuncionalities[elementSelected] = contadorFuncionalidades
                        //Mostramos el contador
                        funcionalidad.vista?.count?.text = contadorFuncionalidades.toString()

                        //Cambiamos su estado a false para quitar su seleccion
                        funcionalidad.ChoosedFuncionalities[num3] = false

                    }
                }
            }

            // Al hacer un click largo se abrira la lista de iconos encargada de elegir una nueva funcionalidad
            vista.setOnLongClickListener {
                mensajeGuardado = true
                idSecondary = num3
                abrirListaIconos(2)

                true
            }
            runOnUiThread {
                //Obtenemos las dimensiones del icono
                val iW = vista.icon.background.intrinsicWidth
                val ih = vista.icon.background.intrinsicHeight
                //La hacemos mas pequeña respetando la proporcion de cada icono (que son diferentes entre si)
                val dimensionInDp =
                    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (iW / 5).toFloat(), resources.displayMetrics)
                        .toInt()

                val dimensionInDp2 =
                    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (ih / 5).toFloat(), resources.displayMetrics)
                        .toInt()
                //Eestablecemos las modificaciones
                vista.icon.layoutParams.height = dimensionInDp2
                vista.icon.layoutParams.width = dimensionInDp
                vista.icon.requestLayout()
                //Añadimos la vista de la funcionalidad secundaria
                row.addView(vista)
            }
            secondFuncionalityView.add(vista)

        }

    fun abrirListaIconos( requestCode: Int){

            val intent = Intent(this, chooseFuncionality::class.java)
            //Le pasamos el numero que identifica a la celda pulsada
            intent.putExtra("MAIN_ID", idMain)
            intent.putExtra("SECONDARY_ID", idSecondary)
            //Comenzamos la actividad pasandole un código para separar las funcionalidades main de las secundarias
            intent.putExtra("requestCode", requestCode)
            //Enviamos la posicion de los linear layout donde fue seleccioanado
            intent.putExtra("seleccionado", seleccionado)
            startActivityForResult(intent,requestCode)
    }

    fun showCreateCategoryDialog(numMainIcon: Int, funcionalidad: Funcionalidad?) {

        val context = this
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Dale un nombre a la funcionalidad")

        // https://stackoverflow.com/questions/10695103/creating-custom-alertdialog-what-is-the-root-view
        // Seems ok to inflate view with null rootView
        val view = layoutInflater.inflate(R.layout.editext_dialog, null)

        val categoryEditText = view.findViewById(R.id.categoryEditText) as EditText

        builder.setView(view)

        // set up the ok button
        builder.setPositiveButton(android.R.string.ok) { dialog, p1 ->
            val newCategory = categoryEditText.text
            var isValid = true
            if (newCategory.isBlank()) {

                isValid = false
            }

            if (isValid) {
                mainFuncionalityNames[numMainIcon-1] = newCategory.toString()
                funcionalidad?.vista?.iconName?.text = newCategory.toString()
            }

            if (isValid) {
                dialog.dismiss()
            }
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
            dialog.cancel()
        }

        builder.show();
    }
}

