package com.k3v1ns4n.loginandroidfirebase.AñadirFuncionalidad

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.k3v1ns4n.loginandroidfirebase.Objetos.iconos
import com.k3v1ns4n.loginandroidfirebase.Package
import com.k3v1ns4n.loginandroidfirebase.R

import kotlinx.android.synthetic.main.activity_choose_funcionality.*
import kotlinx.android.synthetic.main.activity_visualizar_idea.*
import kotlinx.android.synthetic.main.content_visualizar_idea.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class chooseFuncionality : AppCompatActivity() {

    var array = MainFuncionalititesIcons.values()
    val customAdaptor = CustomAdapator(this)
    val iconosAnimatable2 = SecondaryFuncionalititesIcons().iconos
    val iconosBusqueda: ArrayList<String> = ArrayList()

    var copiaIconos: ArrayList<Int> = ArrayList()
    var copiaNombres: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_funcionality)

        //Informacion relativa al circulo seleccionado en la actividad anterior
        val sessionId = intent.getIntExtra("MAIN_ID",0)
        val sessionId2 = intent.getIntExtra("SECONDARY_ID",0)

        val seleccionado = intent.getIntExtra("seleccionado",0)

        val iconos = iconos

        //Comprueba si los iconos ya se han cargado
        if (iconos.iconos.size == 0){

            Thread(Runnable {
                /**
                 * Manera antigua de obtener los recursos, ahora se accede directamente al array que es mas rapido.
                 */
                //val drawablesFields = R.drawable::class.java!!.fields

                val drawables: ArrayList<Drawable> = ArrayList()
                var nombres: String = ""
                var tam = iconosAnimatable2.size

                progressBar.max = tam-300

                for (field in iconosAnimatable2) {
                    try {
                       //Log.i("LOG_TAG", "com.your.project.R.drawable." + field.name)

                        //Creamos el recurso del icono guardado en un enum de iconos para las funcionalidades
                        val ressourceId = this.resources.getIdentifier(
                            field,
                            "drawable",
                            Package.instance
                        )

                        /*
                        if (field.contains("svg", ignoreCase = true)){
                            //nombres += " \""+field.name +"\","
                            iconos.iconos.add(ressourceId)
                            iconos.nombres.add(field)
                            progressBar.progress = iconos.iconos.size
                        }
                        */
                        iconos.iconos.add(ressourceId)
                        iconos.nombres.add(field)
                        progressBar.progress = iconos.iconos.size

                        this@chooseFuncionality.runOnUiThread(java.lang.Runnable {

                            cuenta.text = "${tam--} Iconos restantes"
                        })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

                copiaIconos = iconos.iconos.clone() as ArrayList<Int>
                copiaNombres = iconos.nombres.clone() as ArrayList<String>
                //Log.i("LOG_TAG", "com.your.project.R.drawable." + nombres.toString())

                this@chooseFuncionality.runOnUiThread(java.lang.Runnable {
                    // Se actualizan los elementos de la lista al cargarse todos los iconos
                    cuenta.visibility = View.GONE
                    progressBar.visibility = View.GONE
                    val listView: ListView = findViewById(R.id.listaIconos)
                    val customAdptor = CustomAdapator(this)
                    listView.adapter=customAdptor

                    //Se envia el icono de la lista elegido a la actividad anterior
                    listView.setOnItemClickListener{ _, _, position, _ ->
                        //Toast.makeText(this, "You Clicked:"+" "+ iconos.nombres[position],Toast.LENGTH_SHORT).show()

                        val intent = Intent(this, addIdea::class.java)
                        intent.putExtra("Name", iconos.nombres[position])
                        intent.putExtra("Name2", sessionId)
                        intent.putExtra("Name3", sessionId2)
                        intent.putExtra("seleccionado", seleccionado)

                        setResult(RESULT_OK,intent)

                        com.k3v1ns4n.loginandroidfirebase.Objetos.iconos.iconos = copiaIconos.clone() as ArrayList<Int>
                        com.k3v1ns4n.loginandroidfirebase.Objetos.iconos.nombres = copiaNombres.clone() as ArrayList<String>
                        this.finish()
                    }

                })
            }).start()

        }else{

            cuenta.visibility = View.GONE
            progressBar.visibility = View.GONE

            val listView: ListView = findViewById(R.id.listaIconos)

            listView.adapter = customAdaptor

            listView.setOnItemClickListener{ parent, view, position, id ->
                //Toast.makeText(this, "You Clicked:"+" "+ iconos.nombres[position],Toast.LENGTH_SHORT).show()
                val intent = Intent(this, addIdea::class.java)
                intent.putExtra("Name", iconos.nombres[position])
                intent.putExtra("Name2", sessionId)
                intent.putExtra("Name3", sessionId2)
                setResult(RESULT_OK,intent)
                this.finish()
            }
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                iconos.iconos.clear()
                iconos.nombres.clear()

                if (query!!.isNotEmpty()) {
                    try {
                        for (icono in iconosBusqueda) {

                            //Creamos el recurso del icono guardado en un enum de iconos para las funcionalidades
                            val ressourceId = resources.getIdentifier(
                                icono,
                                "drawable",
                                Package.instance
                            )
                            iconos.iconos.add(ressourceId)
                            iconos.nombres.add(icono)
                        }
                    } catch (e: Exception) {


                    }
                }else{

                    com.k3v1ns4n.loginandroidfirebase.Objetos.iconos.iconos = copiaIconos.clone() as ArrayList<Int>
                    com.k3v1ns4n.loginandroidfirebase.Objetos.iconos.nombres = copiaNombres.clone() as ArrayList<String>
                }


                customAdaptor.notifyDataSetChanged()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                val text = newText
                /*Call filter Method Created in Custom Adapter
                    This Method Filter ListView According to Search Keyword
                 */

                if (text!!.isNotEmpty()){
                    iconosBusqueda.clear()
                    Thread(Runnable {
                        try {
                            for (icono in iconosAnimatable2) {

                                if (icono.contains("$text", ignoreCase = true)) {


                                    iconosBusqueda.add(icono)

                                }
                            }
                        }
                        catch (e: Exception) {


                        }

                    }).start()

                }

                return false
            }
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()

        iconos.iconos = copiaIconos.clone() as ArrayList<Int>
        iconos.nombres = copiaNombres.clone() as ArrayList<String>
    }




}





/**
 * Adaptador de la lista
 */

class CustomAdapator(private val context: Activity): BaseAdapter() {

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        Log.i("LOG_TAG4", "SE LLAMA")
        val inflater = context.layoutInflater
        val view1 = inflater.inflate(R.layout.listview_item,null)
        val fimage = view1.findViewById<ImageView>(R.id.fimageView)
        var fName = view1.findViewById<TextView>(R.id.fName)
        fimage.setImageResource(iconos.iconos[p0])
        fName.text = iconos.nombres[p0]
        return view1
    }

    override fun getItem(p0: Int): Any {
        return iconos.iconos[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return iconos.iconos.size
    }



}


