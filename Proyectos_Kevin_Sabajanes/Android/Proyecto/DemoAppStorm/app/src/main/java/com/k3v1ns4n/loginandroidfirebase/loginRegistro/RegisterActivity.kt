package com.k3v1ns4n.loginandroidfirebase.loginRegistro


import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.content.Intent
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.k3v1ns4n.loginandroidfirebase.BD
import com.k3v1ns4n.loginandroidfirebase.R

import kotlinx.android.synthetic.main.register_layout.*
import java.util.regex.Pattern

/**
 * A login screen that offers login via email/password.
 */
class RegisterActivity : AppCompatActivity() {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private var mAuth: FirebaseAuth? = null
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_layout)
        mensaje()
        database = FirebaseDatabase.getInstance().reference

        pass.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })
        email_sign_in_button.setOnClickListener { attemptLogin() }
        mAuth = FirebaseAuth.getInstance()

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {

        // Reset errors.
        user.error = null
        pass.error = null


        // Store values at the time of the login attempt.
        val emailStr = user.text.toString()
        val passwordStr = pass.text.toString()
        val userName = usuario.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!validaPass(passwordStr)) {
            pass.error = getString(R.string.error_invalid_password)
            focusView = pass
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(emailStr)) {
            user.error = getString(R.string.error_field_required)
            focusView = user
            cancel = true
        } else if (!validaEmail(emailStr)) {
            user.error = getString(R.string.error_invalid_email)
            focusView = user
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true)

            registrarUsuario(userName, emailStr, passwordStr)
        }
    }

    /**

    Genera datos aleatorios
     */
    fun generateRandom(): String {
        val chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var passWord = ""
        for (i in 0..15) {
            passWord += chars[Math.floor(Math.random() * chars.length).toInt()]
        }
        return passWord
    }

    fun registrarUsuario(userName: String, email: String, pass: String) {

        /**
        Se registra con un email y contraseña aleatoria, por el tema de que no hay terminos y licencia
        y asi no se tratan los datos insertados de quien quiera "registrarse" y probar la APP.
         */
        //Si es correcta, registramos
        mAuth?.createUserWithEmailAndPassword("${generateRandom()}@prueba.com", generateRandom())

            ?.addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) {

                    // Sign in success, update UI with the signed-in user's information
                    Log.d("log", "createUserWithEmail:success")
                    val bd = BD()

                    //Alamcenamos el usuario en nuestra base de datos
                    /**
                    USUARIO FICTICIO
                     */
                    bd.guardarUsuario(database, task.result!!.user.uid, generateRandom(), generateRandom())

                    Toast.makeText(this, "Se ha registrado", Toast.LENGTH_LONG).show()

                    val user = mAuth?.currentUser
                    updateUI(user)

                } else {

                    // If sign in fails, display a message to the user.
                    Log.w("log", "createUserWithEmail:failure", task.exception)
                    val error = task.exception?.toString()
                    val mensaje = error?.split(":")
                    mensaje("${mensaje!![1]}")
                    Toast.makeText(this, "Registro Fallido", Toast.LENGTH_LONG).show()

                }
            }

    }

    fun mensaje(mensaje: String?) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
    }

    fun updateUI(usuario: FirebaseUser?) {

        // Comprueba si tenemos un usuario
        usuario?.email
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        finish()

    }

    private fun validaEmail(email: String): Boolean {

        //Se obtiene un pattern ya creado y mas efectivo para validar el email
        val pattern: Pattern = Patterns.EMAIL_ADDRESS

        //Se comprueba si el email introducido es un email valido a partir del pattern
        return pattern.matcher(email).matches()

    }

    private fun validaPass(email: String): Boolean {

        /*
            /^
            (?=.*\d)          // Debe contener al menos 1 dígito
            (?=.*[a-z])       // Debe contener al menos 1 una minúscula
            (?=.*[A-Z])       // Debe contener al menos 1 una mayúscula
            [a-zA-Z0-9]{8,}   // Debe contener al menos 8 caracteres de los mencionados
            $/
        */

        val pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$")

        //Se comprueba si la contraseña introducida es una contraseña segura
        return pattern.matcher(email).matches()

    }

    fun back(v: View) {

        this.finish()
    }

    fun mensaje() {

        // Inicializa una nueva instancia
        val builder = AlertDialog.Builder(this@RegisterActivity)

        // Titulo de la alerta
        builder.setTitle("Alerta")

        // Mensaje de la alerta
        builder.setMessage("La información insertada no será almacenada ni compartida con nadie, se logeará con datos aleatorios de manera ficticia.")

        // Añade un boton positivo
        builder.setPositiveButton("Acepto") { _, _ ->

        }
        // Añade un boton neutral
        builder.setNeutralButton("Salir") { _, _ ->
            finish()
        }
        // Finalmente crea la alerta
        val dialog: AlertDialog = builder.create()

        // Muestra la alerta
        dialog.show()
    }
}
