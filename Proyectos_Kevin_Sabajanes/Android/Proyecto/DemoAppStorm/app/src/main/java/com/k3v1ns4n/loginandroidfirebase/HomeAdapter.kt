package com.k3v1ns4n.loginandroidfirebase

import android.content.Intent
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.k3v1ns4n.loginandroidfirebase.Objetos.BDINSTANCE
import com.k3v1ns4n.loginandroidfirebase.Objetos.CurrentUser
import com.k3v1ns4n.loginandroidfirebase.MostrarIdea.VisualizarIdea
import com.k3v1ns4n.loginandroidfirebase.Objetos.Ideas
import kotlinx.android.synthetic.main.item_card_home.view.*
import kotlinx.android.synthetic.main.android_main_funcionality.view.*


/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class HomeAdapter(homeActivity: HomeActivity?, savedMode: Boolean) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    var home = homeActivity
    var bd: BD? = BD()
    val savedMode = savedMode
    val bdInstance =  BDINSTANCE.database
    var ideas = Ideas.ideas
    val user = CurrentUser.UID
    val likesUser = CurrentUser.likes

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vistaCelda = LayoutInflater.from(parent.context).inflate(R.layout.item_card_home,parent,false)
        return ViewHolder(vistaCelda)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        /** empty implementation */

        if (savedMode){
            holder.itemView.likes.visibility = View.INVISIBLE
            holder.itemView.comments.visibility = View.INVISIBLE
            holder.itemView.like.visibility = View.INVISIBLE
            holder.itemView.commentIcon.visibility = View.INVISIBLE
            ideas = Ideas.ideasSQL
        }

        val idea = ideas?.get(position)
        holder.itemView.shortDescription.text = idea?.shortDescription
        holder.itemView.likes.text = idea?.likes.toString()
        holder.itemView.comments.text = idea?.comments.toString()
        holder.itemView.tag = position

        //Limpiamos el linear layout de los elementos que se hna insertado anteriormente para evitar que se repitan
        holder.itemView.linearLayout.removeAllViews()

        if (!savedMode) {
            //Recorremos las funcionaliades almacenadas
            if (idea?.liked!!) {

                holder.itemView.like.setBackgroundResource(R.drawable.ic_heart_red)

            } else {

                holder.itemView.like.setBackgroundResource(R.drawable.ic_heart_grey)
            }
        }
        for (i in 0 until idea?.funcionalidades?.size!!) {

            //Añadimos una funcionalidad a partir de un diseño
            val view = LayoutInflater.from(holder.itemView.context).inflate(R.layout.android_main_funcionality,null)

            view.iconName.text = idea.funcionalidades!![i].nombre

            //Creamos el recurso del icono

            val ressourceId = view.resources.getIdentifier(
                "${idea.funcionalidades!![i].Icon}",
                "drawable",
                Package.instance
            )

            //Añadimos el icono correspondiente
            view.icon.setBackgroundResource(ressourceId)
            view.setPadding(15,0,15,0)
            view.circleCount.visibility = View.VISIBLE
            view.count.visibility = View.VISIBLE
            view.count.text = idea.funcionalidades!![i].f_menores.size.toString()
            //Añadimos la vista al linear layout
            holder.itemView.linearLayout.addView(view)

            if (i == 4){ break }
        }
    }

    override fun getItemCount(): Int {

        return if (savedMode){
            Ideas.ideasSQL?.size!!
        }else{

            Ideas.ideas?.size!!
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> MEDIA16x9_ACTIONS_VIEW_TYPE
            else -> MEDIA16x9_ACTIONS_VIEW_TYPE
        }
    }

    companion object {
        const val MEDIA16x9_ACTIONS_VIEW_TYPE = 0
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        val descripcion: TextView
        val card: CardView

        init {

            descripcion = itemView.findViewById(R.id.shortDescription)
            card = itemView.findViewById(R.id.card)
            if (!savedMode) {
                itemView.like.setOnClickListener {
                    val id = itemView.tag as Int
                    val idea = ideas?.get(id)
                    var liked = ideas?.get(id)?.liked

                    //Almacena el valor contrario al que es
                    liked = !liked!!

                    ideas?.get(id)?.liked = liked

                    if (idea?.liked!!) {

                        itemView.like.setBackgroundResource(R.drawable.ic_heart_red)
                        idea?.likes = idea?.likes?.plus(1)
                        itemView.likes.text = idea?.likes.toString()

                    } else {

                        itemView.like.like.setBackgroundResource(R.drawable.ic_heart_grey)
                        idea?.likes = idea?.likes?.minus(1)
                        itemView.likes.text = idea?.likes.toString()

                    }
                    if (user != null && user.isNotEmpty()) {

                        idea?.key?.let { it1 ->
                            bd!!.addLike(
                                bdInstance, it1, user,
                                liked
                            )
                        }
                    }
                }
            }
            if (!savedMode) {
                itemView.setOnClickListener {

                    val intent = Intent(it.context, VisualizarIdea::class.java)

                    // El titulo de la app identificara al elemento para que no cambie al añadir mas en otras posiciones
                    val id = itemView.tag as Int
                    intent.putExtra("ID", id)

                    //val descriptionPair  = android.support.v4.util.Pair.create<View, String>(descripcion, "description")
                    val cardPair = android.support.v4.util.Pair.create<View, String>(card, "card")
                    val options = home?.let { it1 ->
                        ActivityOptionsCompat.makeSceneTransitionAnimation(
                            it1
                            , cardPair
                        )
                    }
                    it.context.startActivity(intent, options?.toBundle())

                }
            }
        }
    }
}



