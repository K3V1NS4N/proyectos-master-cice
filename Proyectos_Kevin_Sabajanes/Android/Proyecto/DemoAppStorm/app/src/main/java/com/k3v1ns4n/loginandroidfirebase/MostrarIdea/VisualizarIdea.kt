package com.k3v1ns4n.loginandroidfirebase.MostrarIdea

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View

import kotlinx.android.synthetic.main.activity_visualizar_idea.*
import kotlinx.android.synthetic.main.android_main_funcionality.view.*
import kotlinx.android.synthetic.main.content_visualizar_idea.*
import android.widget.LinearLayout
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import com.google.firebase.database.*
import com.k3v1ns4n.loginandroidfirebase.*
import com.k3v1ns4n.loginandroidfirebase.Objetos.BDINSTANCE
import com.k3v1ns4n.loginandroidfirebase.Objetos.CurrentUser
import com.k3v1ns4n.loginandroidfirebase.Objetos.Ideas
import com.k3v1ns4n.loginandroidfirebase.R
import kotlinx.android.synthetic.main.android_main_funcionality.view.icon
import kotlinx.android.synthetic.main.minor_funcionality.view.*

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

//La clase encargada de visualizar las ideas de manera mas extensa al pulsar en ellas
class VisualizarIdea : AppCompatActivity() {

    var id: Int = 0

    //Se declara la referencia a la base de datos
    private lateinit var FirebaseBD: DatabaseReference // BD FIREBASE

    /**[1] & [2]**/
    var taskListener: ValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {

            Log.i("ERROR", "ERROR $p0")
        }
        override fun onDataChange(snapshot: DataSnapshot) {

            /**
             * Se encarga de obtener los comentarios en casod e haberlos
             */
            var intento = 0

            Thread {
                while(intento!=30) {

                    val bd = BD()
                    //Se obtiene la idea a partir de la id que se le pasa por intent al pulsar en el cardview de la idea
                    val idea = Ideas.ideas?.get(id)

                    //Obtenemos los comentarios a aptir del identificador de la idea
                    val resultado = bd.loadComments(snapshot.child("zcomments").child(idea?.key!!),idea)
                    Thread.sleep(100)

                    //Comprobamos que se hayan obtenido los elementos necesarios
                    if(resultado != null){
                        intento = 30

                        runOnUiThread {
                            loader.visibility = View.GONE
                            recycler_view2.adapter = ComentarioAdapter(idea = resultado)
                        }
                    }else{
                        intento++
                    }
                }
            }.start()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizar_idea)

        //Referencia FIREBASE
        FirebaseBD = FirebaseDatabase.getInstance().reference


        //Se salva en otra variable para que se pueda acceder desde fuera
        id = intent.getIntExtra("ID",0)

        //Se obtiene la idea a partir de la id
        val idea = Ideas.ideas?.get(id)

        //Añadimos toda la informacion a partir del objeto
        shortDescription2.text = idea?.shortDescription
        description2.text = idea?.description
        likes.text = idea?.likes.toString()
        author.text = "By: ${idea?.author}"

        if (idea?.comments!! > 1){

            loadComments.text = "VER LOS ${idea?.comments} COMENTARIOS"
        }else if (idea?.comments!!.toInt() == 1){

            loadComments.text = "Hay 1 comentario"
        }else{

            loadComments.text = "NO HAY COMENTARIOS"
            loadComments.isEnabled = false
        }

        loader.visibility = View.GONE

        for (i in 0 until idea.funcionalidades?.size!!) {

            //Añadimos una funcionalidad a partir de un diseño
            val view = LayoutInflater.from(this).inflate(R.layout.android_main_funcionality,null)
            view.iconName.text = idea.funcionalidades!![i].nombre

            //Creamos el recurso del icono

            val ressourceId = view.resources.getIdentifier(
                "${idea!!.funcionalidades!![i].Icon}",
                "drawable",
                Package.instance
            )

            //Añadimos el icono correspondiente
            view.icon.setBackgroundResource(ressourceId)
            view.setPadding(15,0,15,0)
            view.circleCount.visibility = View.VISIBLE
            view.count.visibility = View.VISIBLE
            view.count.text = idea?.funcionalidades!![i].f_menores.size.toString()
            //Añadimos la vista al linear layout
            linearFuncionalities.addView(view)

            //Sirve para limitar el numero de funcionaldiades a mostrar
            if (i == 4){ break }
        }

        /**
         * Muestra las funcionalidades menores, aunque unicamente lo hace con la primera funcioanlidad, ya que...
         * TODO Hay qur mejorar la manera en la que se van a mostrar las funcionaldiades de cada funcionalidad principal,
         *  a si como
         */

        //Recorre las funcionalidades menores
        for (y in 0 until idea?.funcionalidades!![0].f_menores.size) {

            //Generamos los linear layout de manera programatica
            val layout2 = LinearLayout(this)
            layout2.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            layout2.orientation = LinearLayout.HORIZONTAL

            //Añadimos los margenes
            val params2 = layout2.layoutParams as LinearLayout.LayoutParams
            params2.setMargins(0, 20, 0, 0)
            layout2.layoutParams = params2

            //Creamos un text view
            val tv1 = TextView(this)

            //Le asignamos un valor (Aqui iría una descripcion o nombre de la funcioanldaid secundaria)
            tv1.text = "Elemento $y"
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT

            ).apply {
                weight = 1.0f
                gravity = Gravity.CENTER_HORIZONTAL
                gravity = Gravity.CENTER_VERTICAL
                gravity = Gravity.CENTER

            }

            params.setMargins(20, 0, 0, 0)

            tv1.layoutParams = params

            val view = LayoutInflater.from(this).inflate(R.layout.minor_funcionality, null)
            view.littleCircle.setColorFilter((Color.argb(100, 203, 203, 203)))

            val ressourceId = view.resources.getIdentifier(
                "${idea!!.funcionalidades!![0].f_menores[y]}",
                "drawable",
                Package.instance
            )
            view.icon.setBackgroundResource(ressourceId)

            //Obtenemos las dimensiones del icono
            val iW=view.icon.background.intrinsicWidth
            val ih=view.icon.background.intrinsicHeight
            //La hacemos mas pequeña respetando la proporcion de cada icono (que son diferentes entre si)
            val dimensionInDp =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (iW/5).toFloat(), resources.displayMetrics)
                    .toInt()

            val dimensionInDp2 =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (ih/5).toFloat(), resources.displayMetrics)
                    .toInt()
            //Eestablecemos las modificaciones
            view.icon.layoutParams.height = dimensionInDp2
            view.icon.layoutParams.width = dimensionInDp
            view.icon.requestLayout()

            layout2.addView(view)
            layout2.addView(tv1)

            secondFuncionalities.addView(layout2)
        }

        //Boton con forma de corazon encargado de dar like a la idea
        like.setOnClickListener {

            //Almacena el valor contrario al que es
            idea.liked = !idea.liked

            if (idea.liked) {
                like()
                like.setBackgroundResource(R.drawable.ic_heart_red)
                idea.likes = idea.likes?.plus(1)
                likes.text = idea.likes.toString()
            }else{
                like()
                like.setBackgroundResource(R.drawable.ic_heart_grey)
                idea.likes = idea.likes?.minus(1)
                likes.text = idea.likes.toString()
            }
        }

        //Gestiona si se tiene que mostrar el corazon rojo o gris dependiendo si el like esta activado
        if (idea.liked) {
            like.setBackgroundResource(R.drawable.ic_heart_red)
        }else{
            like.setBackgroundResource(R.drawable.ic_heart_grey)
        }

        //Boton para postear un comentario
        post.setOnClickListener {
            post.visibility = View.GONE
            posting.visibility = View.VISIBLE

            val bd = BD()
            val bdFirebase = BDINSTANCE
            val comentario = comment.text.toString()

            if (bdFirebase != null ){
                Thread {
                    //Se establece la respuesta de la base de datos a null
                    BDINSTANCE.respuesta = null
                    bd.uploadComment(bdFirebase.database,idea!!.key!!,comentario)

                    var contador = 0
                    while (BDINSTANCE.respuesta == null){
                        contador++
                        if (contador == 30){
                            break
                        }
                        Thread.sleep(100)
                    }
                    //Si hemos recibido una respuesta y dicha respuesta es positiva
                    //se procera a informar al usuario de que la subida ha sido correcta
                    if(BDINSTANCE.respuesta != null && BDINSTANCE.respuesta!!){
                        runOnUiThread {
                            post.visibility = View.VISIBLE
                            posting.visibility = View.GONE
                            comment.setText("")
                            //Snackbar(view)
                            val snackbar = Snackbar.make(it, "Se ha publicado su comentario",
                                Snackbar.LENGTH_LONG).setAction("Action", null)

                            snackbar.show()

                        }

                        //En caso contrario, informaremos igual
                    }else{
                        runOnUiThread {
                            post.visibility = View.VISIBLE
                            posting.visibility = View.GONE
                            //Snackbar(view)
                            val snackbar = Snackbar.make(it, "Ha ocurrido un error al publicar el comentario",
                                Snackbar.LENGTH_LONG).setAction("Action", null)
                            snackbar.show()
                        }
                    }
                }.start()
            }
        }

        //Cargar los comentarios
        loadComments.setOnClickListener {

            loader.visibility = View.VISIBLE
            loadComments.visibility = View.GONE
            FirebaseBD.orderByKey().addListenerForSingleValueEvent(taskListener)
        }
        recycler_view2.layoutManager = LinearLayoutManager(this)
    }

    //Se encarga de dar like o eliminar el like y reflejar el contador en la base de datos.
    private fun like(){
        val user = CurrentUser.UID
        val idea = Ideas.ideas?.get(id)
        var bd: BD? = BD()
        val bdInstance =  BDINSTANCE.database
        var liked =  idea?.liked

        //Comprueba que hay un usuario logeado
        if (user != null && user.isNotEmpty()){
            //En caso de tener el identificador de la idea, procedemos a dar like o a quitarselo.
            idea?.key?.let { it1 ->
                bd!!.addLike(bdInstance, it1, user,
                    liked!!)
            }
        }
    }

    //Flechita para ir hacia atras
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    /**
     * Mejoramos algo la transicion a la hora de volver a tras
     */
    override fun onBackPressed() {
        super.onBackPressed()

        shortDescription2.visibility = View.GONE
        description2.visibility = View.GONE
        linearFuncionalities.visibility = View.GONE
        secondFuncionalities.visibility = View.GONE
        recycler_view2.visibility = View.GONE
        comment.visibility = View.GONE
        post.visibility = View.GONE
        like.visibility = View.GONE
    }
}
