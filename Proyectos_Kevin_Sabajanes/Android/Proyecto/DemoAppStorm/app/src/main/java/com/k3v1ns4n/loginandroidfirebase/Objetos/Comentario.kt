package com.k3v1ns4n.loginandroidfirebase.Objetos

/**
 * Copyright © 2019 K3V1NS4N. All rights reserved.
 */

class Comentario {

    var key: String? = null
    var fecha: String? = null
    var autor: String? = null
    var comentario: String? = null

}